<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

require_once APPPATH."libraries/Classes/PHPExcel.php";
//require_once APPPATH."libraries/Classes/PHPExcel/IOFactory.php";
//require_once APPPATH."libraries/Classes/PHPExcel/Settings.php";
//require_once APPPATH."libraries/Classes/PHPExcel/CachedObjectStorageFactory.php";

class ExcelCustom {
    public function __construct() {
        $this->CI =& get_instance();
    }
    
    function exportAllContacts($nombreArchivo, $tituloExcel, $data = NULL) {
        ini_set('memory_limit', '-1');
        /*$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
        $cacheSettings = array( 'memoryCacheSize' => '256MB');
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings); */
        
        $objPHPExcel = new PHPExcel();                                  //creando un objeto excel
        $objPHPExcel->getProperties()                                   //propiedades
                ->setCreator("NexoInmobiliario")
                ->setTitle($tituloExcel)
                ->setDescription("Excel Exportado");	
        $objPHPExcel->setActiveSheetIndex(0);                           //poniendo activa la hoja 1
        $objPHPExcel->getActiveSheet()->setTitle("Hoja1");		//titulo de la hoja 1
        
        //llenando celdas
        if($data != NULL) {
            $objPHPExcel->getActiveSheet()->fromArray($data, NULL, 'A1');
        }
		
	    //poniendo en negritas la fila de los titulos
        $styleArray = array('font' => array('bold' => true));
	    $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')-> applyFromArray($styleArray);
        //poniendo columnas con tamanio auto según el contenido, asumiendo Z como la ultima
        /*for ($i = 'A'; $i <= 'Z'; $i++) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
	    }*/
	    // Salida
	    $nombreArchivo = $nombreArchivo.date('YmdHis');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$nombreArchivo.'.xls"');
        // Para (Excel2007)
        //header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        //header('Content-Disposition: attachment; filename="'.$nombreArchivo.'.xlsx"');
        //-------------------------------------------------------------------------------------------
        header("Cache-Control: max-age=0");
        //Si se usa IE9
        header('Cache-Control: max-age=1');
        // Genera excel
        $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel5");
        // Para (Excel2007)
        //$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        //-------------------------------------------------------------------------------------------
        //Escribir
        $writer->save("php://output");
        //$objPHPExcel->disconnectWorksheets();
        //unset($objPHPExcel);
        //$writer->save("$nombreArchivo".".xls");
        /*header("location: ".base_url()."$nombreArchivo".".xls");
        unlink(base_url()."$nombreArchivo".".xsl");*/
        exit();
    }    
    
    
}
?>