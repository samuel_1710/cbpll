<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Card extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Users_model', 'user_model');
        $this->load->model('Ubigeo_model', 'obj_ubigeo');
        $this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Procedures_model', 'procedure_model');
        $this->procedures = $this->get_tramites();
        $this->master_tmp->set("procedures", $this->procedures);
    }
	public function index()
	{
        //$this->load->view('search');
        $message = "mostrando message";
        $this->master_tmp->set("message", $message);
        $this->master_tmp->set("titleSeo", "Registro Virtual | Servicios - CBP La Libertad");
        $obj_departamento = $this->obj_ubigeo->get_departments();

        $this->master_tmp->set('obj_departamento', $obj_departamento);
        $this->master_tmp->render('card_virtual/card_virtual');
    }
    public function get_provincias() {
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $departamento = $this->input->post('departamento');
            $obj_provincia = $this->obj_ubigeo->get_provinces($departamento);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Petición realizada';
            $response['data'] = $obj_provincia;
        }
        echo json_encode($response);
        exit;
    }

    public function get_distritos() {
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $provincia = $this->input->post('provincia');
            $obj_distrito = $this->obj_ubigeo->get_districts($provincia);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Petición realizada';
            $response['data'] = $obj_distrito;
        }
        echo json_encode($response);
        exit;
    }
    public function insert_colegiado(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $nroDoc = $this->input->post("nro_doc");
            $typeDoc = $this->input->post("type_doc");
            $colegiado = $this->colegiado_model->get_colegiado_by_document($typeDoc,$nroDoc);

            if(count($colegiado)>0){
                $response = [
                    'status' => 'ERROR',
                    'message' => 'La persona ya ha sido registrada.'
                ];
            }
            else{

                $speciality = $this->input->post('speciality')==='Otros'?$this->input->post('speciality_other'):$this->input->post('speciality');
                $result=$this->user_model->insert([
                    'ApellidosPaterno' => $this->input->post("ape_pat"),
                    'ApellidoMaterno' => $this->input->post("ape_mat"),
                    'NombresUsuario' => $this->input->post("name"),
                    'FechaNacimiento' =>  $this->input->post("date_nac"),
                    'TipoDocumento' =>  $this->input->post("type_doc"),
                    'NumeroDocumento' => $this->input->post("nro_doc"),
                    'GeneroUsuario' => $this->input->post('genero'),
                    'EstadoCivil' => $this->input->post('estado_civil'),
                    'NacionalidadUsuario' => $this->input->post("nationality"),
                    'TipoSangre' => $this->input->post("group_sangre"),
                    'DireccionUsuario' =>  $this->input->post("address"),
                    'DepartamentoUsuario' =>  $this->input->post("dpto"),
                    'ProvinciaUsuario' => $this->input->post("prov"),
                    'DistritoUsuario' => $this->input->post("district"),
                    'TelefonoUsuario' => $this->input->post("phone"),
                    'CelularUsuario' =>  $this->input->post("celphone"),
                    'EmailUsuario' =>  $this->input->post("email"),
                    'Linkweb' => $this->input->post('linkweb'),
                    'speciality' => $speciality 
                ]);
                $this->colegiado_model->insert([
                    'FechaColegiado' => null,
                    'EstadoColegiado' => 0,
                    'CodigoColegiado' => null,
                    'Usuarios_idUsuarios' => $result
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se registró correctamente.';
            }
        }
        echo json_encode($response);
        exit;
    }
}
