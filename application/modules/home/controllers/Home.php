<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Users_model', 'user_model');
        $this->load->model('Ubigeo_model', 'obj_ubigeo');
        $this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Imageslider_model', 'slider_model');
        $this->load->model('Events_model', 'events_model');
        $this->load->model('Comunication_model', 'comunication_model');
        $this->load->model('Procedures_model', 'procedure_model');
        $this->procedures = $this->get_tramites();
        $this->master_tmp->set("procedures", $this->procedures);
    }
	public function index()
	{
        //$this->load->view('search');
        $events=$this->events_model->get_last_events();
        $images = $this->slider_model->get_images_by_type(1);
        $infraestructura = $this->slider_model->get_images_by_type(2);
        $convenios = $this->slider_model->get_images_by_type(3);
        $comunications = $this->comunication_model->get_images_home();
		$this->master_tmp->set("comunications", $comunications);
        $this->master_tmp->set("events", $events);
		$this->master_tmp->set("images", $images);
        $this->master_tmp->set("infraestructura", $infraestructura);
        $this->master_tmp->set("convenios", $convenios);
        $this->master_tmp->render('inicio/inicio');
    }
       
}
