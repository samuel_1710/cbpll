<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Colegiado_model', 'colegiado_model');
		$this->load->model('Procedures_model', 'procedure_model');
        $this->procedures = $this->get_tramites();
        $this->master_tmp->set("procedures", $this->procedures);
    }
	public function index()
	{
        //$this->load->view('search');
        $message = "mostrando message";
		$this->master_tmp->set("titleSeo", "Consultar Habilitación | Servicios - CBP La Libertad");
        $this->master_tmp->set("message", $message);
        $this->master_tmp->render('search/search');
	}
	public function filter()
	{
        if ($this->input->is_ajax_request()) {
			$type = $this->input->post('type_filter');
			$description = $this->input->post('description_filter');
			$result=$this->colegiado_model->filter($type,$description);
			$data['status'] = 'SUCCESS';
			$data['colegiados'] = $result;
			$data['message'] = 'Se registró correctamente.';
			echo $this->load->view('search/table_search',$data,true);
			
        	exit;
        }

	}
}
