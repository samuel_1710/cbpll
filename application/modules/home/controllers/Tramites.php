<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tramites extends MY_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Procedures_model', 'procedure_model');
        $this->procedures = $this->get_tramites();
        $this->master_tmp->set("procedures", $this->procedures);
    }
	public function index()
	{
        $message = "mostrando message";
        $this->master_tmp->set("message", $message);
        $this->master_tmp->set("titleSeo", "Colegiatura | Trámites - CBP La Libertad");
        $this->master_tmp->render('tramites/colegiatura');
    }
    public function tramite($slug)
	{   
        if($slug!==''){
            $procedure=$this->procedure_model->get_procedure_by_slug($slug);
            $this->master_tmp->set("titleSeo", $procedure->title." | CBP La Libertad");
            $this->master_tmp->set("descriptionSeo", "Requisitos y Procedimiento para $procedure->title | CBP La Libertad");
            $this->master_tmp->set("procedure", $procedure);
            $this->master_tmp->render('tramites/tramite');
        }
        
    }
    public function segunda_especialidad()
	{ 
        $this->master_tmp->set("titleSeo", "Segunda Especialización | Trámites - CBP La Libertad");
        $this->master_tmp->render('tramites/segunda_especialidad');
    }
    public function traslados()
	{
        $this->master_tmp->set("titleSeo", "Traslados de Sede | Trámites - CBP La Libertad");
        $this->master_tmp->render('tramites/traslados');
    }
    public function renovacion_carnet()
    {
        $this->master_tmp->set("titleSeo", "Renovación de Carnet | Trámites - CBP La Libertad");
        $this->master_tmp->render('tramites/renovacion_carnet');
    }
    public function otros_tramites()
    {
        $this->master_tmp->set("titleSeo", "Otros trámites | Trámites - CBP La Libertad");
        $this->master_tmp->render('tramites/others_tramite');
    }
	
}