<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Institucional extends MY_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Institucional_model', 'institucional_model');
        $this->load->model('Comisions_model', 'comisions_model');
        $this->load->model('Boletin_model', 'boletin_model');
        $this->load->model('Directives_model', 'directives_model');
        $this->load->model('Procedures_model', 'procedure_model');
        $this->procedures = $this->get_tramites();
        $this->master_tmp->set("procedures", $this->procedures);
        
    }
	public function index()
	{
        $institucional=$this->institucional_model->get_data();
        $this->master_tmp->set("titleSeo", "Quiénes Somos | CBP La Libertad");
		$this->master_tmp->set("institucional", $institucional);
        $this->master_tmp->render('institucional/institucional');
    }
    public function consejodirectivo(){
        $this->master_tmp->set("titleSeo", "Consejo Directivo | CBP La Libertad");
        $obj_directives = $this->directives_model->get_directive_visibility();
		$this->master_tmp->set("obj_directives", $obj_directives);
        $this->master_tmp->render('institucional/consejo_directivo');
    }
    public function comisiones()
    {
        $comisiones=$this->comisions_model->get_comisions_visibility();
		$this->master_tmp->set("titleSeo", "Nuestras Comisiones | CBP La Libertad");
        $this->master_tmp->set("comisiones", $comisiones);
        $this->master_tmp->render('institucional/comisiones');
    }
    public function organigrama(){
        $this->master_tmp->render('institucional/organigrama');
    }
    public function resoluciones(){
        $boletines=$this->boletin_model->get_resoluciones();
        $this->master_tmp->set("titleSeo", "Nuestras Resoluciones aprobadas en Consejo | CBP La Libertad");
		$this->master_tmp->set("boletines", $boletines);
        $this->master_tmp->render('institucional/resoluciones');
    }
    public function boletines(){
        $boletines=$this->boletin_model->get_boletines();
        $this->master_tmp->set("titleSeo", "Nuestros Boletínes Informativos | CBP La Libertad");
		$this->master_tmp->set("boletines", $boletines);
        $this->master_tmp->render('institucional/boletines');
    }
	
}
