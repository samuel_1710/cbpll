<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MY_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Events_model', 'events_model');
        $this->load->model('Procedures_model', 'procedure_model');
        $this->procedures = $this->get_tramites();
        $this->master_tmp->set("procedures", $this->procedures);
    }
	public function index()
	{
        $events=$this->events_model->get_events_visibility();
        $this->master_tmp->set("titleSeo", "Eventos y Actividades | CBP La Libertad");
		$this->master_tmp->set("events", $events);
        $this->master_tmp->render('events/list_events');
    }
    public function event($slug)
	{   
        if($slug!==''){
            $event=$this->events_model->get_event_by_slug($slug);
            $imagen_seo = URL_UPLOADS."events/".$event->image;
            $array_seo=array((Object)array("image"=>$imagen_seo,"url"=>'evento-actividad/'.$slug));
            $this->master_tmp->set("titleSeo", $event->title." | CBP La Libertad");
            $this->master_tmp->set("descriptionSeo", $event->description." | CBP La Libertad");
            $this->master_tmp->set("obj_seo", $array_seo );
            $this->master_tmp->set("event", $event);
            $this->master_tmp->render('events/events');
        }
        
    }
	
}
