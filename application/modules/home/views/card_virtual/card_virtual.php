<!-- <section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(<?php echo URL_STATIC?>website/dist/images/breadcrumbs-image-1.jpg);">
	<div class="breadcrumbs-custom-inner">
		<div class="container breadcrumbs-custom-container">
			<div class="breadcrumbs-custom-main">
				<h6 class="breadcrumbs-custom-subtitle title-decorated">CBP La Libertad</h6>
				<h1 class="breadcrumbs-custom-title">Ficha Virtual</h1>
			</div>
			<ul class="breadcrumbs-custom-path">
				<li><a href="<?php echo base_url(); ?>ficha-virtual">Inicio</a></li>
				
				<li><a href="<?php echo base_url(); ?>ficha-virtual">Colegiado</a></li>
				<li class="active">Ficha Virtual</li>
			</ul>
		</div>
	</div>
</section> -->
<section class="section text-center containerSearch">
    <div class="container">
        <h3 class="wow-outer"><span class="wow slideInDown">Ficha Virtual de Registro</span></h3><br>
        <form id="formPreRegistro" class="form-horizontal formColegiado form-card-virtual" role="form">
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="ape_pat" class="col-md-4 control-label">Apellido Paterno</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="Ingrese su apellido paterno">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="ape_mat" class="col-md-4 control-label">Apellido Materno</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="Ingrese su apellido materno">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="name" class="col-md-4 control-label">Nombres</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese sus nombres">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="date_nac" class="col-md-4 control-label">Fecha Nac.</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" id="date_nac" name="date_nac" placeholder="Ingrese su fecha de nacimiento" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="type_doc" class="col-md-4 control-label">Tipo de Documento</label>
                        <div class="col-md-8 input-card-virtual">
                            <select class="form-control" name="type_doc" id="type_doc">
                                <option value="DNI">DNI</option>
                                <option value="CE">CE</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="nro_doc" class="col-md-4 control-label">Nro de Documento</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" name="nro_doc" id="nro_doc" placeholder="Ingrese nro de documento">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="nationality" class="col-md-4 control-label">Nacionalidad</label>
                        <div class="col-md-8 input-card-virtual">
                            <select class="form-control" name="nationality" id="nationality">
                                <option value="Peruana">Peruana</option>
                                <option value="Brasilera">Brasilera</option>
                                <option value="Chilena">Chilena</option>
                                <option value="Cubana">Cubana</option>
                                <option value="Mexicana">Mexicana</option>
                                <option value="Venezolana">Venezolana</option>
                                <option value="Hispano Hablantes">Hispano Hablantes</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="group_sangre" class="col-md-4 control-label">Grupo Sanguíneo</label>
                        <div class="col-md-8 input-card-virtual">
                            <select name="group_sangre" id="group_sangre" class="form-control">
                                <option value="ARH+">ARH+</option>
                                <option value="BRH+">BRH+</option>
                                <option value="ORH+">ORH+</option>
                                <option value="ABRH+">ABRH+</option>
                                <option value="ARH-">ARH-</option>
                                <option value="BRH-">BRH-</option>
                                <option value="ORH-">ORH-</option>
                                <option value="ABRH-">ABRH-</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="ejemplo_email_3" class="col-md-4 control-label">Estado Civil</label>
                        <div class="col-md-8 input-card-virtual">
                            <select class="form-control" name="estado_civil" id="estado_civil">
                                <option value="soltero">Soltero</option>
                                <option value="casado">Casado</option>
                                <option value="viudo">Viudo</option>
                                <option value="divorciado">Divorciado</option>
                                <option value="conviviente">Conviviente</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="ejemplo_email_3" class="col-md-4 control-label">Género</label>
                        <div class="col-md-8 input-card-virtual">
                            <select class="form-control" name="genero" id="genero">
                                <option value="1">Hombre</option>
                                <option value="2">Mujer</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="address" class="col-md-2 control-label">Especialidad</label>
                        <div class="col-md-10 input-card-virtual">
                            <select class="form-control" name="speciality" id="speciality">
                                <option value="">Seleccione Especialidad</option>
                                <option value="Biólogo">Biólogo</option>
                                <option value="Microbiólogo">Microbiólogo</option>
                                <option value="Pesquero">Pesquero</option>
                                <option value="Otros">Otros</option>
                            </select>
                            <div class="mt-2 insert_speciality d-none">
                                <input type="text" class="form-control" id="speciality_other" name="speciality_other" placeholder="Ingrese la especialidad">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="address" class="col-md-2 control-label">Dirección</label>
                        <div class="col-md-10 input-card-virtual">
                            <input type="text" class="form-control" id="address" name="address" placeholder="Ingrese su dirección">  
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="dpto" class="col-md-4 control-label">Departamento</label>
                        <div class="col-md-8 input-card-virtual">
                            <!--<input type="text" class="form-control" id="dpto" name="dpto" placeholder="Ingrese Departamento">  -->
                            <select class="form-control" name="dpto" id="dpto">
                                <option value="">Seleccione Departamento</option>
                                <?php foreach($obj_departamento as $obj_departamento){ ?>
                                    <option value="<?php echo $obj_departamento->id; ?>">
                                        <?php echo $obj_departamento->name; ?>
                                    </option>
                                <?php } ?> 
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="prov" class="col-md-4 control-label">Provincia</label>
                        <div class="col-md-8 input-card-virtual">
                            <!--<input type="text" class="form-control" id="prov" name="prov" placeholder="Ingrese Provincia">  -->
                            <select class="form-control" name="prov" id="prov">
                                <option value="">Seleccione Provincia</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="district" class="col-md-2 control-label">Distrito</label>
                        <div class="col-md-10 input-card-virtual">
                            <!--<input type="text" class="form-control" id="district" name="district" placeholder="Ingrese Distrito">  -->
                            <select class="form-control" name="district" id="district">
                                <option value="">Seleccione Distrito</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="phone" class="col-md-4 control-label">Télefono</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Ingrese su télefono">  
                        </div>
                    </div>
                </div>
                <div class="col-md-6 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="celphone" class="col-md-4 control-label">Celular</label>
                        <div class="col-md-8 input-card-virtual">
                            <input type="text" class="form-control" id="celphone" name="celphone" placeholder="Ingrese su celular">  
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="email" class="col-md-2 control-label">Email</label>
                        <div class="col-md-10 input-card-virtual">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese su email">  
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 subcontainer-card-virtual">
                    <div class="form-group">
                        <label for="linkweb" class="col-md-2 control-label">Blog/Web</label>
                        <div class="col-md-10 input-card-virtual">
                            <input type="text" class="form-control" id="linkweb" name="linkweb" placeholder="Ingrese link de Blog o Web"> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <button type="submit" class="button button-primary button-winona">Guardar</button>
            </div>
            <br>
        </form>
    </div>
</section>
<script src="<?php echo link_static('website/dist/js/cb_card.min.js'); ?>"></script>