<div class="container mt-5 container-main-comisiones">
    <h2 class="mb-0 pb-0 rounded container-title-comisiones">
        <span class="font-weight-bold pt-2 pb-2 pl-3 pr-3">COMISIONES PERMANENTES</span>
    </h2>
    <hr class="shadow-sm pb-1 border-0">
    <div class="row mb-5">
        <?php if(count($comisiones)>0){?>
            <?php foreach ($comisiones as $comision) { ?>
                <?php if($comision->type=="1"){ ?>
                <div class="col-md-3 col-sm-6 col-xs-12 mb-4 container-card-comision">
                    <div class="card mb-3 shadow h-100 w-100">
                        <div class="card-body d-flex justify-content-center align-items-center text-center">
                            <h6 class="card-title text-capitalize  color-principal mb-0 text-capitalize"><?php echo $comision->name;?></h6>
                        </div>
                        <div class="card-footer bg-light">
                            <span class="">Presidente:</span><br>
                            <span class="text-muted presidente-card"><?php echo $comision->president;?></span>
                        </div>
                        <input type="hidden" class="members" value="<?php echo $comision->members;?>">
                    </div>
                </div>
            <?php } }?>
        <?php } ?>
    </div>
    <h2 class="mb-0 pb-0 rounded container-title-comisiones">
        <span class="font-weight-bold pt-2 pb-2 pl-3 pr-3">COMISIONES TRANSITORIAS</span>
    </h2>
    <hr class="shadow-sm pb-1 border-0">
    <div class="row mb-5">
        <?php if(count($comisiones)>0){?>
            <?php foreach ($comisiones as $comision) { ?>
                <?php if($comision->type=="2"){ ?>
                <div class="col-md-3 col-sm-6 col-xs-12 mb-4 container-card-comision">
                    <div class="card mb-3 shadow h-100 w-100">
                        <div class="card-body d-flex justify-content-center align-items-center text-center">
                            <h6 class="card-title text-capitalize  color-principal mb-0 text-capitalize"><?php echo $comision->name;?></h6>
                        </div>
                        <div class="card-footer bg-light">
                            <span class="">Presidente:</span><br>
                            <span class="text-muted presidente-card"><?php echo $comision->president;?></span>
                        </div>
                        <input type="hidden" class="members" value="<?php echo $comision->members;?>">
                    </div>
                </div>
            <?php } }?>
        <?php } ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade modal-comision" id="modal_comision" tabindex="-1" role="dialog" aria-labelledby="modal_comisionLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title font-weight-bold color-principal">Comisión de Asesoría Institucional</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p class="font-weight-bold mb-1">Presidente:</p>
            <span class="pl-3 color-principal presidente-modal">Blgo. Wilmar Gutiérrez Portilla</span>
            <p class="font-weight-bold mb-1 mt-3">Miembros:</p>
            <ul class="list-integrantes-comision pl-3">
                <li class="position-relative"><span class="pl-3">Blgo. Wilmar Gutiérrez Portilla</span></li>
                <li class="position-relative"><span class="pl-3">Blgo. Wilmar Gutiérrez Portilla</span></li>
                <li class="position-relative"><span class="pl-3">Blgo. Wilmar Gutiérrez Portilla</span></li>
            </ul>
        </div>
    </div>
  </div>
</div>
<script src="<?php echo link_static('website/dist/js/cb_comision.min.js'); ?>"></script>