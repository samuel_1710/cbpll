<div class="container container-historia">
    <h2 class="font-weight-bold color-principal mt-5 mb-3 pb-2">Institucional</h2>
    <?php if($institucional->image!=="" && is_null($institucional->image)!==true){?>
    <img src="<?php echo URL_UPLOADS . $institucional->image?>" alt="" class="float-left img-fluid rounded shadow mr-5 mb-3">
    <?php } ?>
    <div class="mb-5">
        <?php echo $institucional->history?>
    </div>
    <!-- <img src="<?php echo URL_STATIC?>website/dist/images/historia/biologo.jpg" alt="" class="float-left img-fluid rounded shadow mr-5 mb-3">    <div class="mb-5"> -->
    <!-- <div class="mb-5"> -->
        <!-- <h6 class="mb-2 mt-2">AÑO 1901 - Augusto Weberbauer</h6> -->
        <!-- <p class="mb-4">Augusto Weberbaue, naturalista, botánico y profesor universitario alemán que siendo  -->
            <!-- profesor en la Universidad de Breslau donde obtuvo su PhD en botánica, recibió el  -->
            <!-- encargo por la Real Academia de Berlín para desarrollar investigaciones botánicas  -->
            <!-- en el Perú (1901), en 1905 entregó un herbario de 5200 especies recolectadas en este  -->
            <!-- país. -->
        <!-- </p> -->
        <!-- <h6 class="mb-2 mt-2">AÑO 1908 - Parque Zoológico y Jardín Botánico</h6> -->
        <!-- <p class="mb-4">Luego el gobierno peruano le contrató para que dirigiera el Parque Zoológico y  -->
            <!-- Jardín Botánico en 1908. Obtuvo el grado de Doctor en Ciencias en la Universidad Nacional  -->
            <!-- Mayor de San Marcos en 1922; donde fue profesor de Química Farmacéutica de 1923 a 1948 y  -->
            <!-- de Botánica Sistemática de 1925 a 1948; también dirigió el Seminario de Botánica de 1935 a 1948. -->
        <!-- </p> -->
        <!-- <h6 class="mb-2 mt-2">POSTERIORMENTE - La Biólogia en el Perú</h6> -->
        <!-- <p class="mb-4"> -->
            <!-- En el Perú actualmente existen programas o carreras de Biología en Lima (Agraria La Molina,  -->
            <!-- Cayetano Heredia, Federico Villarreal, Ricardo Palma, San Marcos y Científica del sur), y las otras,  -->
            <!-- en las universidades: Universidad San Agustín de Arequipa, San Cristóbal de Ayacucho, Pedro Ruiz Gallo  -->
            <!-- de Lambayeque, Nacional de Piura, Nacional de Puno, Nacional Jorge Basadre de Tacna, Nacional de Santa y  -->
            <!-- la Nacional de Trujillo. Sus egresados tienen representantes distinguidos en las diferentes áreas de la -->
            <!-- Biología; y ellos, por mérito propio, hacen honor a su Alma Máter, prestigiándola y fortaleciéndola.  -->
            <!-- Hasta la fecha 11 biólogos se ha convertido en rectores de diversas universidades. -->
        <!-- </p> -->
        <!-- <h6 class="mb-2 mt-2">ACTUALIDAD - Consejo Directivo 2019 - 2021</h6> -->
        <!-- <p class="mb-4">En consonancia con el desarrollo de la tecnología, se lanza con mucho orgullo la página web del  -->
            <!-- Colegio de Biólogos del Perú Consejo Regional IV La Libertad para ser un vínculo inmediato y  -->
            <!-- actualizado de todos los biólogos liberteños entre sí y con los biólogos del país y del mundo.  -->
            <!-- Compartamos nuestros comentarios que desarrollen y fortalezcan la biología peruana en pro de la  -->
            <!-- vida y la suervivencia de la vida en nuestro planeta. -->
        <!-- </p> -->
    <!-- </div> -->
    <div class="mb-5">
        <h4>
            <span class="font-weight-bold text-white fondo-principal rounded pt-2 pb-2 pl-3 pr-3 text-center">
                Nuestra Misión
            </span>
        </h4>
        <hr class="shadow-sm pb-1 pt-1 border-0">
        <p>
            <?php echo $institucional->mision?>
        </p>
    </div>
    <div class="mb-5">
        <h4>
            <span class="font-weight-bold text-white fondo-principal rounded pt-2 pb-2 pl-3 pr-3 text-center">
                Nuestra Visión
            </span>
        </h4>
        <hr class="shadow-sm pb-1 pt-1 border-0">
        <p>
        <?php echo $institucional->vision?>
        </p>
    </div>

</div>