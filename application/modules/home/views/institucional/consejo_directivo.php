<div class="container">
    <h2 class="font-weight-bold color-principal mt-5 mb-5 pb-2">Consejo Directivo CBP LA LIIBERTAD <?php echo $obj_directives[0]->periodo;?></h2>
    <?php if(count($obj_directives)>0){?>
            <?php foreach ($obj_directives as $directive) { ?>
            <div class="row mx-auto d-flex align-items-center justify-content-center  mb-4 mt-4 container-directivo container-size-responsive">
                <div class="col-md-3 text-center container-img-directivo">
                    <img src="<?php echo URL_UPLOADS?>/directivos/<?php echo $directive->image;?>" class="img-fluid rounded-circle shadow" width="120" height="120">
                </div>
                <div class="pt-4 ml-2 mr-2 order-second-directivo">
                    <h5 class="font-weight-bold color-principal"><?php echo $directive->name;?></h5>
                    <p class="text-secondary mb-0"><?php echo $directive->position;?></p>
                </div>
            </div>

            <hr class="container-size-responsive mx-auto fondo-principal pt-1 rounded">
        <?php }?>
    <?php } ?>
    
    <?php /*
    <div class="row mx-auto d-flex align-items-center justify-content-center  mb-4 mt-4  container-directivo container-size-responsive">
        <div class="col-md-3 text-center container-img-directivo">
            <img src="<?php echo URL_STATIC?>website/dist/images/directivos/vicedecana.jpg" class="img-fluid rounded-circle shadow" width="120" height="120">
        </div>
        <div class=" pt-4 ml-2 mr-2 order-second-directivo">
            <h5 class="font-weight-bold color-principal">Blgo. Angela Mariana Morante Aparicio</h5>
            <p class="text-secondary mb-0">Vicedecano</p>
        </div>
    </div>

    <hr class="container-size-responsive mx-auto fondo-principal pt-1 rounded">
    
    <div class="row mx-auto d-flex align-items-center justify-content-center  mb-4 mt-4 container-directivo container-size-responsive">
        <div class="col-md-3 text-center container-img-directivo">
            <img src="<?php echo URL_STATIC?>website/dist/images/directivos/secretaria.jpg" class="img-fluid rounded-circle shadow" width="120" height="120">
        </div>
        <div class=" pt-4 ml-2 mr-2 order-second-directivo">
            <h5 class="font-weight-bold color-principal">Blgo. Mirtha Yanina Afiler Horna</h5>
            <p class="text-secondary mb-0">Secretaria</p>
        </div>
    </div>

    <hr class="container-size-responsive mx-auto fondo-principal pt-1 rounded">

    <div class="row mx-auto d-flex align-items-center justify-content-center  mb-4 mt-4 container-directivo container-size-responsive">
        <div class="col-md-3 text-center container-img-directivo">
            <img src="<?php echo URL_STATIC?>website/dist/images/directivos/tesorero.jpg" class="img-fluid rounded-circle shadow" width="120" height="120">
        </div>
        <div class=" pt-4 ml-2 mr-2 order-second-directivo">
            <h5 class="font-weight-bold color-principal">Blgo. Amado Abdias Solano Sare</h5>
            <p class="text-secondary mb-0">Tesorero</p>
        </div>
    </div>

    <hr class="container-size-responsive mx-auto fondo-principal pt-1 rounded">

    <div class="row mx-auto d-flex align-items-center justify-content-center  mb-4 mt-4 container-directivo container-size-responsive">
        <div class="col-md-3 text-center container-img-directivo">
            <img src="<?php echo URL_STATIC?>website/dist/images/directivos/vocal1.jpg" class="img-fluid rounded-circle shadow" width="120" height="120">
        </div>
        <div class=" pt-4 ml-2 mr-2 order-second-directivo">
            <h5 class="font-weight-bold color-principal">Blgo. Luis José Montes Mudarra</h5>
            <p class="text-secondary mb-0">Vocal 1</p>
        </div>
    </div>

    <hr class="container-size-responsive mx-auto fondo-principal pt-1 rounded">

    <div class="row mx-auto d-flex align-items-center justify-content-center  mb-4 mt-4 container-directivo container-size-responsive">
        <div class="col-md-3 text-center container-img-directivo">
            <img src="<?php echo URL_STATIC?>website/dist/images/directivos/vocal2.jpg" class="img-fluid rounded-circle shadow" width="120" height="120">
        </div>
        <div class=" pt-4 ml-2 mr-2 order-second-directivo">
            <h5 class="font-weight-bold color-principal">Blgo. Omar Oswaldo Colán Garay</h5>
            <p class="text-secondary mb-0">Vocal 2</p>
        </div>
    </div> */?>
    <div class="mb-4"></div>
</div>