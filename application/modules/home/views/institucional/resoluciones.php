<div class="container mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">RESOLUCIONES</h2>
    <hr class="shadow-sm pb-1 border-0">
        
    <?php if(count($boletines)>0){?>
        <div class="container">
            <ul class="container-item-tramites mb-1">
                <?php $i=1;foreach ($boletines as $boletin) { ?>
                <li class="mb-4">
                    <span class="pl-3 mb-0"><?php echo $boletin->texto?></span>
                    <a target="_blank" href="<?php echo URL_UPLOADS?>docs/<?php echo $boletin->filename;?>">
                        ( ver resolución completa )
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
    <?php } else{ ?>
        <div class="row">
            <p class="col-md-12">No se encontraron resoluciones del CBPLL</p>
        </div>
    <?php } ?>
        
    
</div>