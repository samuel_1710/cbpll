<div class="container container-main-boletin mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">BOLETIN <?php echo date('Y');?></h2>
    <hr class="shadow-sm pb-1 border-0">
    <div class="row">
        <?php if(count($boletines)>0){ ?>
        <div class="col-md-4">
            <img src="<?php echo URL_STATIC?>website/dist/images/boletines/boletin2019portada.jpg" class="mr-3 img-fluid shadow" width="300">
        </div>
        <div class="col-md-8">
            <div class="row container-boletin">
                <?php $i=1;foreach ($boletines as $boletin) { ?>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a class="color-principal font-weight-bold" target="_blank" href="<?php echo URL_UPLOADS?>docs/<?php echo $boletin->filename;?>">
                            <?php echo $boletin->texto?>
                        </a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">
                            <?php $date = date_create($boletin->created_at);
                                $pre_date =  date_format($date, 'Y-m-d');
                                echo date_string($pre_date);
                            ?>
                        </p>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php } else{ ?>
            <p class="col-md-12">No se encontraron boletines publicados del CBPLL</p>
        <?php } ?>
    </div>
    <!-- <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">BOLETIN 2018</h2>
    <hr class="shadow-sm pb-1 border-0">
    <div class="row">
        <div class="col-md-4">
            <img src="<?php echo URL_STATIC?>website/dist/images/boletines/boletingeneral.jpg" class="mr-3 img-fluid shadow" width="300">
        </div>
        <div class="col-md-8">
            <div class="row container-boletin">
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Diciembre 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Enero 21, 2020 at 11:32 am</span>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Noviembre 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Diciembre 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Octubre 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Noviembre 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Setiembre 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Octubre 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Agosto 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Setiembre 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Julio 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Agosto 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Junio 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Julio 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Mayo 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Junio 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Abril 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Mayo 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Marzo 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Abril 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Febrero 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Marzo 21, 2019 at 11:32 am</p>
                    </div>
                </div>
                <div class="col-md-4 border-bottom pt-3 subcontainer-boletin">
                    <div class="content-boletin">
                        <a href="" class="color-principal font-weight-bold">Enero 2019</a>
                        <p class="text-secondary font-weight-light text-fecha-boletin mb-3">Febrero 21, 2019 at 11:32 am</p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</div>