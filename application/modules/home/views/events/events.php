<div class="container mt-5">
    <h2 class="font-weight-bold color-principal mt-5 mb-2 pb-2"><?php echo $event->title?></h2>
    <div class="row mt-0">
    <div class="col-md-8 container-events-und">
            <p> <?php echo $event->description; ?>
            </p>

            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Fecha y Hora:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <ul class="container-item-list-events">
                <li class="item-events">
                    <span class="pl-3"><?php echo $event->horario; ?></span>
                </li>
            </ul>
            
            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Objetivo:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <p>
                <?php echo $event->objetivo; ?>
            </p>

            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Duración:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <p>
                <?php echo $event->duracion; ?>
            </p>

            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Temario:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <p><?php echo $event->temario; ?></p>
        </div>
        <?php if($event->image!==""){?>
        <div class="col-md-4 container-img-events text-center">
            <img class="img-fluid rounded shadow" src="<?php echo URL_UPLOADS."events/".$event->image;?>">
        </div>
        <?php }?>
        <!-- <div class="col-md-8 container-events-und">
            <p>
                El Consejo Regional I del Colegio Médico del Perú está organizando el I CURSO DE ENFERMEDADES CRÓNICAS NO TRANSMISIBLES.
                Contaremos con la participación de distinguidos y destacados especialistas regionales, quienes desarrollan los temas más importantes en el tratamiento de las enfermedades no transmisibles.
            </p>
            <p>
                <span class="font-weight-bold">Dirigido a:</span>
                Médicos familiares, médicos internistas, médicos neumólogos, médicos endocrinologos, médicos generales y médicos residentes.
            </p>
            <p>
                <span  class="font-weight-bold">Organiza:</span>
                Consejo Regional I CMP a través de su Comité de Educación Médica Continua.
            </p>
            <p>
                <span  class="font-weight-bold">Pre - inscripción:</span>
                <a href="">bit.ly/emcLaLibertad</a>
            </p>
            <div>
                <p  class="font-weight-bold">Modalidad:</p>
                <ul class="container-item-list-events">
                    <li class="item-events">
                        <span class="pl-3">Presencial: Auditorio 2° Piso del CMP - CRi (Av. Roma 413)</span>
                    </li>
                    <li class="item-events">
                        <span class="pl-3">Virtual: Los asistentes virtuales rendirán un Tele-Test de Evaluación Calificado.</span>
                    </li>
                </ul>
                <p class="pl-4">Link de transmisión: <a href="">bit.ly/zoomEMC</a></p>
            </div>
            <p>
                <span  class="font-weight-bold">Puntos:</span>
                3.0 - válido para Recertificación Médica (asistencia a todas las clases)
            </p>
            <h6 class="text-dark">
                Si tienes alguna duda y/o consulta, puedes enviar un WhatsApp al número 978 400 389
            </h6>

            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Horario:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <ul class="container-item-list-events">
                <li class="item-events">
                    <span class="pl-3">Miércoles del 03 de mayo al 08 de abril</span>
                </li>
                <li class="item-events">
                    <span class="pl-3">De 7:00 p.m. a  10:00 p.m.</span>
                </li>
            </ul>
            
            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Objetivo:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <p>
                Como objetivo principal se pretende capacitar a los médicos de la región en los tratamientos actuales de las enfermedades crónicas no transmisibles.
            </p>

            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Duración:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <p>
                6 semanas
            </p>

            <h6 class="font-weight-bold color-principal mt-5 mt-0 pb-0 mb-0">Temario:</h6>
            <hr class="shadow-sm pb-1 border-0 mt-0">
            <ul class="container-item-list-events">
                <li class="item-events">
                    <span class="pl-3">Diabetes</span>
                </li>
                <li class="item-events">
                    <span class="pl-3">Hipertensión Arterial</span>
                </li>
            </ul>
        </div>
        <div class="col-md-4 container-img-events text-center">
            <img class="img-fluid rounded shadow" src="<?php echo URL_STATIC?>website/dist/images/boletines/boletin2019portada.jpg">
        </div> -->
    </div>
</div>