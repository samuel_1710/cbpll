<div class="container container-main-events mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">Eventos y Actividades</h2>
    <hr class="shadow-sm pb-1 border-0">

    <?php if(count($events)>0){?>
    <div class="container-events mt-5">
        <div class="row mb-5">
            <?php foreach ($events as $event) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/<?php echo $event->slug?>" class="card-link text-light">
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <div class="container-img-project">
                            <img class="card-img-top" src="<?php echo URL_UPLOADS."events/".$event->image;?>" alt="Card image cap">
                        </div>
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light"><?php echo $event->horario?></p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0"><?php echo $event->title;?></h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
            <?php }?>
            <?php /*
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/akljsdklasdasd" class="card-link text-light">
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <img class="card-img-top" src="http://www.cip.org.pe/wp-content/uploads/2021/02/Flyer-P_g.-Inicio-Webinar-E15FEB.-300x225.jpg" alt="Card image cap">
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light">2021-02-15 00:00:00</p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0">Lorem, ipsum dolor sit amet consectetur</h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/akljsdklasdasd" class="card-link text-light">
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <img class="card-img-top" src="http://www.cip.org.pe/wp-content/uploads/2021/02/Flyer-P_g.-Inicio-Webinar-E15FEB.-300x225.jpg" alt="Card image cap">
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light">2021-02-15 00:00:00</p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0">Lorem, ipsum dolor sit</h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/akljsdklasdasd" class="card-link text-light"> 
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <img class="card-img-top" src="http://www.cip.org.pe/wp-content/uploads/2021/02/Flyer-P_g.-Inicio-Webinar-E15FEB.-300x225.jpg" alt="Card image cap">
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light">2021-02-15 00:00:00</p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aut deleniti obcaecati. Lorem, ipsum dolor sit amet consectetur adipisicing elit. Aut deleniti obcaecati.</h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/akljsdklasdasd" class="card-link text-light">
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <img class="card-img-top" src="http://www.cip.org.pe/wp-content/uploads/2021/02/Flyer-P_g.-Inicio-Webinar-E15FEB.-300x225.jpg" alt="Card image cap">
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light">2021-02-15 00:00:00</p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0">Lorem, ipsum dolor sit amet consectetur</h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/akljsdklasdasd" class="card-link text-light">
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <img class="card-img-top" src="http://www.cip.org.pe/wp-content/uploads/2021/02/Flyer-P_g.-Inicio-Webinar-E15FEB.-300x225.jpg" alt="Card image cap">
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light">2021-02-15 00:00:00</p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0">Lorem, ipsum dolor sit</h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
            <?php }*/?>
        </div>
    </div>
    <?php }else{?>
        <p>No se encontraron eventos programados</p>
    <?php }?>
</div>