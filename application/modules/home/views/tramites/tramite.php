<div class="container container-main-colegiatura mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0"><?php echo $procedure->title;?></h2>
    <hr class="shadow-sm pb-1 border-0">
    
    <div class="container-descripcion mt-0">
        <div>
            <?php if($procedure->description !==""){ ?>
                <?php echo $procedure->description; ?>
            <?php } ?>
        </div>
        <?php if($procedure->note !==""){ ?>
            <div class="container-important-coleg pl-3 pr-3 pt-2 pb-2 mb-5 position-relative d-flex justify-content-center align-items-center">
                <i class="fa fa-exclamation-circle color-principal" aria-hidden="true"></i>
                <h6 class="mb-0 color-principal pl-2"><?php echo $procedure->note; ?></h6>
            </div>
        <?php } ?>
    </div> 
</div>