<div class="container container-main-colegiatura mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">COMO SER UN COLEGIADO</h2>
    <hr class="shadow-sm pb-1 border-0">
    
    <div class="container-card-colegiatura mt-5">
        <div class="row mb-0">
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>1</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Solicitud</h5>
                        <a href="https://drive.google.com/open?id=1_RhalN7c73qX8KlpAzXL_LQGWJ3joSZy" target="_blank" download="Solicitudcbp">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>2</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Ficha Inscripción</h5>
                        <a target="_blank" href="https://drive.google.com/open?id=10Uwmg3fyizSxtg9MuC6Hm9KeGJEzrLvd">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>3</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Declaración Jurada</h5>
                        <a target="_blank" href="https://drive.google.com/open?id=1VFi1rwbg3POYy-OWH8ZhHBa-010_4es_">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>4</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Registro Virtual</h5>
                        <a target="_blank" href="<?php echo base_url() ?>ficha-virtual">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Registrar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>5</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Titulo Profesional Original</h5>
                        
                        <button type="button" class="btn btn-extra-card text-white btn-sm mt-2"> 
                            <a target="_blank" class="thumbnail-modern " href="https://enlinea.sunedu.gob.pe/">SUNEDU</a>
                        </button>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites/tituloprofesional.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>6</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">2 Fotocopias Titulo Profesional</h5>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites/2copias.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>7</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">4 Fotografias Tamaño Carnet</h5>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites/4fotografias.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>8</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">2 CD'S</h5>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites/2cdsopticos.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow h-100 w-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>9</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">PAGO CBP CONSEJO NACIONAL</h5>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites/Depositocbpnacional.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Número de cuenta</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>10</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">PAGO CBP CONSEJO REGIONAL</h5>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites//Depositocbpregional.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Número de cuenta</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>   
    </div>

    <div class="container-descripcion mt-0">
        <h6>Procedimientos</h6>
        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">Todos los requisitos deberan ser ingresados en por mesa de partes y seran verificados.</p>
            </li>
            <li class="item-coleg">
                <p class="pl-3">Luego de su revisión del Consejo Directivo Nacional, se realizará la inscripción del nuevo colegiado y efectuará el registro correspondiente en el Libro de Inscripciones del Colegio de Biólogos del Perú..</p>
            </li>
            <li class="item-coleg">
                <p class="pl-3">El expediente aprobado será devuelto al Consejo Regional -rubricado por el Decano Nacional y la Secretaria Nacional- acompañado de los siguientes distintivos oficiales:</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4">
            <li><p class="ml-4">Diploma de Colegiación con un número de colegiatura.</p></li>
            <li><p class="ml-4">Carnet de colegiado.</p></li>
            <li><p class="ml-4">Medalla de colegiado.</p></li>
            <li><p class="ml-4">Sello personal de colegiado.</p></li>
            <li><p class="ml-4">Solapero de colegiado.</p></li>
        </ul>
        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">El trámite de Colegiatura tiene una duración de 10 a 15 días para la obtención del Codigo (CBP), con el cual podra realizar cualquier trámite como colegiado.</p>
            </li>
            <li class="item-coleg">
                <p class="pl-3">De acuerdo al Art. 57e del Reglamento, el Consejo Regional programará la ceremonia de Juramentación e Incorporación de colegiados, ocasión en que se entregará los Distintivos Oficiales de la Orden.</p>
            </li>
        </ul>
        <div class="container-important-coleg pl-3 pr-3 pt-2 pb-2 mb-5 position-relative d-flex justify-content-center align-items-center">
            <i class="fa fa-exclamation-circle color-principal" aria-hidden="true"></i>
            <h6 class="mb-0 color-principal pl-2">Los Distintivos se entregaran en la CEREMONIA DE COLEGIATURA no obstante, si se requiere del sello y/o carnet, debera presentar una solicitud dirigida al Decano Regional</h6>
        </div>
    </div>
</div>