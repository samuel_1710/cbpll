<div class="container mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">Registro Nacional de Biólogos Especialistas</h2>
    <hr class="shadow-sm pb-1 border-0">
    
    <div class="container-card-colegiatura mt-5">
        <div class="row mb-0">
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>1</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Solicitud Decano Nacional</h5>
                        <a href="https://drive.google.com/open?id=1-RlIQFP2Hj-WBZliWIIbYEX8JEK50SD2" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>2</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Ficha Inscripción y Anexo 2</h5>
                        
                        <a href="https://drive.google.com/open?id=1PFEf5E4x9VE1sJ_katwWYS8GpIUczlZY" target="_blank">
                            <button type="button" class="btn btn-extra-card text-white btn-sm mt-2">
                            ANEXO N°2
                            </button>
                        </a>
                        
                        <a href="https://drive.google.com/open?id=1JY_RX-wqr7gmcu4gCQYuxuLuW5Qgc57x" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Ficha Inscripción</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>3</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Certificado Habilidad Profesional</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>4</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Dos (2) copias Diploma de Colegiatura</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/2copiasdiplomacolegiatura.jpg" target="_blank" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>5</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Dos (2) copias del Certificado de Estudios de la Segunda Especialidad</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/2copiascertificadoestudios.jpg" target="_blank" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>6</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Dos (02) copias del Diploma que acredita su Título de la Segunda Especialidad</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/2copiasdiplomatitulo.jpg" target="_blank" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>7</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Pago CBP Consejo Nacional</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/pagocolegionacional.jpg" target="_blank" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Número de cuenta</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>8</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Pago CBP Consejo Regional</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/pagocolegioregional.jpg" target="_blank" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Número de cuenta</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>   
    </div>

    <div class="container-descripcion mt-0">
        <h6>Resolución</h6>
        <ul class="container-item-tramites mb-1">
            <li>
                <p class="pl-3 mb-0">DE LA RESOLUCIÓN DE CONSEJO DIRECTIVO NACIONAL</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4 mb-4">
            <li><a href="https://drive.google.com/open?id=1xmrNrYyF1FnYMb7zQAUKgfz8gdcJJrc-" target="_blank"><p class="ml-4 mb-0">N° 004-2013-CDN-CBP</p></a></li>
            <li><a href="https://drive.google.com/open?id=1NAxywjXcOWKdZR2coojILh61fwcCeo16" target="_blank"><p class="ml-4 mb-0">N° 003-2014-CDN-CBP</p></a></li>
            <li><a href="https://drive.google.com/open?id=1Oac-IzRIw7pZ9rE12U6wS4myeCnJ2omF" target="_blank"><p class="ml-4 mb-0">N° 020-2016-CDN-CBP</p></a></li>
        </ul>
        <h6>Procedimientos</h6>
        <ul class="container-item-tramites">
            <li>
                <p class="pl-3">Se presentará el expediente con los requisitos establecidos al Consejo Regional donde se encuentra colegiado</p>
            </li>
            <li>
                <p class="pl-3">El Colegio Regional valida la información y traslada el expediente con una carta al Consejo Directivo Nacional</p>
            </li>
            <li>
                <p class="pl-3">El Consejo Directivo Nacional revisa expediente si no hay observación procede con el registro del Título de segunda especialidad, si hubiera una o varias observaciones retorna al Colegio Regional para el levantamiento de las mismas.</p>
            </li>
            <li>
                <p class="pl-3">El Consejo Directivo Nacional emite Certificado de registro de la segunda especialidad, resolución correspondiente y el sticker oficial con numero de RNBE, para luego ser enviado al Colegio Regional.</p>
            </li>
            <li>
                <p class="pl-3">El Colegio Rregional coordina con el colegiado la entrega de los documentos mencionados en el punto anterior.</p>
            </li>
        </ul>
    </div>
</div>