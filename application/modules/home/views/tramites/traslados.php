<div class="container mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">Procedimiento Traslado Sede</h2>
    <hr class="shadow-sm pb-1 border-0">
    
    <div class="container-card-colegiatura mt-5">
        <div class="row mb-0">
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>1</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Solicitud Traslado</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/solicitudsimple.jpg" data-lightgallery="item" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>2</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Constancia de no Adeudo</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>3</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Ficha Actualización de datos</h5>
                        <a href="https://drive.google.com/open?id=1JnM8Nrfwpi5uFRZLL0yVD5b3voJ0jCwU" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>4</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Pago CBP Consejo Nacional</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/pagotraslado.jpg" data-lightgallery="item" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Número de cuenta</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>   
    </div>

    <div class="container-descripcion mt-0">
        <h6>Resolución</h6>
        <ul class="container-item-tramites mb-1">
            <li>
                <p class="pl-3 mb-0">RESOLUCIÓN DE CONSEJO NACIONAL Nº 040-2013-CBP-CN</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4 mb-4">
            <li><p class="ml-4 mb-0">
                El Acuerdo 011-2013, establece que el precio en los próximos 3 meses será de S/.20.00 
                (veinte nuevos soles) por promoción a partir de la emisión de la resolución correspondiente 
                y luego quedará establecido en S/. 50.00 (cincuenta nuevos soles). Se acuerda que de existir 
                una deuda, esta se mantendría en la regional anterior y que para efectos de su traslado deberá 
                cancelar o hacer un refinanciamiento de la misma a fin de estar expedito para el traslado a una 
                nueva regional, el CDN desarrollará el procedimiento de este servicio.
            </p></li>
        </ul>
    </div>
</div>