<div class="container mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">CARNET DE COLEGIADO (DUPLICADO O RENOVACIÓN)</h2>
    <hr class="shadow-sm pb-1 border-0">
    
    <div class="container-card-colegiatura mt-5">
        <div class="row mb-0">
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>1</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Solicitud Simple</h5>
                        <a href="https://drive.google.com/open?id=1JnM8Nrfwpi5uFRZLL0yVD5b3voJ0jCwU" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>2</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Ficha de Actualizacón de datos</h5>
                        <a href="https://drive.google.com/open?id=1JnM8Nrfwpi5uFRZLL0yVD5b3voJ0jCwU" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Descargar</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>3</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Un CD</h5>
                        <a href="<?php echo base_url() ?>static/website/dist/images/tramites/solicitudsimple.jpg" data-lightgallery="item" target="_blank">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Leer Importante</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-5 container-card-tramites">
                <div class="card shadow w-100 h-100">
                    <div class="container-num-order position-absolute fondo-principal text-white d-flex justify-content-center align-items-center rounded-circle font-weight-bold">
                        <span>4</span>
                        <div class="animation-num-circle"></div>
                    </div>
                    <div class="container-title-card card-body text-success pt-4 pb-5">
                        <h5 class="card-title mb-0">Pago CBP Consejo Nacional</h5>
                        <a target="_blank" href="<?php echo base_url() ?>static/website/dist/images/tramites/pagotraslado.jpg" data-lightgallery="item">
                            <button type="button" class="btn btn-footer fondo-principal text-white btn-sm position-absolute">Número de cuenta</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>   
    </div>

    <div class="container-descripcion mt-0">
        <h6>Importante</h6>
        <ul class="container-item-tramites mb-1">
            <li>
                <p class="pl-3 mb-0">El Carnet que acredita la membresía en la institución es un documento oficial que debe ser emitido por el Consejo Directivo Nacional toda vez que en los archivos del Consejo Nacional obran los expedientes originales de colegiación, así como los tomos originales del Libro de Inscripciones del Colegio de Biólogos del Perú donde consta el registro histórico de todas las colegiaciones y traslados de sede regional efectuados desde la fundación de la institución en 1972.</p>
            </li>
            <li>
            <p class="pl-3 mb-0">La pertenencia del biólogo colegiado a uno de los Consejos Regionales es señalada en el Carnet y es un reflejo de su inscripción original o del traslado de sede que el colegiado pueda haber hecho posteriormente y estén debidamente inscritos en el Libro de Registros y Títulos que obra en los archivos del Consejo Nacional.</p>
            </li>
        </ul>
    </div>
</div>