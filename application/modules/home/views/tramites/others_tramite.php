<div class="container container-main-colegiatura mb-5">
    <h2 class="font-weight-bold color-principal mt-5 mt-0 pb-0">TRAMITES</h2>
    <hr class="shadow-sm pb-1 border-0">

    <div class="container-descripcion">
        <h6 class="mt-5">Constancia de Habilitación</h6>
        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">RESOLUCIÓN DE CONSEJO NACIONAL Nº 040-2013-CBP-CN (07 de junio del 2013)</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4">
            <li>
                <p class="ml-4">
                    El Acuerdo 014-2013, establece que La CONSTANCIA DE HABILITACIÓN sólo es válida el original y su validez es de 3 meses siempre que se encuentre al día en sus cuotas mensuales. Así mismo establece que el pago en todos los Consejos Regionales será de S/.15.00 (quince nuevos soles) por cada Certificado de habilidad, 
                    a partir de emisión de la resolución correspondiente.
                </p>
            </li>
            <li>
                <p class="ml-4">El Acuerdo 019-2013, establece que a partir de enero 
                    2014 se eleva la cuota mensual que pagan los colegiados a S/:15 (quince nuevos soles).
                </p>
            </li>
        </ul>
        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">PROCEDIMIENTO</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4">
            <li>
                <p class="ml-4">
                El pago de las cotizaciones mensuales es una obligación de cada biólogo colegiado que le permite mantenerse en situación de habilitación. Ello le confiere el derecho de contar con todos los servicios del Colegio de Biólogos del Perú y asimismo cumplir con la Ley 28847, Ley de Trabajo del Biólogo, que obliga a todo 
                profesional biólogo a estar debidamente habilitado para poder ejercer su profesión en el territorio nacional.
                </p>
            </li>
            <li>
                <p class="ml-4">Para estar habilitado el profesional biólogo debe hacer pago puntual de sus cotizaciones mensuales. Este pago de periodicidad mensual es de S/. 10.00 (diez nuevos soles) hasta diciembre del 2013 a partir de enero 2014 se eleva a S/:15.00 (quince nuevos soles) se realiza ante el Consejo Regional donde 
                    se encuentra registrado el biólogo colegiado (basado en el Art. 61e del Reglamento General del CBP).
                </p>
            </li>
        </ul>
        <ul class="container-subitems-tramites-3 pl-5">
            <li>
                <p class="ml-4">
                    a) La CONSTANCIA DE HABILITACIÓN será solicitada a través de una solicitud simple en cada Consejo Regional del cual pertenece el Colegiado, que certifica ante terceros que el profesional biólogo colegiado no tiene deudas u obligaciones pendientes con la institución y que mantiene al día el pago de sus cotizaciones mensuales o las tienen financiadas en un Compromiso de Pago de ser el caso que tenga deudas acumuladas.
                </p>
            </li>
            <li>
                <p class="ml-4">b) El costo de la CONSTANCIA DE HABILITACIÓN es de S/.15.00 (quince nuevos soles) por cada Certificado de habilidad en todos los Consejo Regionales.
                </p>
            </li>
            <li>
                <p class="ml-4">c)La CONSTANCIA DE HABILITACIÓN es válida solo el original y su validez es de 3 meses siempre que se encuentre al día en sus cuotas mensuales:
                </p>
            </li>
        </ul>

        <h6 class="mt-5">Cotizaciones Mensuales</h6>

        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">Procedimiento aprobado en Sesión Ordinaria de Consejo Nacional del 9 de junio 2007</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4">
            <li>
                <p class="ml-4">
                El pago de las cotizaciones mensuales es un ejercicio por el cual cada biólogo colegiado puede mantenerse en situación de habilitación. Ello le confiere el derecho de contar con todos los servicios del Colegio de Biólogos del Perú y asimismo cumplir con la Ley 28847, Ley de Trabajo del Biólogo, que exige a todo profesional biólogo a estar debidamente habilitado para poder ejercer su profesión en el territorio nacional.
                </p>
            </li>
            <li>
                <p class="ml-4">Este pago de periodicidad mensual se realiza ante el Consejo Regional donde se encuentra registrado el biólogo colegiado (basado en el Art. 61°e del Reglamento General del CBP).
                </p>
            </li>
        </ul>
        <ul class="container-subitems-tramites-3 pl-5">
            <li>
                <p class="ml-4">
                a) La cuota mensual que todo biólogo colegiado deberá aportar, según el Art. 61e del Reglamento General del CBP, será de S/. 10.00 (diez soles). Y conforme al Artículo Tercero de la Resolución N° 40-2013-CBP-CN del 07 de junio, en concordancia con el Acuerdo 019-2013 del I Consejo Nacional del 1° de junio del 2013, la aportación mensual a partir del 1° de enero del 2014 será de S/. 15.00 (quince nuevos soles)
                </p>
            </li>
            <li>
                <p class="ml-4">b)Este pago debe hacerse en la sede del Consejo Regional respectivo o en su cuenta bancaria, según lo dispuesto autónomamente por cada Consejo Regional. Asimismo, los biólogos colegiados pueden autorizar a su empleador a que realice descuentos mensuales por planilla.</p>
            </li>
            <li>
                <p class="ml-4">c)Cada Consejo Regional, según Acuerdo del Consejo Nacional del Colegio de Biólogos del Perú, en su sesión del 9 de junio de 2007 que modula el Art. 113b del Reglamento, deberá transferir al Consejo Nacional el 10% del total de las cotizaciones mensuales que haya recaudado en cada mes calendario. Cada Consejo Regional hará este abono dentro de los quince días calendarios del mes inmediato siguiente en la cuenta de ahorros Soles del Banco Continental (a nombre del Colegio de Biólogos del Perú, Consejo Nacional) No 0011-0160-91-0200102100 .
                </p>
            </li>
        </ul>
        
        <h6 class="mt-5">Constancia de Colegiación</h6>
        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">1. La CONSTANCIA DE COLEGIACIÓN es un documento emitido y otorgado por el Consejo Directivo Nacional.</p>
            </li>
            <li class="item-coleg">
                <p class="pl-3">2. Para obtener una CONSTANCIA DE COLEGIACIÓN se requiere presentar:</p>
            </li>
        </ul>
        <ul class="container-subitems-tramites pl-4">
            <li>
                <p class="ml-4">
                Solicitud simple dirigida al Decano Nacional
                </p>
            </li>
            <li>
                <p class="ml-4">Voucher del pago indicado en el Punto 3.</p>
            </li>
        </ul>
        <ul class="container-item-tramites">
            <li class="item-coleg">
                <p class="pl-3">3. Pago de S/. 50.00 (cincuenta nuevos soles) en la cuenta de ahorros Soles del Banco Continental (a nombre del Colegio de Biólogos del Perú, Consejo Nacional) No 0011-0160-91-0200102100.</p>
            </li>
            <li class="item-coleg">
                <p class="pl-3">4. El colegiado podrá tramitar la CONSTANCIA directamente ante el Consejo Directivo Nacional o a través de su respectivo Consejo Regional, para lo que el colegiado deberá reembolsar el costo del envío al Consejo Regional.</p>
            </li>
        </ul>
    </div>
</div>