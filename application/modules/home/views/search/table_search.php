<table id="table_search" class="table">
    <thead>
        <td>N°</td>
        <td>Código CP</td>
        <td>Nombres y Apellidos</td>
        <td class="text-center">Estado</td>               
    </thead>
    <tbody>
        <?php if(count($colegiados)>0){?>
            <?php $i=1;foreach ($colegiados as $colegiado) { ?>
            <tr>
                <td><?=$i;?></td>
                <td><?php echo $colegiado->CodigoColegiado;?></td>
                <td><?php echo $colegiado->ApellidosPaterno." ".$colegiado->ApellidoMaterno." ".$colegiado->NombresUsuario;?></td>
                <td class="text-center">
                  <?php 
                    echo status_colegiado_web($colegiado->type_member,$colegiado->FechaUltimoPago,$colegiado->EstadoColegiado);
                  ?>
                </td>
            </tr>   
            <?php $i++;} ?>
        <?php } else {?>
            <tr>
                <td class="text-center" colspan="4">No se encontraron resultados en su búsqueda</td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<input type="hidden" id="cantidad" name="cantidad" value="<?php echo count($colegiados)>0?'1':'0'?>">
<script src="<?php echo link_static('website/dist/js/cb_table_search.min.js'); ?>"></script>