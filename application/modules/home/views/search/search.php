<section class="breadcrumbs-custom bg-image context-dark" style="background-image: url(<?php echo URL_STATIC?>website/dist/images/breadcrumbs-image-1.jpg);">
	<div class="breadcrumbs-custom-inner">
		<div class="container breadcrumbs-custom-container">
			<div class="breadcrumbs-custom-main">
				<h6 class="breadcrumbs-custom-subtitle title-decorated">CBP La Libertad</h6>
				<h1 class="breadcrumbs-custom-title">Búsqueda</h1>
			</div>
			<ul class="breadcrumbs-custom-path">
				<li><a href="<?php echo base_url(); ?>busqueda">Inicio</a></li>
				
				<li><a href="<?php echo base_url(); ?>busqueda">Colegiado</a></li>
				<li class="active">Búsqueda de Colegiado</li>
			</ul>
		</div>
	</div>
</section>
<section class="section text-center containerSearch">
    <div class="container">
        <h3 class="wow-outer"><span class="wow slideInDown">Búsqueda de Colegiados</span></h3>
        <div class="row">
            <form id="formSearchColegiado" class="formSearch container-search-colegiado" action="">
                <div class="form-group">
                    <div class="formSearch__item col-12 col-md-1">
                        <label for="type_filter">Buscar por:</label>
                    </div>    
                    <div class="formSearch__item col-12 col-md-3">
                        <select name="type_filter" id="type_filter" name="type_filter" class="form-control">
                            <option value="name">Apellidos y nombres</option>
                            <option value="dni">DNI</option>
                            <option value="cbpll">Código CBP</option>
                        </select>
                    </div>
                    <div class="formSearch__item col-12 col-md-4">
                        <input type="text" placeholder="Ingrese Apellidos y nombres" class="form-control" id="description_filter" name="description_filter" autocomplete="off">
                    </div>
                    <div class="formSearch__item col-12 col-md-4">
                        <button type="submit" class="button button-primary button-winona">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12 text-left hide title_resultados">Resultados de la búsqueda</div>
            <div class="col-md-12" id="content_table"><?php //$this->load->view('search/table_search')?></div>
        </div>
    </div>
</section>
<script src="<?php echo link_static('website/dist/js/cb_search.min.js'); ?>"></script>