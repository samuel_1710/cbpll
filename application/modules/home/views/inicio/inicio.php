<div class="mb-2">
    <div class="owl-carousel owl-carousel-home">
        <?php if(isset($images) && count($images)>0){?>
            <?php foreach ($images as $image) { ?>
                <div class="item">
                    <img src="<?php echo URL_UPLOADS. 'slidehome/'.$image->name;?>" alt="Banner">
                </div>
            <?php } ?>
        <?php } else { ?>
        <div class="item">
            <img src="<?php echo URL_STATIC?>website/dist/images/slider1920.jpg" alt="">
        </div>
        <?php }?>
    </div>
</div>
<div class="container my-4">
    <div class="card-group">
        <a href="<?php echo base_url() ?>resoluciones" class="card cardCustom">
            <div class="card-body">
                <p class="card-text text-center"><i class="far fa-file-alt"></i></p>
                <h5 class="card-title text-center">Boletín Informativo</h5>
            </div>
        </a>
        <a href="<?php echo base_url() ?>busqueda" class="card cardCustom">
            <div class="card-body">
                <p class="card-text text-center"><i class="fas fa-search-plus"></i></p>
                <h5 class="card-title text-center">Consultar Habilitación</h5>
            </div>
        </a>
        <a href="<?php echo base_url() ?>ficha-virtual" class="card cardCustom">
            <div class="card-body">
                <p class="card-text text-center"><i class="fas fa-address-card"></i></p>
                <h5 class="card-title text-center">Ficha Virtual</h5>
            </div>
        </a>
        <a href="<?php echo base_url() ?>admin/login" class="card cardCustom">
            <div class="card-body">
                <p class="card-text text-center"><i class="fas fa-address-book"></i></p>
                <h5 class="card-title text-center">Ir a mi perfil</h5>
            </div>
        </a>
    </div>
</div>
<?php if(count($events)>0){?>
<div class="container my-2">
    <h2 class="mb-0 pb-0 rounded container-title-comisiones">
        <span class="font-weight-bold pt-2 pb-2 pl-3 pr-3">Últimos Eventos y Actividades</span>
    </h2>
    <div class="container-events mt-4 mb-2">
        <div class="row">
        <?php foreach ($events as $event) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12 mb-4">
                <a href="<?php echo base_url() ?>evento-actividad/<?php echo $event->slug?>" class="card-link text-light">
                    <div class="card mb-3 shadow h-100 w-100 content-card-events">
                        <div class="container-img-project">
                            <img class="card-img-top" src="<?php echo URL_UPLOADS."events/".$event->image;?>" alt="<?php echo $event->title;?>">
                        </div>
                        <div class="card-body content-body-events">
                            <p class="card-text text-secondary text-secondary font-weight-light"><?php echo $event->horario?></p>
                            <h5 class="card-title text-dark font-weight-bold mb-0 mt-0"><?php echo $event->title;?></h5>
                        </div>
                        <div class="card-body fondo-principal pt-2 pb-2 text-right footer-events">
                            ... ver más
                        </div>
                    </div>
                </a>
            </div>
        <?php }?>
        </div>
    </div>
</div>
<?php }?>
<div class="container my-2">
    <h2 class="mb-0 pb-0 rounded container-title-comisiones">
        <span class="font-weight-bold pt-2 pb-2 pl-3 pr-3">Infraestructura CPLL</span>
    </h2>
    <div class="card-columns mt-4 mb-2">
        <?php if(isset($infraestructura) && count($infraestructura)>0){ ?>
            <?php foreach ($infraestructura as $infra) { ?>
                <div class="card">
                    <img class="card-img-top" src="<?php echo URL_UPLOADS. 'infraestructura/'.$infra->name;?>" alt="Convenios">
                </div>
            <?php } ?>
        <?php } ?>
        <?php /*<div class="card">
            <img class="card-img-top" src="<?php echo URL_STATIC; ?>website/dist/images/infraestructura/auditorio1-1original.jpg" alt="Auditorio Principal">
        </div>
        <div class="card">
            <img class="card-img-top" src="<?php echo URL_STATIC; ?>website/dist/images/infraestructura/auditorio2-1preview.jpg" alt="Auditorio 2">
        </div>
        <div class="card">
            <img class="card-img-top" src="<?php echo URL_STATIC; ?>website/dist/images/infraestructura/sala2.jpg" alt="Salon 2">
        </div>
        <div class="card">
            <img class="card-img-top" src="<?php echo URL_STATIC; ?>website/dist/images/infraestructura/sala3.jpg" alt="Salon 3">
        </div>
        <div class="card">
            <img class="card-img-top" src="<?php echo URL_STATIC; ?>website/dist/images/infraestructura/sala4.jpg" alt="Salon 4">
        </div>
        <div class="card">
            <img class="card-img-top" src="<?php echo URL_STATIC; ?>website/dist/images/infraestructura/losa2preview.jpg" alt="Salon 4">
        </div> */ ?>
    </div>
</div>

<div class="container my-2 mb-4">
    <h2 class="mb-0 pb-0 rounded container-title-comisiones">
        <span class="font-weight-bold pt-2 pb-2 pl-3 pr-3">Convenios CBPLL</span>
    </h2>
    <div class="owl-carousel owl-carousel-convenios">
        <?php if(isset($convenios) && count($convenios)>0){ ?>
            <?php foreach ($convenios as $convenio) { ?>
                <div class="item">
                    <img class="owl-lazy" data-src="<?php echo URL_UPLOADS. 'convenios/'.$convenio->name;?>" alt="Convenios">
                </div>
            <?php } ?>
        <?php } ?>
        <?php /*<div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-1-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-2-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-3-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-4-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-5-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-6-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-7-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-8-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-9-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-10-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-11-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-12-370x256.jpg" alt="">
        </div>
        <div class="item">
            <img class="owl-lazy" data-src="<?php echo URL_STATIC?>website/dist/images/convenios/grid-layout-13-370x256.jpg" alt="">
        </div> */?>
    </div>
</div>

<?php if(isset($comunications) && count($comunications)>0){?>
<div class="modal fade modal-comision" id="modal_main_comunicado" tabindex="-1" role="dialog" aria-labelledby="modal_comisionLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute fixed-top">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="owl-carousel owl-carousel-modal">
                    <?php $i=1;foreach ($comunications as $comunication) { ?>
                        <div class="item">
                            <img class="image-responsive" src="<?php echo URL_UPLOADS. 'slidehome/'.$comunication->image;?>" alt="Comunicado <?php echo $i;?>">
                        </div>
                    <?php $i++;} ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }?>
<script src="<?php echo link_static('website/dist/js/cb_home.min.js'); ?>"></script>