<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Colegiado extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }

		$this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Users_model', 'user_model');
        $this->load->model('Grades_model', 'grade_model');
        $this->load->model('Login_model', 'login_model');
        $this->load->model('Pagoscolegiatura_model', 'pagos_model');
        $this->load->model('Ubigeo_model', 'obj_ubigeo');
    }
	public function index()
	{
        //$this->load->view('search');
        $message = "mostrando message";
        $this->master_admin_tmp->set("message", $message);
        $this->master_admin_tmp->render('colegiado/search');
	}
	public function filter()
	{
        if ($this->input->is_ajax_request()) {
			$type = $this->input->post('type_filter');
			$description = $this->input->post('description_filter');
			$result=$this->colegiado_model->filter($type,$description);
			$data['status'] = 'SUCCESS';
			$data['colegiados'] = $result;
			$data['message'] = 'Se registró correctamente.';
			echo $this->load->view('colegiado/search_table',$data,true);
			
        	exit;
        }

	}
	public function reset_password($idColegiado)
	{
        $colegiado=$this->colegiado_model->get_colegiado_by_id($idColegiado);
        
		$this->master_admin_tmp->set("colegiado", $colegiado);
		$this->master_admin_tmp->render('colegiado/change_password_admin');
	}
    public function editar_colegiado($idColegiado)
	{
        $colegiado=$this->colegiado_model->get_colegiado_by_id($idColegiado);
        $grades=$this->grade_model->get_grades_by_colegiado($idColegiado);
        $obj_departamento = $this->obj_ubigeo->get_departments();
        $obj_provincia = $this->obj_ubigeo->get_provinces($colegiado->DepartamentoUsuario);
        $obj_distrito = $this->obj_ubigeo->get_districts($colegiado->ProvinciaUsuario);

        $this->master_admin_tmp->set('obj_departamento', $obj_departamento);
        $this->master_admin_tmp->set('obj_provincia', $obj_provincia);
        $this->master_admin_tmp->set('obj_distrito', $obj_distrito);
		$this->master_admin_tmp->set("colegiado", $colegiado);
        $this->master_admin_tmp->set("grades", $grades);
		$this->master_admin_tmp->render('colegiado/edit_colegiado');
	}
	public function ajax_editar_colegiado()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $speciality = $this->input->post('speciality')==='Otros'?$this->input->post('speciality_other'):$this->input->post('speciality');
            $this->db->trans_begin();
            $result=$this->user_model->update($this->input->post("user_id"),[
                'ApellidosPaterno' => trim($this->input->post("ape_pat")),
                'ApellidoMaterno' => trim($this->input->post("ape_mat")),
                'NombresUsuario' => trim($this->input->post("name")),
                'FechaNacimiento' =>  $this->input->post("date_nac"),
                'TipoDocumento' =>  $this->input->post("type_doc"),
                'NumeroDocumento' => $this->input->post("nro_doc"),
                'GeneroUsuario' => $this->input->post('genero'),
                'EstadoCivil' => $this->input->post('estado_civil'),
                'NacionalidadUsuario' => $this->input->post("nationality"),
                'TipoSangre' => $this->input->post("group_sangre"),
                'DireccionUsuario' =>  $this->input->post("address"),
                'DepartamentoUsuario' =>  $this->input->post("dpto"),
                'ProvinciaUsuario' => $this->input->post("prov"),
                'DistritoUsuario' => $this->input->post("district"),
                'TelefonoUsuario' => $this->input->post("phone"),
                'CelularUsuario' =>  $this->input->post("celphone"),
                'EmailUsuario' =>  $this->input->post("email"),
                'Linkweb' => $this->input->post('linkweb'),
                'speciality' => $speciality
            ]);
            $result2=$this->colegiado_model->update($this->input->post("colegiado_id"),[
                'FechaColegiado' => $this->input->post('date_colegiatura'),
                'CodigoColegiado' => $this->input->post('cod_cbp'),  
            ]);

            $grades =  $this->input->post('grade[]');
            $namegrades =  $this->input->post('namegrade[]');
            if( !is_null($namegrades) && !is_null($grades)){
                $i =0;
                $this->grade_model->delete_grades($this->input->post("colegiado_id"));
                if( count($namegrades) > 0 && count($grades) > 0){
                    foreach ($grades as $grade) {
                        $this->grade_model->insert([
                            'grade' => $grade,
                            'name_grade' => $namegrades[$i],
                            'colegiado_id' => $this->input->post("colegiado_id"),
                        ]);
                        $i++;
                    }
                }
            }
            else{
                $this->grade_model->delete_grades($this->input->post("colegiado_id"));
            }

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $response['status'] = 'ERROR';
                $response['message'] = 'Hubo un inconveniente al editar el colegiado.';
            }
            else {
                $this->db->trans_commit();
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se editó correctamente.'; 
            }
            /*if($this->input->post('cod_cbp')!=""){
                $result2=$this->colegiado_model->update($this->input->post("colegiado_id"),[
                    'FechaColegiado' => $this->input->post('date_colegiatura'),
                    'CodigoColegiado' => $this->input->post('cod_cbp'),  
                ]);
            }*/
        }
        echo json_encode($response);
        exit;
	}


	public function listado_virtual()
	{
		$cards=$this->colegiado_model->list_register_virtual();
		$this->master_admin_tmp->set("cards", $cards);
		$this->master_admin_tmp->render('colegiado/register_virtual_list');
	}
	public function verify_colegiado_virtual($idColegiado)
	{
        $colegiado=$this->colegiado_model->get_colegiado_by_id($idColegiado);
        if(isset($colegiado)){
            $obj_departamento = $this->obj_ubigeo->get_departments();
            $obj_provincia = $this->obj_ubigeo->get_provinces($colegiado->DepartamentoUsuario);
            $obj_distrito = $this->obj_ubigeo->get_districts($colegiado->ProvinciaUsuario);

            $this->master_admin_tmp->set('obj_departamento', $obj_departamento);
            $this->master_admin_tmp->set('obj_provincia', $obj_provincia);
            $this->master_admin_tmp->set('obj_distrito', $obj_distrito);
        }
		$this->master_admin_tmp->set("colegiado", $colegiado);
		$this->master_admin_tmp->render('colegiado/verify_colegiado');
	}
	public function ajax_verificar_colegiado()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $speciality = $this->input->post('speciality')==='Otros'?$this->input->post('speciality_other'):$this->input->post('speciality');
            $result=$this->user_model->update($this->input->post("user_id"),[
                'ApellidosPaterno' => trim($this->input->post("ape_pat")),
                'ApellidoMaterno' => trim($this->input->post("ape_mat")),
                'NombresUsuario' => trim($this->input->post("name")),
                'FechaNacimiento' =>  $this->input->post("date_nac"),
                'TipoDocumento' =>  $this->input->post("type_doc"),
                'NumeroDocumento' => $this->input->post("nro_doc"),
                'GeneroUsuario' => $this->input->post('genero'),
                'EstadoCivil' => $this->input->post('estado_civil'),
                'NacionalidadUsuario' => $this->input->post("nationality"),
                'TipoSangre' => $this->input->post("group_sangre"),
                'DireccionUsuario' =>  $this->input->post("address"),
                'DepartamentoUsuario' =>  $this->input->post("dpto"),
                'ProvinciaUsuario' => $this->input->post("prov"),
                'DistritoUsuario' => $this->input->post("district"),
                'TelefonoUsuario' => $this->input->post("phone"),
                'CelularUsuario' =>  $this->input->post("celphone"),
                'EmailUsuario' =>  $this->input->post("email"),
                'Linkweb' => $this->input->post('linkweb'),
                'speciality' => $speciality 
            ]);
            $result2=$this->colegiado_model->update($this->input->post("colegiado_id"),[
                'EstadoColegiado' => 1
            ]);
            $result3=$this->login_model->insert([
                'PasswordLogin' => md5($this->input->post("nro_doc")),
                'Usuarios_idUsuarios' => $this->input->post("user_id"),
            ]);
			//if($result && $result2){
            	$response['status'] = 'SUCCESS';
				$response['message'] = 'Se verificó correctamente.';
			//}
        }
        echo json_encode($response);
        exit;
	}


	public function registro_virtual()
	{
        $obj_departamento = $this->obj_ubigeo->get_departments();
        $this->master_admin_tmp->set('obj_departamento', $obj_departamento);        
        $this->master_admin_tmp->render('colegiado/register_virtual');
	}
	public function insert_colegiado_admin(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {

            $nroDoc = $this->input->post("nro_doc");
            $typeDoc = $this->input->post("type_doc");
            $colegiado = $this->colegiado_model->get_colegiado_by_document($typeDoc,$nroDoc);
            
            if(count($colegiado)>0){
                $response = [
                    'status' => 'ERROR',
                    'message' => 'La persona ya ha sido registrada.'
                ];
            }
            else {
                $result=$this->user_model->insert([
                    'ApellidosPaterno' => trim($this->input->post("ape_pat")),
                    'ApellidoMaterno' => trim($this->input->post("ape_mat")),
                    'NombresUsuario' => trim($this->input->post("name")),
                    'FechaNacimiento' =>  $this->input->post("date_nac"),
                    'TipoDocumento' =>  $typeDoc,
                    'NumeroDocumento' => $nroDoc,
                    'GeneroUsuario' => $this->input->post('genero'),
                    'EstadoCivil' => $this->input->post('estado_civil'),
                    'NacionalidadUsuario' => $this->input->post("nationality"),
                    'TipoSangre' => $this->input->post("group_sangre"),
                    'DireccionUsuario' =>  $this->input->post("address"),
                    'DepartamentoUsuario' =>  $this->input->post("dpto"),
                    'ProvinciaUsuario' => $this->input->post("prov"),
                    'DistritoUsuario' => $this->input->post("district"),
                    'TelefonoUsuario' => $this->input->post("phone"),
                    'CelularUsuario' =>  $this->input->post("celphone"),
                    'EmailUsuario' =>  $this->input->post("email"),
                    'Linkweb' => $this->input->post('linkweb')
                ]);
                $this->colegiado_model->insert([
                    'FechaColegiado' => null,
                    'EstadoColegiado' => 0,
                    'CodigoColegiado' => null,
                    'Usuarios_idUsuarios' => $result
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se registró correctamente.';
            }
        }
        echo json_encode($response);
        exit;
    }
    public function delete_register_virtual(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {

            $user_id = $this->input->post("user_id");
            $result=$this->user_model->update($this->input->post("user_id"),[
                'status_value' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se registró correctamente.';
        }
        echo json_encode($response);
        exit;
    }




	public function registro_cuotas()
	{
		$this->master_admin_tmp->render('colegiado/register_cuota');
    }
    public function edit_cuotas($id = null)
	{
        if($id){
            $pago=$this->pagos_model->get_pagos_by_id($id);
            $this->master_admin_tmp->set("pago", $pago);
            $this->master_admin_tmp->set("cuota_id", $id);
            $this->master_admin_tmp->render('colegiado/edit_cuota');
        }
        else{
            redirect('admin/buscador');
        }
		
	}
	public function status_financiero($idColegiado)
	{
        if($this->session->session_cbpll["type"] == "Finanzas"){
            $pagos=$this->pagos_model->get_pagos_by_colegiado($idColegiado,10);
        }
        else{
            $pagos=$this->pagos_model->get_pagos_by_colegiado($idColegiado);
        }
       
		$colegiado=$this->colegiado_model->get_colegiado_by_id($idColegiado);
		$this->master_admin_tmp->set("colegiado", $colegiado);
		$this->master_admin_tmp->set("pagos", $pagos);

		$this->master_admin_tmp->render('colegiado/status_financiero');
	}
	public function ajax_get_colegiados()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
			$colegiado=$this->colegiado_model->get_colegiados($this->input->post("term"));
			$response['status'] = 'SUCCESS';
			$response['message'] = 'Petición válida';
			$response['data'] = $colegiado;
		}
		echo json_encode($response);
        exit;
	}
	public function ajax_insert_cuota()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            if($_FILES["voucher_file"]["name"] != ""){
                if($this->upload->do_upload('voucher_file')){
                    $data = array("upload_data" => $this->upload->data());
                    $result=$this->pagos_model->insert([
                        'FechaPagada' => $this->input->post("date_pago"),
                        'FechaVencimiento' => $this->input->post("date_vencimiento"),
                        'monto' => $this->input->post("monto_pago"),
                        'nro_operacion' => $this->input->post('nro_operacion'),
                        'detail' => $this->input->post('detalle_pago'),
                        'voucher_img' => $data['upload_data']['file_name'],
                        'Colegiado_idColegiado' =>  $this->input->post("select_colegiado")
                    ]);
        
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se registró su pago con éxito'; 
                }
                else{
                    $response['message'] = $this->upload->display_errors();
                }
            }
            else{
                $result=$this->pagos_model->insert([
                    'FechaPagada' => $this->input->post("date_pago"),
                    'FechaVencimiento' => $this->input->post("date_vencimiento"),
                    'monto' => $this->input->post("monto_pago"),
                    'nro_operacion' => $this->input->post('nro_operacion'),
                    'detail' => $this->input->post('detalle_pago'),
                    'voucher_img' => null,
                    'Colegiado_idColegiado' =>  $this->input->post("select_colegiado")
                ]);
                if($result){
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se registró su pago con éxito';
                }
                else{
                    $response['message'] = "Error al insertar pago";
                }
            }
            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_edit_cuota()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            $payment_id = $this->input->post("payment_id");
            $result = $this->pagos_model->update($payment_id,[
                'FechaPagada' => $this->input->post("date_pago"),
                'FechaVencimiento' => $this->input->post("date_vencimiento"),
                'monto' => $this->input->post("monto_pago"),
                'nro_operacion' => $this->input->post('nro_operacion'),
                'detail' => $this->input->post('detalle_pago'),
            ]);
            if($_FILES["voucher_file"]["name"] != ""){
                if($this->upload->do_upload('voucher_file')){
                    $data = array("upload_data" => $this->upload->data());
                    $result = $this->pagos_model->update($payment_id,[
                        'voucher_img' => $data['upload_data']['file_name']
                    ]);
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se registró su pago con éxito'; 
                }
                else{
                    $response['message'] = $this->upload->display_errors();
                }
            }
            else{
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se registró su pago con éxito';
                    //$response['message'] = "Error al editar el pago";
                
            }
            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_delete_cuota(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->pagos_model->update($this->input->post("payment_id"),[
                'status_value' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se registró correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_set_vitalicio(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result2=$this->colegiado_model->update($this->input->post("colegiado_id"),[
                'is_vitalicio' => 1,
                'date_vitalicio' => date('Y-m-d H:i:s')
            ]);;
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se realizó la petición con éxito.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_set_type_member(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result2=$this->colegiado_model->update($this->input->post("colegiado_id"),[
                'type_member' => $this->input->post("type_member"),
                'date_vitalicio' => date('Y-m-d H:i:s')
            ]);;
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se realizó la petición con éxito.';
        }
        echo json_encode($response);
        exit;
    }

    public function ajax_change_password()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
			$usuario_id = $this->input->post("user_id");
            $this->db->trans_begin();
            $this->login_model->update($usuario_id,[
                'PasswordLogin' => md5($this->input->post("new_password")),
            ]);
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
                $response['status'] = 'ERROR';
                $response['message'] = 'Hubo un inconveniente al restablecer contraseña.';
            }
            else {
                $this->db->trans_commit();
                $response['status'] = 'SUCCESS';
				$response['message'] = 'Se actualizó sus datos correctamente.';
            }
        }
        echo json_encode($response);
        exit;
    }
}
