<?php

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
  
        $this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Administrativos_model', 'administrativo_model');
        $this->load->model('Users_model', 'user_model');
    }

    public function login()
    {
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];

        if ($this->input->is_ajax_request()) {
            $usuario = $this->security->xss_clean($this->input->post('usuario'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $hashToken = $this->input->post($this->security->get_csrf_token_name());

            if ($usuario && $password && ($this->security->get_csrf_hash() === $hashToken)) {
                $user = $this->user_model->verify_user($usuario,$password);
                if($user){
                    $name = $user->NombresUsuario.' '.$user->ApellidosPaterno;
                    $response = [
                        'status' => 'SUCCESS',
                        'message' => 'Bienvenido, ' . $name,
                    ];
                    $type = '';
                    $admin = $this->administrativo_model->get_administrativos_by_user_id($user->idUsuarios);
                    if( isset($admin) && $admin !== null){
                        $type = $admin->CargoAdministrativos;
                        $role = $admin->role;
                        /*if($user->NombresUsuario == "Adminweb"){
                            $type = "AdminWeb";
                        }
                        else if($user->NombresUsuario == "Secretaria"){
                            $type = "Secretaria";
                        }
                        else if($user->NombresUsuario == "Finanzas"){
                            $type = "Finanzas";
                        }*/
                    }
                    else{
                        $type = "Colegiado";
                        $role = 'colegiado';
                        $colegiado = $this->colegiado_model->get_colegiado_by_idUsuario($user->idUsuarios);
                    }
                    $response['type'] = $type;
                    $this->session->set_userdata('session_cbpll', [
                        'nombres' => $name,
                        'type' => $type,
                        'role' => $role,
                        'idColegiado' => isset($colegiado)?$colegiado->idColegiado:'',
                        'idUsuario' => $user->idUsuarios
                    ]);

                }
                else {
                    $response['message'] = 'Usuario o Contraseña incorrecto, por favor intentelo nuevamente.';
                }
            }
        }
        echo json_encode($response);
        exit;
    }

    public function index()
    {
        $data['error'] = validation_errors();
        $this->load->view('admin/login',$data);
    }

    public function logout()
    {
        $this->session->unset_userdata('session_cbpll');
        $this->session->sess_destroy();
        redirect('admin/login');
    }

    public function reset()
    {
        $this->master_tmp->render('Customers/Login_reset');
    }


    public function reset_password()
    {
        if ($this->input->is_ajax_request()) {
            $customer_id=$this->input->post('customer_id');
            $password = md5(get_semilla().$this->input->post('password'));

            if ($customer_id!="") {
                $params = ["select" =>"","where" => "customer_id = $customer_id"];
                $obj_customers  = $this->obj_customers->get_search_row($params);
                if ($obj_customers) {
                    $customer_data = [
                        'customer_id' => $customer_id,
                        'password' => $password,
                        'update_create' => date("Y-m-d H:i:s"),
                        'by_update'=> $customer_id
                    ];

                    $this->obj_customers->update($customer_id, $customer_data);

                    $data['print'] ="Actualizacion correcta";
                    $data['message'] = "true";
                } else {
                    $data['print'] ="ERROR: Usuario no exite.";
                    $data['message'] = "false";
                }

            } else {
                $data['print'] ="ERROR: Vuelve Intentar";
                $data['message'] = "false";
            }

            echo json_encode($data);
            exit();
        } else {
            $data['error']="Acesso No permitido";
            $this->load->view('Customers/Template_error',$data);
        }
    }

}
