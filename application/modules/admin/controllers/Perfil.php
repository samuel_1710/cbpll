<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends MY_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }

		$this->load->model('Colegiado_model', 'colegiado_model');
		$this->load->model('Users_model', 'user_model');
        $this->load->model('Login_model', 'login_model');
        $this->load->model('Pagoscolegiatura_model', 'pagos_model');
        $this->load->model('Ubigeo_model', 'obj_ubigeo');
        $this->load->model('Grades_model', 'grade_model');
    }
	public function index()
	{
		$colegiado_id = $this->session->session_cbpll["idColegiado"];
        $colegiado = $this->colegiado_model->get_colegiado_by_id($colegiado_id);
        $grades=$this->grade_model->get_grades_by_colegiado($colegiado_id);
        $obj_departamento = $this->obj_ubigeo->get_departments();
        $obj_provincia = $this->obj_ubigeo->get_provinces($colegiado->DepartamentoUsuario);
        $obj_distrito = $this->obj_ubigeo->get_districts($colegiado->ProvinciaUsuario);

        $this->master_admin_tmp->set('obj_departamento', $obj_departamento);
        $this->master_admin_tmp->set('obj_provincia', $obj_provincia);
        $this->master_admin_tmp->set('obj_distrito', $obj_distrito);
		$this->master_admin_tmp->set("colegiado", $colegiado);
        $this->master_admin_tmp->set("grades", $grades);
        $this->master_admin_tmp->render('perfil/edit_perfil');
		
	}
	public function ajax_editar_perfil()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
			$usuario_id = $this->session->session_cbpll["idUsuario"];
            $speciality = $this->input->post('speciality')==='Otros'?$this->input->post('speciality_other'):$this->input->post('speciality');
            
            $this->db->trans_begin();
            $result=$this->user_model->update($usuario_id,[
                'GeneroUsuario' => $this->input->post('genero'),
                'EstadoCivil' => $this->input->post('estado_civil'),
                'NacionalidadUsuario' => $this->input->post("nationality"),
                'TipoSangre' => $this->input->post("group_sangre"),
                'DireccionUsuario' =>  $this->input->post("address"),
                'DepartamentoUsuario' =>  $this->input->post("dpto"),
                'ProvinciaUsuario' => $this->input->post("prov"),
                'DistritoUsuario' => $this->input->post("district"),
                'TelefonoUsuario' => $this->input->post("phone"),
                'CelularUsuario' =>  $this->input->post("celphone"),
                'EmailUsuario' =>  $this->input->post("email"),
                'Linkweb' => $this->input->post('linkweb'),
                'speciality' => $speciality
            ]);

            $grades =  $this->input->post('grade[]');
            $namegrades =  $this->input->post('namegrade[]');
            if( !is_null($namegrades) && !is_null($grades)){
                $i =0;
                $this->grade_model->delete_grades($this->input->post("colegiado_id"));
                if( count($namegrades) > 0 && count($grades) > 0){
                    foreach ($grades as $grade) {
                        $this->grade_model->insert([
                            'grade' => $grade,
                            'name_grade' => $namegrades[$i],
                            'colegiado_id' => $this->input->post("colegiado_id"),
                        ]);
                        $i++;
                    }
                }
            }
            else{
                $this->grade_model->delete_grades($this->input->post("colegiado_id"));
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                $response['status'] = 'ERROR';
                $response['message'] = 'Hubo un inconveniente al editar el colegiado.';
            }
            else {
                $this->db->trans_commit();
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se editó correctamente.'; 
            }
			//if($result && $result2){
            	$response['status'] = 'SUCCESS';
				$response['message'] = 'Se actualizó sus datos correctamente.';
			//}
        }
        echo json_encode($response);
        exit;
	}
	public function change_password()
	{
		$this->master_admin_tmp->render('perfil/change_password');
	}
	public function ajax_change_password()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
			$usuario_id = $this->session->session_cbpll["idUsuario"];
            $result3=$this->login_model->update($usuario_id,[
                'PasswordLogin' => md5($this->input->post("new_password")),
            ]);
			//if($result && $result2){
            	$response['status'] = 'SUCCESS';
				$response['message'] = 'Se actualizó sus datos correctamente.';
			//}
        }
        echo json_encode($response);
        exit;
    }
    
    public function reporte_financiero()
    {
        $colegiado_id = $this->session->session_cbpll["idColegiado"];
        $pagos=$this->pagos_model->get_pagos_by_colegiado($colegiado_id);
		$colegiado=$this->colegiado_model->get_colegiado_by_id($colegiado_id);
		$this->master_admin_tmp->set("colegiado", $colegiado);
        $this->master_admin_tmp->set("pagos", $pagos);
        
		$this->master_admin_tmp->render('perfil/reporte_financiero');
    }

}
