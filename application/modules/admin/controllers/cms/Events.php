<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }
        $this->load->model('Events_model', 'events_model');
    }
	public function index()
	{   
        $events=$this->events_model->get_events();
		$this->master_admin_tmp->set("events", $events);
        $this->master_admin_tmp->render('cms/events/list_events');
    }
    public function editar_event($id_event = null)
	{   
        if($id_event!==null){
            $obj_event = $this->events_model->get_event($id_event);
            $this->master_admin_tmp->set("obj_event", $obj_event);
            $this->master_admin_tmp->set("id_event", $id_event);
            $this->master_admin_tmp->render('cms/events/insert_event');
        }
        else{
            $this->master_admin_tmp->render('cms/events/insert_event');
        }
    }
    public function ajax_insert_event()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/events/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            $event_id = $this->input->post("event_id");
            if($event_id!=="") {
                if( $_FILES["image_event"]["name"] != ""){
                    if($this->upload->do_upload('image_event')){
                        $data = array("upload_data" => $this->upload->data());
                        $result=$this->events_model->update($event_id,[
                            'title' => $this->input->post("title_event"),
                            'slug' => $this->generate_slug($event_id,$this->input->post("title_event")),
                            'description' => $this->input->post("description"),
                            'image' => $data['upload_data']['file_name'],
                            'horario' => $this->input->post('horario'),
                            'objetivo' => $this->input->post('objetivo'),
                            'duracion' => $this->input->post('duracion'),
                            'temario' => $this->input->post('temario')
                        ]);
            
                        $response['status'] = 'SUCCESS';
                        $response['message'] = 'Se actualizó el evento con éxito'; 
                    }
                    else{
                        $response['message'] = $this->upload->display_errors();
                    }
                }
                else{
                    $result=$this->events_model->update($this->input->post("event_id"),[
                        'title' => $this->input->post("title_event"),
                        'description' => $this->input->post("description"),
                        'horario' => $this->input->post('horario'),
                        'objetivo' => $this->input->post('objetivo'),
                        'duracion' => $this->input->post('duracion'),
                        'temario' => $this->input->post('temario')
                    ]);
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se actualizó el evento con éxito';
                }
            }else{
                if($_FILES["image_event"]["name"]){
                    if($this->upload->do_upload('image_event')){
                        $data = array("upload_data" => $this->upload->data());
                        $result=$this->events_model->insert([
                            'title' => $this->input->post("title_event"),
                            'description' => $this->input->post("description"),
                            'image' => $data['upload_data']['file_name'],
                            'horario' => $this->input->post('horario'),
                            'objetivo' => $this->input->post('objetivo'),
                            'duracion' => $this->input->post('duracion'),
                            'temario' => $this->input->post('temario'),
                            'status' => 1,
                            'visibility' => 1,
                        ]);
                        $this->events_model->update($result,[
                            'slug' => $this->generate_slug($result,$this->input->post("title_event"))
                        ]);
                        $response['status'] = 'SUCCESS';
                        $response['message'] = 'Se registró el evento con éxito'; 
                    }
                    else{
                        $response['message'] = $this->upload->display_errors();
                    } 
                }
            }
            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_delete_event(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->events_model->update($this->input->post("event_id"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_visibility_event(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->events_model->update($this->input->post("event_id"),[
                'visibility' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se cambió la visibilidad correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    private function generate_slug($event_id, $name) {
        if ($event_id > 0) {
            return slug($name. ' ' .$event_id);
        }
    }
	
}