<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nosotros extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }
        $this->load->model('Events_model', 'events_model');
        $this->load->model('Institucional_model', 'institucional_model');
        $this->load->model('Comisions_model', 'comisions_model');
        $this->load->model('Directives_model', 'directives_model');
        $this->load->model('Boletin_model', 'boletin_model');
    }
	public function index()
	{   
        $institucional=$this->institucional_model->get_data();
		$this->master_admin_tmp->set("institucional", $institucional);
        $this->master_admin_tmp->render('cms/nosotros/institucional');
    }
    public function directivos()
	{   
        $directives=$this->directives_model->get_directives();
		$this->master_admin_tmp->set("directives", $directives);
        $this->master_admin_tmp->render('cms/nosotros/consejo_list');
    }
    public function directivo_insert($directive_id=null)
	{   
        if($directive_id!==null){
            $obj_directive = $this->directives_model->get_directive($directive_id);
            $this->master_admin_tmp->set("obj_directive", $obj_directive);
            $this->master_admin_tmp->set("directive_id", $directive_id);
        }
        $this->master_admin_tmp->render('cms/nosotros/consejo_insert');
    }
    public function boletines()
	{   
        $boletines=$this->boletin_model->get_all();
		$this->master_admin_tmp->set("boletines", $boletines);
        $this->master_admin_tmp->render('cms/nosotros/boletin_list');
    }
    public function boletin_insert($boletin_id=null)
	{   
        if($boletin_id!==null){
            $obj_boletin = $this->boletin_model->get_boletin($boletin_id);
            $this->master_admin_tmp->set("obj_boletin", $obj_boletin);
            $this->master_admin_tmp->set("boletin_id", $boletin_id);
        }
        $this->master_admin_tmp->render('cms/nosotros/boletin_insert');
    }
    public function comisiones()
	{   
        $comisiones=$this->comisions_model->get_comisions();
		$this->master_admin_tmp->set("comisiones", $comisiones);
        $this->master_admin_tmp->render('cms/nosotros/comision_list');
    }
    public function comision_insert($comision_id=null)
	{   
        if($comision_id!==null){
            $obj_comision = $this->comisions_model->get_comision($comision_id);
            $this->master_admin_tmp->set("obj_comision", $obj_comision);
            $this->master_admin_tmp->set("comision_id", $comision_id);
        }
        $this->master_admin_tmp->render('cms/nosotros/comision_insert');
    }
    
    public function ajax_update_institucional()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            if( $_FILES["image_institucional"]["name"] != ""){
                if($this->upload->do_upload('image_institucional')){
                    $data = array("upload_data" => $this->upload->data());
                    $result=$this->institucional_model->update($this->input->post("institucional_id"),[
                        'history' => $this->input->post("textHistoria"),
                        'mision' => $this->input->post("mision"),
                        'image' => $data['upload_data']['file_name'],
                        'vision' => $this->input->post('vision')
                    ]);
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se actualizó textos con éxito'; 
                }
                else{
                    $response['message'] = $this->upload->display_errors();
                }
            }else{
                $result=$this->institucional_model->update($this->input->post("institucional_id"),[
                    'history' => $this->input->post("textHistoria"),
                    'mision' => $this->input->post("mision"),
                    'vision' => $this->input->post('vision')
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se actualizó textos con éxito';
            }
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_insert_comision()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $comision_id = $this->input->post("comision_id");
            if($comision_id!=="") {
                $result=$this->comisions_model->update($comision_id,[
                    'name' => $this->input->post("name_comision"),
                    'type' => $this->input->post("tipoComision"),
                    'president' => $this->input->post('presidente_comision'),
                    'members' => $this->input->post('members_comision')
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se actualizó la comisión con éxito'; 
            } else {
                $result=$this->comisions_model->insert([
                    'name' => $this->input->post("name_comision"),
                    'type' => $this->input->post("tipoComision"),
                    'president' => $this->input->post("presidente_comision"),
                    'members' => $this->input->post('members_comision'),
                    'status' => 1,
                    'visibility' => 1,
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se registró la comisión con éxito'; 
            }            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_delete_comision(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->comisions_model->update($this->input->post("comision_id"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_visibility_comision(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->comisions_model->update($this->input->post("comision_id"),[
                'visibility' => $this->input->post("visibility")=='1'?0:1,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se cambió la visibilidad correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_insert_boletin(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/docs/',
                'allowed_types' => '*',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            $boletin_id = $this->input->post("boletin_id");
            if($boletin_id!=="") {
                if( $_FILES["doc_file"]["name"] != ""){
                    if($this->upload->do_upload('doc_file')){
                        $data = array("upload_data" => $this->upload->data());
                        $result=$this->boletin_model->update($boletin_id,[
                            'texto' => $this->input->post("text_boletin"),
                            'filename' => $data['upload_data']['file_name'],
                            'type' => $this->input->post('type_boletin')
                        ]);
                        $response['status'] = 'SUCCESS';
                        $response['message'] = 'Se actualizó el evento con éxito'; 
                    }
                    else{
                        $response['message'] = $this->upload->display_errors();
                    }
                }
                else{
                    $result=$this->boletin_model->update($boletin_id,[
                        'texto' => $this->input->post("text_boletin"),
                        'type' => $this->input->post('type_boletin')
                    ]);
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se actualizó el evento con éxito';
                }
            }else{
                if($_FILES["doc_file"]["name"]){
                    if($this->upload->do_upload('doc_file')){
                        $data = array("upload_data" => $this->upload->data());
                        $result=$this->boletin_model->insert([
                            'texto' => $this->input->post("text_boletin"),
                            'filename' => $data['upload_data']['file_name'],
                            'type' => $this->input->post('type_boletin'),
                            'status' => 1
                        ]);
                        $response['status'] = 'SUCCESS';
                        $response['message'] = 'Se registró el evento con éxito'; 
                    }
                    else{
                        $response['message'] = $this->upload->display_errors();
                    } 
                }
            }
            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_delete_boletin(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->boletin_model->update($this->input->post("boletin_id"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
        }
        echo json_encode($response);
        exit;
    }

    public function ajax_insert_directivo(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/directivos/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            $directive_id = $this->input->post("directivo_id");
            if($directive_id!=="") {
                if( $_FILES["image_directive"]["name"] != ""){
                    if($this->upload->do_upload('image_directive')){
                        $data = array("upload_data" => $this->upload->data());
                        $result=$this->directives_model->update($directive_id,[
                            'name' => $this->input->post("name_directive"),
                            'position' => $this->input->post('position_directive'),
                            'image' => $data['upload_data']['file_name'],
                            'orden_position' => $this->generate_order($this->input->post('position_directive')),
                            'periodo' => $this->input->post('periodo')
                        ]);
            
                        $response['status'] = 'SUCCESS';
                        $response['message'] = 'Se actualizó el directivo con éxito'; 
                    }
                    else{
                        $response['message'] = $this->upload->display_errors();
                    }
                }
                else{
                    $result=$this->directives_model->update($directive_id,[
                        'name' => $this->input->post("name_directive"),
                        'position' => $this->input->post('position_directive'),
                        'orden_position' => $this->generate_order($this->input->post('position_directive')),
                        'periodo' => $this->input->post('periodo'),
                    ]);
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se actualizó el directivo con éxito';
                }
            }else{
                if($_FILES["image_directive"]["name"]){
                    if($this->upload->do_upload('image_directive')){
                        $data = array("upload_data" => $this->upload->data());
                        $result=$this->directives_model->insert([
                            'name' => $this->input->post("name_directive"),
                            'position' => $this->input->post('position_directive'),
                            'image' => $data['upload_data']['file_name'],
                            'orden_position' => $this->generate_order($this->input->post('position_directive')),
                            'periodo' => $this->input->post('periodo'),
                            'status' => 1,
                            'visibility' => 0
                        ]);
                        $response['status'] = 'SUCCESS';
                        $response['message'] = 'Se registró el directivo con éxito'; 
                    }
                    else{
                        $response['message'] = $this->upload->display_errors();
                    } 
                }
            }
            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_delete_directive(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->directives_model->update($this->input->post("directive_id"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_visibility_directive(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->directives_model->update($this->input->post("directive_id"),[
                'visibility' => $this->input->post("visibility")=='1'?0:1,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se cambió la visibilidad correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    private function generate_order($position){
        $order = 1;
        switch($position){
            case 'Decano': 
                $order = 1;
                break;
            case 'Vicedecano': 
                $order = 2;
                break;
            case 'Secretario': 
                $order = 3;
                break;
            case 'Tesorero': 
                $order = 4;
                break;
            case 'Primer Vocal': 
                $order = 5;
                break;
            case 'Segundo Vocal': 
                $order = 6;
                break;
        }
        return $order;
                
    }
}