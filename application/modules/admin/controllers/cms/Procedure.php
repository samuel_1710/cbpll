<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Procedure extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }
        $this->load->model('Events_model', 'events_model');
        $this->load->model('Institucional_model', 'institucional_model');
        $this->load->model('Comisions_model', 'comisions_model');
        $this->load->model('Directives_model', 'directives_model');
        $this->load->model('Procedures_model', 'procedure_model');
        $this->load->model('Boletin_model', 'boletin_model');
    }
    public function index()
	{   
        $procedures = $this->procedure_model->get_procedures();
		$this->master_admin_tmp->set("procedures", $procedures);
        $this->master_admin_tmp->render('cms/procedure/procedure_list');
    }
    public function procedure_insert($procedure_id=null)
	{   
        if($procedure_id!==null){
            $obj_procedure = $this->procedure_model->get_procedure($procedure_id);
            $this->master_admin_tmp->set("obj_procedure", $obj_procedure);
            $this->master_admin_tmp->set("procedure_id", $procedure_id);
        }
        $this->master_admin_tmp->render('cms/procedure/procedure_insert');
    }
    
    public function ajax_insert_procedure()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $procedure_id = $this->input->post("procedure_id");
            if($procedure_id!=="") {
                $result=$this->procedure_model->update($procedure_id,[
                    'title' => $this->input->post("name_tramite"),
                    'slug' => slug($this->input->post("name_tramite")),
                    'description' => $this->input->post("description_tramite"),
                    'note' => $this->input->post("note"),
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se actualizó el trámite con éxito'; 
            } else {
                $result=$this->procedure_model->insert([
                    'title' => $this->input->post("name_tramite"),
                    'slug' => slug($this->input->post("name_tramite")),
                    'description' => $this->input->post("description_tramite"),
                    'note' => $this->input->post("note"),
                    'status' => 1,
                    'visibility' => 0,
                ]);
                $response['status'] = 'SUCCESS';
                $response['message'] = 'Se registró el trámite con éxito'; 
            }            
		}
		echo json_encode($response);
        exit;
    }
    public function ajax_delete_procedure(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->procedure_model->update($this->input->post("procedure_id"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_visibility_procedure(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result=$this->procedure_model->update($this->input->post("procedure_id"),[
                'visibility' => $this->input->post("visibility")=='1'?0:1,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se cambió la visibilidad correctamente.';
        }
        echo json_encode($response);
        exit;
    }
    
}