<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }
        $this->load->model('Events_model', 'events_model');
        $this->load->model('Imageslider_model', 'slider_model');
        $this->load->model('Comunication_model', 'comunication_model');
    }
	public function index()
	{   
        $comunications = $this->comunication_model->get_images();
		$this->master_admin_tmp->set("comunications", $comunications);
        $this->master_admin_tmp->render('cms/home/comunication');
    }
    public function slider()
	{   
        $images = $this->slider_model->get_images_by_type(1);
		$this->master_admin_tmp->set("images", $images);
        $this->master_admin_tmp->render('cms/home/slider');
    }
    public function infraestructura(){
        $images = $this->slider_model->get_images_by_type(2);
		$this->master_admin_tmp->set("images", $images);
        $this->master_admin_tmp->render('cms/home/infraestructura');
    }
    public function convenios(){
        $images = $this->slider_model->get_images_by_type(3);
		$this->master_admin_tmp->set("images", $images);
        $this->master_admin_tmp->render('cms/home/convenios');
    }
    public function ajax_insert_slider(){
        $response = [
            'error' => 'ERROR',
            'initialPreview' => '',
            'initialPreviewConfig' => '',
            'initialPreviewThumbTags' => '',
            'append' => true
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/slidehome/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            if( isset($_FILES)){
                if($this->upload->do_upload('fileBlob')){
                    $data = array("upload_data" => $this->upload->data());
                    $name_image =  $data['upload_data']['file_name'];
                    $result=$this->slider_model->insert([
                        'name' => $data['upload_data']['file_name'],
                        'type_image' => 1,
                        'status' => 1
                    ]);
                    $fileSize = $_POST['fileSize'];          // you receive the file size as a separate post data
                    $fileId = $_POST['fileId'];              // you receive the file identifier as a separate post data
                    $index =  $_POST['chunkIndex'];          // the current file chunk index
                    $totalChunks = $_POST['chunkCount'];     // the total number of chunks for this file
                    $response = [
                        'chunkIndex' => $index,         // the chunk index processed
                        'initialPreview' => URL_UPLOADS.'slidehome/'.$name_image, // the thumbnail preview data (e.g. image)
                        'initialPreviewConfig' => [
                            [
                                'type' => 'image',      // check previewTypes (set it to 'other' if you want no content preview)
                                'caption' => $name_image, // caption
                                'key' => $result,       // keys for deleting/reorganizing preview
                                'fileId' => $result,    // file identifier
                                'size' => $fileSize,    // file size
                                'url' => base_url()."admin/cms/home/delete_image_slider"
                            ]
                        ],
                        'append' => true
                    ];
                }
                else{
                    $response = [
                        'error' => 'Error al subir la imagen' . $_POST['chunkIndex']
                    ];
                }
            }
        }
        echo json_encode($response);
        exit;
    }
    public function delete_image_slider(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result = $this->slider_model->update($this->input->post("key"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
                
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_insert_comunication(){
        $response = [
            'error' => 'ERROR',
            'initialPreview' => '',
            'initialPreviewConfig' => '',
            'initialPreviewThumbTags' => '',
            'append' => true
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/slidehome/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            if( isset($_FILES)){
                if($this->upload->do_upload('fileBlob')){
                    $data = array("upload_data" => $this->upload->data());
                    $name_image =  $data['upload_data']['file_name'];
                    $result=$this->comunication_model->insert([
                        'image' => $data['upload_data']['file_name'],
                        'status' => 1
                    ]);
                    $fileSize = $_POST['fileSize'];          // you receive the file size as a separate post data
                    $fileId = $_POST['fileId'];              // you receive the file identifier as a separate post data
                    $index =  $_POST['chunkIndex'];          // the current file chunk index
                    $totalChunks = $_POST['chunkCount'];     // the total number of chunks for this file
                    $response = [
                        'chunkIndex' => $index,         // the chunk index processed
                        'initialPreview' => URL_UPLOADS.'slidehome/'.$name_image, // the thumbnail preview data (e.g. image)
                        'initialPreviewConfig' => [
                            [
                                'type' => 'image',      // check previewTypes (set it to 'other' if you want no content preview)
                                'caption' => $name_image, // caption
                                'key' => $result,       // keys for deleting/reorganizing preview
                                'fileId' => $result,    // file identifier
                                'size' => $fileSize,    // file size
                                'url' => base_url()."admin/cms/home/delete_image_slider"
                            ]
                        ],
                        'append' => true
                    ];
                }
                else{
                    $response = [
                        'error' => 'Error al subir la imagen' . $_POST['chunkIndex']
                    ];
                }
            }
        }
        echo json_encode($response);
        exit;
    }
    public function delete_comunication(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $result = $this->comunication_model->update($this->input->post("key"),[
                'status' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se eliminó correctamente.';
                
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_insert_infraestructura(){
        $response = [
            'error' => 'ERROR',
            'initialPreview' => '',
            'initialPreviewConfig' => '',
            'initialPreviewThumbTags' => '',
            'append' => true
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/infraestructura/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            if( isset($_FILES)){
                if($this->upload->do_upload('fileBlob')){
                    $data = array("upload_data" => $this->upload->data());
                    $name_image =  $data['upload_data']['file_name'];
                    $result=$this->slider_model->insert([
                        'name' => $data['upload_data']['file_name'],
                        'type_image' => 2,
                        'status' => 1
                    ]);
                    $fileSize = $_POST['fileSize'];          // you receive the file size as a separate post data
                    $fileId = $_POST['fileId'];              // you receive the file identifier as a separate post data
                    $index =  $_POST['chunkIndex'];          // the current file chunk index
                    $totalChunks = $_POST['chunkCount'];     // the total number of chunks for this file
                    $response = [
                        'chunkIndex' => $index,         // the chunk index processed
                        'initialPreview' => URL_UPLOADS.'infraestructura/'.$name_image, // the thumbnail preview data (e.g. image)
                        'initialPreviewConfig' => [
                            [
                                'type' => 'image',      // check previewTypes (set it to 'other' if you want no content preview)
                                'caption' => $name_image, // caption
                                'key' => $result,       // keys for deleting/reorganizing preview
                                'fileId' => $result,    // file identifier
                                'size' => $fileSize,    // file size
                                'url' => base_url()."admin/cms/home/delete_image_slider"
                            ]
                        ],
                        'append' => true
                    ];
                }
                else{
                    $response = [
                        'error' => 'Error al subir la imagen' . $_POST['chunkIndex']
                    ];
                }
            }
        }
        echo json_encode($response);
        exit;
    }
    public function ajax_insert_convenios(){
        $response = [
            'error' => 'ERROR',
            'initialPreview' => '',
            'initialPreviewConfig' => '',
            'initialPreviewThumbTags' => '',
            'append' => true
        ];
        if ($this->input->is_ajax_request()) {
            $config = [
                'upload_path' => './static/uploads/convenios/',
                'allowed_types' => 'png|jpg|jpeg',
                'encrypt_name' => TRUE
            ];
            $this->load->library('upload',$config);
            if( isset($_FILES)){
                if($this->upload->do_upload('fileBlob')){
                    $data = array("upload_data" => $this->upload->data());
                    $name_image =  $data['upload_data']['file_name'];
                    $result=$this->slider_model->insert([
                        'name' => $data['upload_data']['file_name'],
                        'type_image' => 3,
                        'status' => 1
                    ]);
                    $fileSize = $_POST['fileSize'];          // you receive the file size as a separate post data
                    $fileId = $_POST['fileId'];              // you receive the file identifier as a separate post data
                    $index =  $_POST['chunkIndex'];          // the current file chunk index
                    $totalChunks = $_POST['chunkCount'];     // the total number of chunks for this file
                    $response = [
                        'chunkIndex' => $index,         // the chunk index processed
                        'initialPreview' => URL_UPLOADS.'convenios/'.$name_image, // the thumbnail preview data (e.g. image)
                        'initialPreviewConfig' => [
                            [
                                'type' => 'image',      // check previewTypes (set it to 'other' if you want no content preview)
                                'caption' => $name_image, // caption
                                'key' => $result,       // keys for deleting/reorganizing preview
                                'fileId' => $result,    // file identifier
                                'size' => $fileSize,    // file size
                                'url' => base_url()."admin/cms/home/delete_image_slider"
                            ]
                        ],
                        'append' => true
                    ];
                }
                else{
                    $response = [
                        'error' => 'Error al subir la imagen' . $_POST['chunkIndex']
                    ];
                }
            }
        }
        echo json_encode($response);
        exit;
    }
	
}