<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }

        $this->load->model('Users_model', 'user_model');
        $this->load->model('Login_model', 'login_model');
        $this->load->model('Administrativos_model', 'administrativo_model');
    }
	public function index()
	{
        $users = $this->administrativo_model->get_administrativos_user();
		$this->master_admin_tmp->set("users", $users);
        $this->master_admin_tmp->render('user/user_list');
	}
    public function user_insert($administrativo_id=null)
	{   
        if($administrativo_id!==null){
            $obj_user = $this->administrativo_model->get_administrativo_by_id($administrativo_id);
            $this->master_admin_tmp->set("obj_user", $obj_user);
            $this->master_admin_tmp->set("administrativo_id", $administrativo_id);
        }
        $this->master_admin_tmp->render('user/user_insert');
    }
	
	public function ajax_insert_user()
	{
		$response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {
            $user_id = $this->input->post("user_id");
            $administrativo_id = $this->input->post("administrativo_id");
            $roles = $this->input->post('roles[]');
            $roles = implode("-",$roles);
            if($user_id!=="") {
                $this->db->trans_begin();
                $result = $this->user_model->update($user_id,[
                    'ApellidosPaterno' => trim($this->input->post("ape_pat")),
                    'ApellidoMaterno' => trim($this->input->post("ape_mat")),
                    'NombresUsuario' => trim($this->input->post("name")),
                    'TipoDocumento' =>  'DNI',
                    'NumeroDocumento' =>  trim($this->input->post("username")),
                ]);
                if($this->input->post("password_user") !== ""){
                    $this->login_model->update($user_id,[
                        'PasswordLogin' => md5($this->input->post("password_user"))
                    ]);
                }
                $this->administrativo_model->update($administrativo_id,[
                    'role' => $roles
                ]);
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $response['status'] = 'ERROR';
                    $response['message'] = 'Hubo un inconveniente al editar el usuario.';
                }
                else {
                    $this->db->trans_commit();
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se actualizó el usuario con éxito'; 
                }
            } else {

                $this->db->trans_begin();
                $result = $this->user_model->insert([
                    'ApellidosPaterno' => trim($this->input->post("ape_pat")),
                    'ApellidoMaterno' => trim($this->input->post("ape_mat")),
                    'NombresUsuario' => trim($this->input->post("name")),
                    'TipoDocumento' =>  'DNI',
                    'NumeroDocumento' =>  trim($this->input->post("username")),
                ]);
                $this->login_model->insert([
                    'PasswordLogin' => md5($this->input->post("password_user")),
                    'Usuarios_idUsuarios' => $result
                ]);
                $this->administrativo_model->insert([
                    'AreaAdministrativos' => 'Sistemas - Editor',
                    'CargoAdministrativos' => 'Adminweb',
                    'FechaIngreso' => date("Y-m-d"),
                    'role' => $roles,
                    'Usuarios_idUsuarios' => $result
                ]);
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    $response['status'] = 'ERROR';
                    $response['message'] = 'Hubo un inconveniente al insertar el usuario.';
                }
                else
                {
                    $this->db->trans_commit();
                    $response['status'] = 'SUCCESS';
                    $response['message'] = 'Se registró el usuario con éxito'; 
                }
                
            }            
        }
        echo json_encode($response);
        exit;
	}
    public function ajax_delete_user(){
        $response = [
            'status' => 'ERROR',
            'message' => 'Petición no es valida'
        ];
        if ($this->input->is_ajax_request()) {

            $user_id = $this->input->post("user_id");
            $result=$this->user_model->update($user_id,[
                'status_value' => 0,
            ]);
            $response['status'] = 'SUCCESS';
            $response['message'] = 'Se registró correctamente.';
        }
        echo json_encode($response);
        exit;
    }

}
