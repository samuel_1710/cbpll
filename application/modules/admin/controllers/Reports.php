<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller {

	function __construct() {
        parent::__construct();
        if(!$this->session->has_userdata('session_cbpll')) {
            redirect('/admin/login');
        }

		$this->load->model('Colegiado_model', 'colegiado_model');
        $this->load->model('Users_model', 'user_model');
        $this->load->model('Login_model', 'login_model');
        $this->load->model('Pagoscolegiatura_model', 'pagos_model');
        $this->load->model('Ubigeo_model', 'obj_ubigeo');
        $this->load->library('Classes/PHPExcel.php');
        $this->load->library('ExcelCustom');
    }
	public function index()
	{
        
        $cards=$this->colegiado_model->list_register_virtual();
        //$this->master_admin_tmp->set("cards", $cards);
        /*$table[] = [
            'Nro', 'PORTAL', 'TIPO CONTACTO', 'FECHA', 'PROYECTO', 'TIPO DE PROYECTO', 'TIPO DE INMUEBLE', 
            'PISO', 'INMUEBLE', 'METROS CUADRADOS', 'DORMITORIOS', 'BAÑOS', 
            'MONEDA', 'PRECIO', 'CLIENTE', 'TIPO DOC.', 'DOC.', 'TELEFONO', 'EMAIL'
        ];
        $data = $this->render_contact_in_excel($cards, $table);
        $this->excelcustom->exportAllContacts("Reporte","reportaados",$data);*/
        $this->phpexcel->getProperties()->setCreator("Arkos Noem Arenom")
            ->setLastModifiedBy("Arkos Noem Arenom")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");
        // agregamos información a las celdas
        $this->phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Nro.')
            ->setCellValue('B1', 'PROYECTO')
            ->setCellValue('C1', 'BANCO FINANCIA')
            ->setCellValue('D1', 'TIPO DE PROYECTO')
            ->setCellValue('E1', 'TIPO DE INMUEBLE')
            ->setCellValue('F1', 'PISO')
            ->setCellValue('G1', 'INMUEBLE')
            ->setCellValue('H1', 'PRECIO')
            ->setCellValue('I1', 'METROS CUADRADOS')
            ->setCellValue('J1', 'DORMITORIOS')
            ->setCellValue('K1', 'BAÑOS')
            ->setCellValue('L1', 'FECHA DE COTIZACION')
            ->setCellValue('M1', 'CLIENTE')
            ->setCellValue('N1', 'DNI')
            ->setCellValue('O1', 'TELEFONO')
            ->setCellValue('P1', 'EMAIL')
        ;
        // Renombramos la hoja de trabajo
        $this->phpexcel->getActiveSheet()->setTitle('Cotizaciones');

        // configuramos el documento para que la hoja
        // de trabajo número 0 sera la primera en mostrarse
        // al abrir el documento
        $this->phpexcel->setActiveSheetIndex(0);      

        // redireccionamos la salida al navegador del cliente (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="cotizacion.xlsx"');
        header('Cache-Control: max-age=0');


        $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
    }
    public function report_colegiado(){
		$this->master_admin_tmp->render('reports/report_colegiado');
    }
    public function export_colegiado(){
        if($this->input->post()) {
            $type_report=$this->input->post('option_export');
            $initial = ['Nro','Apellido Paterno','Apellido Materno','Nombres'];
            $table[] = array_merge($initial,$this->input->post('fields[]'));

            $list_colegiados =  $this->colegiado_model->get_all_colegiados();
            $data = $this->render_colegiados_in_excel($list_colegiados, $table);
            $this->data_colegiados_excel('Colegiados','Colegiados',$data);
        }else {
            redirect('admin/reporte-colegiado');
        }
    }
    public function data_colegiados_excel($name_hoja, $titulo_excel,$data_excel = NULL) {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        $this->phpexcel->getProperties()->setCreator("CBPLL")
            ->setLastModifiedBy("CBPLL")
            ->setTitle("Colegiados")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Listado de Colegiados");
        //llenando celdas
        if($data_excel != NULL) {
            $this->phpexcel->getActiveSheet()->fromArray($data_excel, NULL, 'A1');
        }
        unset($data_excel);
        $this->phpexcel->getActiveSheet()->getStyle('A1:T1')->applyFromArray([
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ],
            'font' => [
                'bold' => true,
            ],
        ]);
        foreach(range('A','T') as $columnID) {
            $this->phpexcel->getActiveSheet()
                ->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        $this->phpexcel->getActiveSheet()->setTitle($name_hoja);
        
        $this->phpexcel->setActiveSheetIndex(0);
        
        $nombreArchivo = $titulo_excel.date('Y-m-d_H-i-s');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Cache-Control: max-age=0");
        header('Cache-Control: max-age=1');
        header('Content-Disposition: attachment;filename="'.$nombreArchivo.'.xls"');

        $objWriter = PHPExcel_IOFactory::createWriter($this->phpexcel, 'Excel2007');
        ob_end_clean();
        $objWriter->save('php://output');
        exit();
    }
    public function render_colegiados_in_excel($datos, $table)
    {
        $num = 1;
        foreach ($datos as $index => $value) {

            $value->provincia = ($value->provincia != NULL) ? $value->provincia : '-';
            $value->departamento = ($value->departamento != NULL) ? $value->departamento : '-';
            $value->distrito = ($value->distrito != NULL) ? $value->distrito : '-';
            $value->GeneroUsuario = ($value->GeneroUsuario != NULL) ? ($value->GeneroUsuario=="1"?"Hombre":"Mujer") : '-';

            $row = [
                $num,
                $value->ApellidosPaterno,
                $value->ApellidoMaterno,
                $value->NombresUsuario,
                array_search('CodigoColegiado',$table[0])?is_null_custom($value->CodigoColegiado):null,
                array_search('FechaColegiado',$table[0])?is_null_custom($value->FechaColegiado):null,
                array_search('FechaUltimoPago',$table[0])?isset($value->FechaUltimoPago)?$value->FechaUltimoPago:'-':null,
                array_search('EstadoColegiado',$table[0])?status_colegiado_report($value->type_member,$value->FechaUltimoPago,$value->EstadoColegiado):null,
                array_search('TipoDocumento',$table[0])?is_null_custom($value->TipoDocumento):null,
                array_search('NumeroDocumento',$table[0])?is_null_custom($value->NumeroDocumento):null,
                array_search('FechaNacimiento',$table[0])?is_null_custom($value->FechaNacimiento):null,
                array_search('GeneroUsuario',$table[0])?is_null_custom($value->GeneroUsuario):null,
                array_search('EstadoCivil',$table[0])?is_null_custom($value->EstadoCivil):null,
                array_search('DireccionUsuario',$table[0])?is_null_custom($value->DireccionUsuario):null,
                array_search('DistritoUsuario',$table[0])?is_null_custom($value->distrito):null,
                array_search('ProvinciaUsuario',$table[0])?is_null_custom($value->provincia):null,
                array_search('DepartamentoUsuario',$table[0])?is_null_custom($value->departamento):null,
                array_search('TelefonoUsuario',$table[0])?is_null_custom($value->TelefonoUsuario):null,
                array_search('CelularUsuario',$table[0])?is_null_custom($value->CelularUsuario):null,
                array_search('EmailUsuario',$table[0])?is_null_custom($value->EmailUsuario):null,
                array_search('TipoSangre',$table[0])?is_null_custom($value->TipoSangre):null,
                array_search('Linkweb',$table[0])?is_null_custom($value->Linkweb):null,
            ];
            $row = array_filter($row);
            $table[] = $row;
            $num = $num + 1;
        }

        $table[0] = array_replace(
            $table[0],
            [array_search('CodigoColegiado',$table[0]) => 'Cod. CBP'],
            [array_search('FechaColegiado',$table[0]) => 'Fecha de Colegiatura'],
            [array_search('TipoDocumento',$table[0]) => 'Tipo de Documento'],
            [array_search('NumeroDocumento',$table[0]) => 'Nro. Documento'],
            [array_search('FechaNacimiento',$table[0]) => 'Fecha de Nacimiento'],
            [array_search('GeneroUsuario',$table[0]) => 'Género'],
            [array_search('EstadoCivil',$table[0]) => 'Estado Civil'],
            [array_search('DireccionUsuario',$table[0]) => 'Dirección'],
            [array_search('DistritoUsuario',$table[0]) => 'Distrito'],
            [array_search('ProvinciaUsuario',$table[0]) => 'Provincia'],
            [array_search('DepartamentoUsuario',$table[0]) => 'Departamento'],
            [array_search('TelefonoUsuario',$table[0]) => 'Teléfono'],
            [array_search('CelularUsuario',$table[0]) => 'Celular'],
            [array_search('TipoSangre',$table[0]) => 'Tipo de Sangre'],
            [array_search('EmailUsuario',$table[0]) => 'Email'],
            [array_search('Linkweb',$table[0]) => 'Blog/Web'],
            [array_search('FechaUltimoPago', $table[0]) => 'Última Fecha de Pago'],
            [array_search('EstadoColegiado', $table[0]) =>'Estado de Habilidad'],
            [array_search('Nro',$table[0]) => 'Nro'],
        );
        return $table;
    }
    public function report_ultimate_pago(){
        $this->master_admin_tmp->render('reports/report_ultimate_pago');
    }
    public function export_ultimate_pago(){
        if($this->input->post()) {
            $date_payment=$this->input->post('payment_to');
            $table[] = ['Nro',
                'Cod CBP',
                'Apellido Paterno',
                'Apellido Materno',
                'Nombres',
                'Tipo de Documento',
                'Nro de Documento',
                'Dirección',
                'Telefono',
                'Celular',
                'Correo Eléctronico',
                'Último Pago'
            ];
            $list_colegiados =  $this->colegiado_model->get_all_colegiado_ultimo_payment($date_payment);
            $data = $this->render_colegiados_payment($list_colegiados, $table);
            $this->data_colegiados_excel('Colegiados','Colegiados',$data);
        }else {
            redirect('admin/reporte-ultimo-pago');
        }
    }
    public function render_colegiados_payment($datos, $table)
    {
        $num = 1;
        foreach ($datos as $index => $value) {          
            $row = [
                $num,
                $value->CodigoColegiado,
                $value->ApellidosPaterno,
                $value->ApellidoMaterno,
                $value->NombresUsuario,
                $value->TipoDocumento,
                $value->NumeroDocumento,
                $value->DireccionUsuario,
                $value->TelefonoUsuario,
                $value->CelularUsuario,
                $value->EmailUsuario,
                $value->FechaUltimoPago,
            ];
            $table[] = $row;
            $num = $num + 1;
        }

        return $table;
    }
    public function report_habilitados(){
        $this->master_admin_tmp->render('reports/report_habilitados');
    }
    public function export_habilitados(){
        if($this->input->post()) {
            $habilitados_to=$this->input->post('habilitados_to');
            $table[] = ['Nro',
                'Cod CBP',
                'Apellido Paterno',
                'Apellido Materno',
                'Nombres',
                'Dirección',
                'Telefono',
                'Celular',
                'Último Pago',
                'Estado',
                'Habilitado hasta'
            ];
            $list_colegiados =  $this->colegiado_model->get_habilitados($habilitados_to);
            $data = $this->render_habilitados($list_colegiados, $table);
            $this->data_colegiados_excel('Colegiados','Colegiados',$data);
        }else {
            redirect('admin/reporte-ultimo-pago');
        }
    }
    public function render_habilitados($datos, $table)
    {
        $num = 1;
        foreach ($datos as $index => $value) {          
            $row = [
                $num,
                $value->CodigoColegiado,
                $value->ApellidosPaterno,
                $value->ApellidoMaterno,
                $value->NombresUsuario,
                $value->DireccionUsuario,
                $value->TelefonoUsuario,
                $value->CelularUsuario,
                isset($value->FechaUltimoPago)?$value->FechaUltimoPago:'-',
                $value->status,
                isset($value->date_habilitado)?$value->date_habilitado:'-',
            ];
            $table[] = $row;
            $num = $num + 1;
        }

        return $table;
    }
    
}