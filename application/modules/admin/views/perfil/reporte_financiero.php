<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reporte de Cuotas</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Reporte de Cuotas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Listado de Cuotas</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formSearchColegiado" class="formSearch" action="">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_colegiatura">Fecha de Colegiatura</label>
                            <input disabled value="<?php echo $colegiado->FechaColegiado?>" type="text" class="form-control" id="date_colegiatura" name="date_colegiatura" placeholder="Ingrese fecha de colegiatura" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="cod_cbp">Código CBP</label>
                            <input disabled value="<?php echo $colegiado->CodigoColegiado?>" type="text" class="form-control" id="cod_cbp" name="cod_cbp" placeholder="Ingrese código CBP" autocomplete="off">
                        </div>
                    </div>
                </div>
              <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_pat">Apellido Paterno</label>
                            <input disabled value="<?php echo $colegiado->ApellidosPaterno?>" type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="Ingrese su apellido paterno">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_mat">Apellido Materno</label>
                            <input disabled value="<?php echo $colegiado->ApellidoMaterno?>" type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="Ingrese su apellido materno">
                        </div>
                    </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Nombres</label>
                        <input disabled value="<?php echo $colegiado->NombresUsuario?>" type="text" class="form-control" id="name" name="name" placeholder="Ingrese sus nombres">
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="nro_doc">Documento</label>
                      <input disabled value="<?php echo $colegiado->TipoDocumento." : ".$colegiado->NumeroDocumento; ?>" type="text" class="form-control" name="nro_doc" id="nro_doc" placeholder="Ingrese nro de documento">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Estado</label>
                        <?php 
                          echo status_colegiado($colegiado->type_member,$colegiado->FechaUltimoPago,$colegiado->EstadoColegiado,true);
                        ?>
                    </div>
                </div>
                </div>
            </form>
            <div class="row">
              <div class="col-md-12"><br>
                <div id="content_table"><?php $this->load->view('perfil/reporte_financiero_table')?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>