<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Cambiar Contraseña</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Cambiar contraseña</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-6">
      <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Cambiar contraseña</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" id="formChangePassword">
                <div class="card-body">
                  <div class="form-group">
                    <label for="new_password">Contraseña nueva:</label>
                    <input type="password" class="form-control" id="new_password" name="new_password">
                  </div>
                  <div class="form-group">
                    <label for="repeat_new_password">Repita contraseña nueva:</label>
                    <input type="password" class="form-control" id="repeat_new_password" name="repeat_new_password">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
              </form>
            </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo link_static('admin/js/change_password.js')?>"></script>