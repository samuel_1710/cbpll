<?php if(count($pagos)>0){?>
<table id="example2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-center">Fecha de Registro</td>
      <td class="text-center">Fecha de Pago</td>
      <td class="text-center">Último mes pagado</td>
      <th class="text-center">Comprobante de Pago</th>
      <th class="text-center">Monto</th>
      <th class="text-center">Documento de Pago</th>
    </tr>
  </thead>
  <tbody>
            <?php $i=1;foreach ($pagos as $pago) { ?>
            <tr>
                <td class="text-center"><?php echo $pago->date_registro_pago;?></td>
                <td class="text-center"><?php echo $pago->FechaPagada;?></td>
                <td class="text-center"><?php echo $pago->FechaVencimiento;?></td>
                <td class="text-center"><?php echo $pago->nro_operacion;?></td>
                <td class="text-center"><?php echo $pago->monto;?></td>
                <td class="text-center">
                  <?php if($pago->voucher_img !=null || $pago->voucher_img !=""){?>
                  <a title="Ver Documento de Pago" target="_blank" class="btn btn-info" href="<?php echo URL_STATIC?>uploads/<?php echo $pago->voucher_img;?>">
                    <i class="fas fa-eye"></i>
                  </a>
                  <?php }else{ ?>
                  <span>Documento no registrado</span>
                  <?php } ?>
                  <?php /*<a href="<?php echo URL_STATIC?>uploads/<?php echo $pago->voucher_img;?>" data-toggle="lightbox" data-title="Voucher <?php echo $pago->FechaPagada?>">
                    <img style="width:50px" src="<?php echo URL_STATIC?>uploads/<?php echo $pago->voucher_img;?>" class="img-fluid mb-2" alt=""/>
                  </a> */?>
                </td>
            </tr>   
            <?php } ?>
  </tbody>
</table>
<?php } else {?>
  <div class="text-center">No se encontraron cuotas registrados</div>
<?php } ?>
<?php /*<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });
  });
</script> */ ?>