<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>HOME</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Buscador</li>
            </ol>
          </div>
        </div>
      </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Buscar por</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formSearchColegiado" class="formSearch" action="">
              <div class="row">
                <div class="col-md-4">
                  <select name="type_filter" id="type_filter" name="type_filter" class="form-control">
                    <option value="name">Apellidos y nombres</option>
                    <option value="dni">DNI</option>
                    <option value="cbpll">Código CBP</option>
                  </select>
                </div>
                <div class="col-md-5">
                   <input type="text" placeholder="Ingrese Apellidos y nombres" class="form-control" id="description_filter" name="description_filter" autocomplete="off">
                </div>
                <div class="col-md-3">
                  <button type="submit" class="btn btn-block btn-outline-primary btn-flat">Buscar</button>
                </div>
              </div>
            </form>
            <div class="row">
              <div class="col-md-12"><br>
                <div class="text-left title_resultados d-none">Resultados de la búsqueda</div>
                <div id="content_table"><?php //$this->load->view('colegiado/table_search')?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo link_static('admin/js/search.js')?>"></script>