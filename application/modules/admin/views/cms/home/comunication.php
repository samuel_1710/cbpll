<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Comunicados en Modal</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li class="breadcrumb-item active">Comunicados</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Imágenes</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row">
                <div class="col-md-12"><br>
                    <div class="form-group">
                        <label for="voucher_file">Imagen del Comunicado</label>
                        <input multiple="true" type="file" accept="image/*" id="comunicado_slide" name="comunicado_slide[]">
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput.css">
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput-rtl.css">

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/piexif.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/locales/es.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/themes/fas/theme.min.js"></script>


<?php $image_initial=[];$image_initialPreviewConfig = []; 
  foreach ($comunications as $image) { 
    array_push($image_initial,URL_UPLOADS . 'slidehome/'.$image->image);
    array_push($image_initialPreviewConfig,
        [   'type' => 'image',      // check previewTypes (set it to 'other' if you want no content preview)
            'caption' => $image->image, // caption
            'key' => $image->comunication_id,       // keys for deleting/reorganizing preview
            'fileId' => $image->comunication_id,    // file identifier
            'url' => base_url()."admin/cms/home/delete_comunication"
        ]
    );
}?>
<script>
    <?php if(isset($comunications)){?>
    var image_initial = <?php echo json_encode($image_initial);?>;
    var image_initialPreviewConfig = <?php echo json_encode($image_initialPreviewConfig);?> 
    <?php }else{?>
    var image_initial = [];
    var image_initialPreviewConfig =[] 
    <?php }?>
</script>
<script src="<?php echo link_static('admin/js/cms/comunication_slider.js');?>"></script>