<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/summernote-bs4.css">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Institucional</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li class="breadcrumb-item active">Nosotros</li>
                    <li class="breadcrumb-item active">Institucional</li>
                </ol>
            </div>
        </div>
        <hr class="shadow-sm pb-1 border-0 mt-0">
    </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formInstitucional" role="form" class="formSearch" action="" enctype="multipart/form-data">
                <div class="row mt-3">
                    <div class="form-group col-md-12">
                        <label for="image_institucional">Imagen para vista institucional</label>
                        <input type="file" class="custom-file-input" id="image_institucional" name="image_institucional">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="textHistoria">Historia</label>
                        <textarea class="form-control customTextArea" name="textHistoria" id="textHistoria" rows="10"><?php echo isset($institucional)?$institucional->history:''?></textarea>
                    </div>
                    <input type="hidden" id="institucional_id" name="institucional_id" value="<?php echo isset($institucional)?$institucional->institucional_id:''?>">
                    <div class="form-group col-md-12">
                        <label for="mision">Misión</label>
                        <textarea class="form-control" name="mision" id="mision" placeholder="Misión" rows="3"><?php echo isset($institucional)?$institucional->mision:''?></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="vision">Visión</label>
                        <textarea class="form-control" name="vision" id="vision" placeholder="Visión" rows="3"><?php echo isset($institucional)?$institucional->vision:''?></textarea>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-3 ">
                        <button type="submit" class="btn btn-block btn-outline-primary btn-flat">Guardar Información</button>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput.css">
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput-rtl.css">

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/piexif.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/locales/es.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/lang/summernote-es-ES.min.js"></script>

<script>
    <?php if(isset($institucional) && $institucional->image!=null){?>
    var image_initial = ["<?php echo URL_UPLOADS . $institucional->image?>"]
    
    <?php }else{?>
    var image_initial = []  
    <?php }?>
</script>
<script src="<?php echo link_static('admin/js/cms/institucional.js');?>"></script>