<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Resoluciones</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li class="breadcrumb-item">Nosotros</li>
                    <li class="breadcrumb-item active">Resoluciones</li>
                </ol>
            </div>
        </div>
        <hr class="shadow-sm pb-1 border-0 mt-0">
    </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Listado</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formSearchColegiado" class="formSearch" action="">
              <div class="row">
                <div class="col-md-3">
                  <a href="#" class="btn btn-block btn-outline-primary btn-flat">Agregar Resolución</a>
                </div>
              </div>
            </form>
            <div class="row">
              <div class="col-md-12"><br>
                <div class="text-left title_resultados d-none">Resultados de la búsqueda</div>
                <div id="content_table">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                            <td class="text-center">N°</td>
                            <td class="text-center">Resolución</td>
                            <td class="text-center">Archivo</td>
                            <th style="width:150px" class="text-center">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="event_name">Lorem ipsum dolor sit amet consectetur adipisicing elit.</td>
                                <td class="event_name">Resolucion11.pdf</td>
                                <td class="text-center">
                                <a href="#" class="btn btn-primary btn-sm" title="Editar"><i class="fas fa-edit"></i></a>
                                <button class="btn btn-warning btn-sm visibility_event" data-id=""  title="Ocultar"><i class="fas fa-eye-slash"></i></button>
                                <button class="btn btn-danger btn-sm delete_event" data-id=""  title="Eliminar"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
            <form id="formComision" class="formSearch" action="">
                <div class="row mt-3">
                    <div class="form-group col-md-12">
                        <label for="resolucion">Resolución</label>
                        <input type="text" class="form-control" id="resolucion" aria-describedby="emailHelp" placeholder="Ingrese nombre del Presidente de la comisión...">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="up_resolucion">Subir Resolución</label>
                        <input type="file" class="form-control-file" id="up_resolucion">
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-md-3 ">
                        <a href="#" class="btn btn-block btn-outline-primary btn-flat">Guardar</a>
                    </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>