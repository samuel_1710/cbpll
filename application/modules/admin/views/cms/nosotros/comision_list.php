<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Comisiones</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li class="breadcrumb-item">Nosotros</li>
                    <li class="breadcrumb-item active">Comisiones</li>
                </ol>
            </div>
        </div>
        <hr class="shadow-sm pb-1 border-0 mt-0">
    </div>
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Listado</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formSearchColegiado" class="formSearch" action="">
              <div class="row">
                <div class="col-md-3">
                  <a href="<?php echo base_url(); ?>admin/cms/nosotros/comision_insert" class="btn btn-block btn-outline-primary btn-flat">Agregar Comisión</a>
                </div>
              </div>
            </form>
            <div class="row">
              <div class="col-md-12"><br>
                <div class="text-left title_resultados d-none">Resultados de la búsqueda</div>
                <div id="content_table">
                <?php if(count($comisiones)>0){?>
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                            <td class="text-center">N°</td>
                            <td class="text-center">Tipo</td>
                            <td class="text-center">Nombre</td>
                            <td class="text-center">Presidente</td>
                            <th style="width:150px" class="text-center">Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i=1;foreach ($comisiones as $comision) { ?>
                            <tr>
                                <td class="text-center"><?php echo $i;?></td>
                                <td class="comision_name"><?php echo $comision->name;?></td>
                                <td class=""><?php echo $comision->type==1?'Comision Permanente':'Comision Transitoria';?></td>
                                <td class=""><?php echo $comision->president;?></td>
                                <td class="text-center">
                                  <a href="<?php echo base_url(); ?>admin/cms/nosotros/comision_insert/<?php echo $comision->comision_id;?>" class="btn btn-primary btn-sm" title="Editar"><i class="fas fa-edit"></i></a>
                                  <button class="btn btn-warning btn-sm visibility_comision" data-visibility="<?php echo $comision->visibility;?>" data-id="<?php echo $comision->comision_id;?>" title="<?php echo $comision->visibility==1?'Ocultar':'Mostrar'?>"><i class="fas <?php echo $comision->visibility==1?'fa-eye-slash':'fa-eye'?>"></i></button>
                                  <button class="btn btn-danger btn-sm delete_comision" data-id="<?php echo $comision->comision_id;?>"  title="Eliminar"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                            <?php $i++;} ?>
                        </tbody>
                    </table>
                  <?php } else {?>
                    <div class="text-center">No se encontraron resultados </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo link_static('admin/js/cms/comision_list.js');?>"></script>