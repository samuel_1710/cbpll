<style>
.file-actions,.file-thumbnail-footer{
    display:none;
}
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro de Boletines/Resoluciones</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">CMS</a></li>
              <li class="breadcrumb-item active">Boletines/Resoluciones</li>
            </ol>
          </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Registrar Boletines/Resoluciones</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="formBoletin" action="">
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="form-group col-md-12">
                            <label for="type_boletin">Tipo de Documento</label>
                            <select class="form-control" id="type_boletin" name="type_boletin">
                                <option value="">Seleccion Tipo de Documento</option>
                                <option <?php echo (isset($obj_boletin) && $obj_boletin->type==1)?'selected':''?> value="1">Boletín</option>
                                <option  <?php echo (isset($obj_boletin) && $obj_boletin->type==2)?'selected':''?> value="2">Resolución</option>
                            </select>
                        </div>
                        <input type="hidden" id="boletin_id" name="boletin_id" value="<?php echo isset($obj_boletin)?$obj_boletin->boletin_id:''?>">
                        <div class="form-group col-md-12">
                            <label for="name_comision">Texto del documento</label>
                            <input value="<?php echo isset($obj_boletin)?$obj_boletin->texto:''?>" type="text" class="form-control" name="text_boletin" id="text_boletin" aria-describedby="emailHelp" placeholder="Ingrese texto del documento">
                        </div>
                        <div class="form-group">
                            <label for="doc_file">Documento a adjuntar</label>
                            <div class="input-group">
                                <div class="custom-file">
                                <input type="file" class="custom-file-input" id="doc_file" name="doc_file">
                                <label class="custom-file-label" for="exampleInputFile">Escoge una archivo para adjuntar</label>
                                </div>
                            </div>
                        </div>
                        <?php if( isset($obj_boletin->filename) ){ ?>
                        <div class="form-group col-md-12">
                            <a target="_blank" href="<?php echo URL_UPLOADS?>docs/<?php echo $obj_boletin->filename;?>">
                                <?php echo $obj_boletin->filename;?>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</section>

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?php echo link_static('admin/js/cms/boletin_insert.js');?>"></script>