<style>
.file-actions,.file-thumbnail-footer{
    display:none;
}
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro de Directivo</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">CMS</a></li>
              <li class="breadcrumb-item active">Consejo Directivo</li>
            </ol>
          </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar Directivo</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="formDirective" role="form">
                  <div class="card-body">
                      <div class="form-group">
                          <label for="voucher_file">Imagen del biologo</label>
                          <input type="file" class="custom-file-input" id="image_directive" name="image_directive">
                      </div>
                      <input type="hidden" id="directivo_id" name="directivo_id" value="<?php echo isset($obj_directive)?$obj_directive->directive_id:''?>">
                      <div class="form-group">
                          <label for="title_event">Nombre</label>
                          <input value="<?php echo isset($obj_directive)?$obj_directive->name:''?>" type="text" class="form-control" id="name_directive" name="name_directive" placeholder="Ingrese nombre del directivo">
                      </div>
                      <div class="form-group">
                          <label for="description">Cargo</label>
													<select class="form-control" name="position_directive" id="position_directive">
                            <option value="">Seleccione el cargo del directivo</option>
                            <option <?php echo isset($obj_directive) && $obj_directive->position=="Decano"?'selected':''?> value="Decano">Decano</option>
                            <option <?php echo isset($obj_directive) && $obj_directive->position=="Vicedecano"?'selected':''?> value="Vicedecano">Vicedecano</option>
														<option <?php echo isset($obj_directive) && $obj_directive->position=="Secretario"?'selected':''?> value="Secretario">Secretario</option>
														<option <?php echo isset($obj_directive) && $obj_directive->position=="Tesorero"?'selected':''?> value="Tesorero">Tesorero</option>
														<option <?php echo isset($obj_directive) && $obj_directive->position=="Primer Vocal"?'selected':''?> value="Primer Vocal">Primer Vocal</option>
														<option <?php echo isset($obj_directive) && $obj_directive->position=="Segundo Vocal"?'selected':''?> value="Segundo Vocal">Segundo Vocal</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <label for="objetivo">Periodo</label>
                          <input value="<?php echo isset($obj_directive)?$obj_directive->periodo:''?>" type="text" class="form-control" id="periodo" name="periodo" placeholder="Ingrese el periodo">
                      </div>                  
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                      <button type="submit" class="btn btn-success">Guardar</button>
                  </div>
              </form>
        </div>
    </div>
  </div>
</section>
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput.css">
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput-rtl.css">

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/piexif.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/locales/es.js"></script>

<script>
    <?php if(isset($obj_directive)){?>
    var image_initial = [
        '<img src="<?php echo isset($obj_directive)? URL_UPLOADS . 'directivos/'.$obj_directive->image:'';?>" class="kv-preview-data file-preview-image">'
    ]
    <?php }else{?>
    var image_initial = [ ]  
    <?php }?>
</script>
<script src="<?php echo link_static('admin/js/cms/directive_insert.js');?>"></script>