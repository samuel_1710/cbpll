<style>
.file-actions,.file-thumbnail-footer{
    display:none;
}
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro de Comisiones</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">CMS</a></li>
              <li class="breadcrumb-item active">Comisiones</li>
            </ol>
          </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar Comisiones</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="formComision" class="formSearch" action="">
                <div class="card-body">
                    <div class="row mt-3">
                        <div class="form-group col-md-12">
                            <label for="tipoComision">Tipo de Comisión</label>
                            <select class="form-control" id="tipoComision" name="tipoComision">
                                <option value="">Seleccion Tipo de Comisión</option>
                                <option <?php echo (isset($obj_comision) && $obj_comision->type==1)?'selected':''?> value="1">Comisión Permamente</option>
                                <option  <?php echo (isset($obj_comision) && $obj_comision->type==2)?'selected':''?> value="2">Comisión Transitoria</option>
                            </select>
                        </div>
                        <input type="hidden" id="comision_id" name="comision_id" value="<?php echo isset($obj_comision)?$obj_comision->comision_id:''?>">
                        <div class="form-group col-md-12">
                            <label for="name_comision">Nombre de Comisión</label>
                            <input value="<?php echo isset($obj_comision)?$obj_comision->name:''?>" type="text" class="form-control" name="name_comision" id="name_comision" aria-describedby="emailHelp" placeholder="Ingrese nombre de Comisión">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="presidente_comision">Nombre de Presidente</label>
                            <input value="<?php echo isset($obj_comision)?$obj_comision->president:''?>" type="text" class="form-control" name="presidente_comision" id="presidente_comision" aria-describedby="emailHelp" placeholder="Ingrese nombre del Presidente de la comisión">
                        </div>
                        <div class="form-group col-md-12">
                            <label for="miembros_comision">Miembros de la Comisión</label>
                            <textarea class="form-control" name="members_comision" id="members_comision" rows="3"><?php echo isset($obj_comision)?$obj_comision->members:''?></textarea>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success btn-flat">Guardar</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</section>
<script src="<?php echo link_static('admin/js/cms/comision_insert.js');?>"></script>