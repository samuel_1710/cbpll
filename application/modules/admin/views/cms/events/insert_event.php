<style>
.file-actions,.file-thumbnail-footer{
    display:none;
}
</style>
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro de Eventos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">CMS</a></li>
              <li class="breadcrumb-item active">Eventos</li>
            </ol>
          </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar Evento</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="formEvent" role="form">
                  <div class="card-body">
                      <div class="form-group">
                          <label for="voucher_file">Imagen del evento</label>
                          <input type="file" class="custom-file-input" id="image_event" name="image_event">
                      </div>
                      <input type="hidden" id="event_id" name="event_id" value="<?php echo isset($obj_event)?$obj_event->event_id:''?>">
                      <div class="form-group">
                          <label for="title_event">Título de Evento</label>
                          <input value="<?php echo isset($obj_event)?$obj_event->title:''?>" type="text" class="form-control" id="title_event" name="title_event" placeholder="Ingrese titulo para evento y/o actividad">
                      </div>
                      <div class="form-group">
                          <label for="description">Descripción del Evento</label>
                          <textarea style="resize: none;" class="form-control" id="description" name="description" placeholder="Descripción" autocomplete="off"><?php echo isset($obj_event)?$obj_event->description:''?></textarea>
                      </div>
                      <div class="form-group">
                          <label for="objetivo">Objetivo</label>
                          <input value="<?php echo isset($obj_event)?$obj_event->objetivo:''?>" type="text" class="form-control" id="objetivo" name="objetivo" placeholder="Ingrese objetivo del evento y/o actividad">
                      </div>
                      <div class="form-group">
                          <label for="horario">Horario</label>
                          <input value="<?php echo isset($obj_event)?$obj_event->horario:''?>" type="text" class="form-control" id="horario" name="horario" placeholder="Ingrese horario del evento y/o actividad">
                      </div>
                      <div class="form-group">
                          <label for="horario">Duración</label>
                          <input value="<?php echo isset($obj_event)?$obj_event->duracion:''?>" type="text" class="form-control" id="duracion" name="duracion" placeholder="Ingrese duracion del evento y/o actividad">
                      </div>
                      <div class="form-group">
                          <label for="temario">Temario</label>
                          <textarea  class="form-control" id="temario" name="temario" placeholder="Temario" autocomplete="off"><?php echo isset($obj_event)?$obj_event->temario:''?></textarea>
                      </div>                  
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                      <button type="submit" class="btn btn-success">Guardar</button>
                  </div>
              </form>
        </div>
    </div>
  </div>
</section>
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput.css">
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/css/fileinput-rtl.css">

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/piexif.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/plugins/sortable.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-fileinput/js/locales/es.js"></script>

<script>
    <?php if(isset($obj_event)){?>
    var image_initial = [
        '<img src="<?php echo isset($obj_event)? URL_UPLOADS . 'events/'.$obj_event->image:'';?>" class="kv-preview-data file-preview-image">'
    ]
    <?php }else{?>
    var image_initial = [ ]  
    <?php }?>
</script>
<script src="<?php echo link_static('admin/js/cms/event_insert.js');?>"></script>