<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/summernote-bs4.css">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro de Trámite</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">CMS</a></li>
              <li class="breadcrumb-item active">Trámites</li>
            </ol>
          </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Registrar Trámite</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="formProcedure" role="form">
                  <div class="card-body">
                      <input type="hidden" id="procedure_id" name="procedure_id" value="<?php echo isset($obj_procedure)?$obj_procedure->procedure_id:''?>">
                      <div class="form-group">
                          <label for="title_event">Nombre del Trámite</label>
                          <input value="<?php echo isset($obj_procedure)?$obj_procedure->title:''?>" type="text" class="form-control" id="name_tramite" name="name_tramite" placeholder="Ingrese nombre del trámite">
                      </div>
                      <div class="form-group">
                          <label for="description">Descripción</label>
							            <textarea class="form-control customTextArea" name="description_tramite" id="description_tramite" rows="10"><?php echo isset($obj_procedure)?$obj_procedure->description:''?></textarea>
                      </div>
                      <div class="form-group">
                          <label for="objetivo">Nota (opcional)</label>
                          <input value="<?php echo isset($obj_procedure)?$obj_procedure->note:''?>" type="text" class="form-control" id="note" name="note" placeholder="Ingrese el periodo">
                      </div>                  
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                      <button type="submit" class="btn btn-success">Guardar</button>
                  </div>
              </form>
        </div>
    </div>
  </div>
</section>

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/lang/summernote-es-ES.min.js"></script>

<script src="<?php echo link_static('admin/js/cms/procedure_insert.js');?>"></script>