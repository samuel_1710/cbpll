<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edición de Comprobantes de Pagos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Pagos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Editar Comprobante de Pago</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form id="formEditCuota" role="form">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Colegiado</label>
                          <input type="text" disabled="true" class="form-control" id="colegiado" name="colegiado" value="<?php echo $pago->ApellidosPaterno." ".$pago->ApellidoMaterno." ".$pago->NombresUsuario?>">
                        </div>
                        <div class="form-group">
                          <label for="nro_operacion">Comprobante de Pago</label>
                          <input value="<?php echo isset($pago->nro_operacion)?$pago->nro_operacion:''?>" type="text" class="form-control" id="nro_operacion" name="nro_operacion" placeholder="Nro de Operación">
                        </div>
                        <div class="form-group">
                          <label for="date_vencimiento">Último mes pagado</label>
                          <input value="<?php echo isset($pago->FechaVencimiento)?$pago->FechaVencimiento:''?>" type="text" class="form-control" id="date_vencimiento" name="date_vencimiento" placeholder="Último mes pagado" autocomplete="off">
                        </div>
                        <div class="form-group">
                          <label for="voucher_file">Documento de Pago</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="voucher_file" name="voucher_file">
                              <label class="custom-file-label" for="exampleInputFile">Escoge una imagen del voucher</label>
                            </div>
                          </div>
                        </div>
                        <?php if( isset($pago->voucher_img) ){ ?>
                        <div class="form-group">
                          <a href="<?php echo URL_STATIC?>uploads/<?php echo $pago->voucher_img;?>" data-toggle="lightbox" data-title="Voucher <?php echo $pago->FechaPagada?>">
                            <img style="width:50px" src="<?php echo URL_STATIC?>uploads/<?php echo $pago->voucher_img;?>" class="img-fluid mb-2" alt=""/>
                          </a>
                        </div>
                        <?php } ?>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="date_pago">Fecha de Pago</label>
                          <input value="<?php echo isset($pago->FechaPagada)?$pago->FechaPagada:''?>" type="text" class="form-control" id="date_pago" name="date_pago" placeholder="Fecha de Pago" autocomplete="off">
                        </div>
                        <div class="form-group">
                          <label for="monto_pago">Monto</label>
                          <input value="<?php echo isset($pago->monto)?$pago->monto:''?>" type="text" class="form-control" id="monto_pago" name="monto_pago" placeholder="Monto" autocomplete="off">
                        </div>
                        <div class="form-group">
                          <label for="detalle_pago">Detalle</label>
                          <textarea 
                            style="resize: none;"
                            class="form-control" id="detalle_pago" name="detalle_pago" 
                            placeholder="Detalle" autocomplete="off"><?php echo isset($pago->detail)?rtrim($pago->detail):''?></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <input id="payment_id" name="payment_id" type="hidden" value="<?php echo $cuota_id;?>">
                  <div class="card-footer">
                    <button type="submit" class="btn btn-success">Editar</button>
                  </div>
                </form>
              </div>
        </div>
    </div>
  </div>
</section>

<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<link rel="styleshhet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css">

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/select2/js/select2.full.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/select2/js/i18n/es.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?php echo link_static('admin/js/payments.js');?>"></script>
<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });
  });
</script>