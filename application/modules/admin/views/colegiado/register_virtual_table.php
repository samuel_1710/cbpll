<?php if(count($cards)>0){?>
<table id="tableRegisterVirtual" class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-center">N°</td>
      <td class="text-center">Nombres y Apellidos</td>
      <td class="text-center">Fecha de Registro</td>
      <th class="text-center">Opciones</th>
    </tr>
  </thead>
  <tbody>
            <?php $i=1;foreach ($cards as $card) { ?>
            <tr>
                <td class="text-center"><?=$i;?></td>
                <td class="nameUser"><?php echo $card->ApellidosPaterno." ".$card->ApellidoMaterno." ".$card->NombresUsuario;?></td>
                <td class="text-center">
                  <?php echo $card->created_at;?>
                </td>
                <td class="text-center">
                  <a href="<?php echo base_url(); ?>admin/verificar-registro/<?php echo $card->idColegiado;?>" class="btn btn-success btn-sm"><i class="fas fa-check"></i> Verificar datos</a>
                  <button class="btn btn-danger btn-sm delete_register_virtual" data-user="<?php echo $card->Usuarios_idUsuarios?>" title="Eliminar"><i class="fas fa-trash-alt"></i></button>
                </td>
            </tr>   
            <?php $i++;} ?>
  </tbody>
</table>
<?php } else {?>
  <div class="text-center">No se encontraron resultados en su búsqueda</div>
<?php } ?>
<script src="<?php echo link_static('admin/js/register_virtual_datatable.js')?>"></script>