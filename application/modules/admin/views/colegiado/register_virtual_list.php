<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Listado de Registro Virtual</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Registro Virtual</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Listado</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <p>
                <a href="<?php echo base_url(); ?>admin/registro-virtual" class="btn btn-primary btn-flat"><i class="fas fa-user-plus"></i> Agregar nuevo registro</a>
            </p>
            <div class="row">
              <div class="col-md-12"><br>
                <div id="content_table"><?php $this->load->view('colegiado/register_virtual_table')?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>