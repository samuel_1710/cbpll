<?php if(count($colegiados)>0){?>
<table id="example2" class="table table-bordered table-hover">
  <thead>
    <tr>
      <td class="text-center">N°</td>
      <td class="text-center">Código CB</td>
      <td class="text-center">Nombres y Apellidos</td>
      <th class="text-center">Habilidad</th>
      <th class="text-center">Opciones</th>
    </tr>
  </thead>
  <tbody>
            <?php $i=1;foreach ($colegiados as $colegiado) { ?>
            <tr>
                <td class="text-center"><?=$i;?></td>
                <td class="text-center"><?php echo $colegiado->CodigoColegiado==''?'-':$colegiado->CodigoColegiado;?></td>
                <td class="nameUser"><?php echo $colegiado->ApellidosPaterno." ".$colegiado->ApellidoMaterno." ".$colegiado->NombresUsuario;?></td>
                <td class="text-center">
                  <?php 
                    echo status_colegiado($colegiado->type_member,$colegiado->FechaUltimoPago,$colegiado->EstadoColegiado);
                  ?>
                </td>
                <td class="text-center">
                  <?php if($this->session->session_cbpll["type"] == "Secretaria"){ ?>
                  <a href="<?php echo base_url(); ?>admin/editar-colegiado/<?php echo $colegiado->idColegiado;?>" class="btn btn-primary btn-sm" title="Editar"><i class="fas fa-edit"></i></a>
                  <?php } ?>
                  <?php if($this->session->session_cbpll["type"] == "Secretaria"){ ?>
                  <a href="<?php echo base_url(); ?>admin/reset-password/<?php echo $colegiado->idColegiado;?>" class="btn btn-warning btn-sm text-white" title="Cambiar contraseña"><i class="fas fa-key"></i></a>
                  <?php } ?>
                  <a href="<?php echo base_url(); ?>admin/status-financiero/<?php echo $colegiado->idColegiado;?>" title="Reporte Financiero" class="btn btn-success btn-sm"><i class="fas fa-coins"></i></a>
                  <?php if($this->session->session_cbpll["type"] == "Secretaria"){ ?>
                    <button class="btn btn-danger btn-sm delete_colegiado" data-user="<?php echo $colegiado->Usuarios_idUsuarios?>" title="Eliminar"><i class="fas fa-trash-alt"></i></button>
                  <?php } ?>
                </td>
            </tr>   
            <?php $i++;} ?>
  </tbody>
</table>
<?php } else {?>
  <div class="text-center">No se encontraron resultados en su búsqueda</div>
<?php } ?>