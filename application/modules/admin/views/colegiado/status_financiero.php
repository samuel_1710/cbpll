<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reporte de Cuotas</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Reporte de Cuotas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Listado de Cuotas</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <?php /*if($this->session->session_cbpll["type"] == "Finanzas"){ 
              if($colegiado->is_vitalicio==0){?>
                <div class="row">
                  <div class="my-2 ml-auto">
                      <button class="btn btn-info btn-sm set_vitalicio" data-colegiado="<?php echo $colegiado->Usuarios_idUsuarios?>">
                        <i class="fas fa-infinity"></i> Establecer Vitalicio</button>
                  </div>
                </div>
              <?php }else if($colegiado->is_vitalicio==1){ ?>
                <div class="row">
                  <div class="my-2 ml-auto">
                    <p class="text-info"><i class="fas fa-star text-warning"></i> Colegiado Vitalicio</p>
                  </div>
                </div>
              <?php } ?>
            <?php } */?>
            <input type="hidden" id="colegiado_id" value="<?php echo $colegiado->idColegiado?>">
            <form id="formSearchColegiado" class="formSearch" action="">
              <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_pat">Apellido Paterno</label>
                            <input disabled value="<?php echo $colegiado->ApellidosPaterno?>" type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="Ingrese su apellido paterno">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_mat">Apellido Materno</label>
                            <input disabled value="<?php echo $colegiado->ApellidoMaterno?>" type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="Ingrese su apellido materno">
                        </div>
                    </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Nombres</label>
                        <input disabled value="<?php echo $colegiado->NombresUsuario?>" type="text" class="form-control" id="name" name="name" placeholder="Ingrese sus nombres">
                    </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <label for="nro_doc">Documento</label>
                      <input disabled value="<?php echo $colegiado->TipoDocumento." : ".$colegiado->NumeroDocumento; ?>" type="text" class="form-control" name="nro_doc" id="nro_doc" placeholder="Ingrese nro de documento">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Denominación de Miembro</label>
                        <select class="form-control" name="type_member" id="type_member">
                          <option <?php echo $colegiado->type_member==""?'selected':''?> value="">Seleccione Tipo de Miembro</option>
                          <option <?php echo $colegiado->type_member==="Ordinario"?'selected':''?> value="Ordinario">Miembro Ordinario</option>
                          <option <?php echo $colegiado->type_member==="Honorario"?'selected':''?> value="Honorario">Miembro Honorario</option>
                          <option <?php echo $colegiado->type_member==="Temporal"?'selected':''?> value="Temporal">Miembro Temporal</option>
                          <option <?php echo $colegiado->type_member==="Vitalicio"?'selected':''?> value="Vitalicio">Miembros Vitalicio</option>
                        </select>
                        <div class="preload"></div>
                    </div>
                </div>
                
              </div>
            </form>
            <div class="row">
              <div class="col-md-12"><br>
                <div id="content_table"><?php $this->load->view('colegiado/status_financiero_table')?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo link_static('admin/js/list_cuotas.js')?>"></script>