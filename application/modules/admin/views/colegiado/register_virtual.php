<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro Virtual</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Registro Virtual</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Registro Virtual</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formPreRegistro" class="form-horizontal formColegiado" role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_pat">Apellido Paterno</label>
                            <input type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="Ingrese su apellido paterno">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_mat">Apellido Paterno</label>
                            <input type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="Ingrese su apellido materno">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Nombres</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Ingrese sus nombres">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_nac">Fecha Nac.</label>
                            <input type="text" class="form-control" id="date_nac" name="date_nac" placeholder="Ingrese su fecha de nacimiento" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type_doc">Tipo de Documento</label>
                            <select class="form-control" name="type_doc" id="type_doc">
                                <option value="DNI">DNI</option>
                                <option value="CE">CE</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nro_doc">Nro de Documento</label>
                            <input type="text" class="form-control" name="nro_doc" id="nro_doc" placeholder="Ingrese nro de documento">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nationality">Nacionalidad</label>
                            <select class="form-control" name="nationality" id="nationality">
                                <option value="Peruana">Peruana</option>
                                <option value="Brasilera">Brasilera</option>
                                <option value="Chilena">Chilena</option>
                                <option value="Cubana">Cubana</option>
                                <option value="Mexicana">Mexicana</option>
                                <option value="Venezolana">Venezolana</option>
                                <option value="Hispano Hablantes">Hispano Hablantes</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="group_sangre" >Grupo Sanguíneo</label>
                            <select name="group_sangre" id="group_sangre" class="form-control">
                                <option value="ARH+">ARH+</option>
                                <option value="BRH+">BRH+</option>
                                <option value="ORH+">ORH+</option>
                                <option value="ABRH+">ABRH+</option>
                                <option value="ARH-">ARH-</option>
                                <option value="BRH-">BRH-</option>
                                <option value="ORH-">ORH-</option>
                                <option value="ABRH-">ABRH-</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ejemplo_email_3">Estado Civil</label>
                            <select class="form-control" name="estado_civil" id="estado_civil">
                                <option value="soltero">Soltero</option>
                                <option value="casado">Casado</option>
                                <option value="viudo">Viudo</option>
                                <option value="divorciado">Divorciado</option>                                
                                <option value="conviviente">Conviviente</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="genero">Género</label>
                            <select class="form-control" name="genero" id="genero">
                                <option value="1">Hombre</option>
                                <option value="2">Mujer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Dirección</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Ingrese su dirección"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dpto">Departamento</label>
                            <!--<input type="text" class="form-control" id="dpto" name="dpto" placeholder="Ingrese Departamento"> -->
                            <select class="form-control" name="dpto" id="dpto">
                                <option value="">Seleccione Departamento</option>
                                <?php foreach($obj_departamento as $obj_departamento){ ?>
                                    <option value="<?php echo $obj_departamento->id; ?>">
                                        <?php echo $obj_departamento->name; ?>
                                    </option>
                                <?php } ?> 
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="prov">Provincia</label>
                            <select class="form-control" name="prov" id="prov">
                                <option value="">Seleccione Provincia</option>
                            </select> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="district">Distrito</label>
                            <!-- <input type="text" class="form-control" id="district" name="district" placeholder="Ingrese Distrito"> -->
                            <select class="form-control" name="district" id="district">
                                <option value="">Seleccione Distrito</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Télefono</label>
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Ingrese su télefono"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="celphone">Celular</label>
                            <input type="text" class="form-control" id="celphone" name="celphone" placeholder="Ingrese su celular">  
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Ingrese su email"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Blog/Web</label>
                            <input type="text" class="form-control" id="linkweb" name="linkweb" placeholder="Ingrese link de Blog o Web"> 
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo link_static('admin/js/ubigeo.js')?>"></script>
<script src="<?php echo link_static('admin/js/register_virtual.js')?>"></script>