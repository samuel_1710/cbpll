<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Verificar Ficha Virtual</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Verificar Ficha Virtual</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Verificar Ficha Virtual</h3>
          </div>
          <!-- /.card-header -->
          <?php if(isset($colegiado)){?>
          <div class="card-body">
            <form id="formPreRegistro" class="form-horizontal formColegiado" role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_pat">Apellido Paterno</label>
                            <input value="<?php echo $colegiado->ApellidosPaterno?>" type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="Ingrese su apellido paterno">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ape_mat">Apellido Materno</label>
                            <input value="<?php echo $colegiado->ApellidoMaterno?>" type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="Ingrese su apellido materno">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Nombres</label>
                            <input value="<?php echo $colegiado->NombresUsuario?>" type="text" class="form-control" id="name" name="name" placeholder="Ingrese sus nombres">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="date_nac">Fecha Nac.</label>
                            <input value="<?php echo $colegiado->FechaNacimiento?>" type="text" class="form-control" id="date_nac" name="date_nac" placeholder="Ingrese su fecha de nacimiento" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type_doc">Tipo de Documento</label>
                            <select class="form-control" name="type_doc" id="type_doc">
                                <option <?php echo $colegiado->TipoDocumento=="DNI"?'selected':''?> value="DNI">DNI</option>
                                <option <?php echo $colegiado->TipoDocumento=="CE"?'selected':''?> value="CE">CE</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nro_doc">Nro de Documento</label>
                            <input value="<?php echo $colegiado->NumeroDocumento; ?>" type="text" class="form-control" name="nro_doc" id="nro_doc" placeholder="Ingrese nro de documento">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nationality">Nacionalidad</label>
                            <select class="form-control" name="nationality" id="nationality">
                                <option value="Peruana">Peruana</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="group_sangre" >Grupo Sanguíneo</label>
                            <select name="group_sangre" id="group_sangre" class="form-control">
                                <option <?php echo $colegiado->TipoSangre=="ARH+"?'selected':''?> value="ARH+">ARH+</option>
                                <option <?php echo $colegiado->TipoSangre=="BRH+"?'selected':''?> value="BRH+">BRH+</option>
                                <option <?php echo $colegiado->TipoSangre=="ORH+"?'selected':''?> value="ORH+">ORH+</option>
                                <option <?php echo $colegiado->TipoSangre=="ABRH+"?'selected':''?> value="ABRH+">ABRH+</option>
                                <option <?php echo $colegiado->TipoSangre=="ARH-"?'selected':''?> value="ARH-">ARH-</option>
                                <option <?php echo $colegiado->TipoSangre=="BRH-"?'selected':''?> value="BRH-">BRH-</option>
                                <option <?php echo $colegiado->TipoSangre=="ORH-"?'selected':''?> value="ORH-">ORH-</option>
                                <option <?php echo $colegiado->TipoSangre=="ABRH-"?'selected':''?> value="ABRH-">ABRH-</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="estado_civil">Estado Civil</label>
                            <select class="form-control" name="estado_civil" id="estado_civil">
                                <option <?php echo $colegiado->EstadoCivil=="soltero"?'selected':''?> value="soltero">Soltero</option>
                                <option <?php echo $colegiado->EstadoCivil=="casado"?'selected':''?> value="casado">Casado</option>
                                <option <?php echo $colegiado->EstadoCivil=="viudo"?'selected':''?> value="viudo">Viudo</option>
                                <option <?php echo $colegiado->EstadoCivil=="divorciado"?'selected':''?> value="divorciado">Divorciado</option>
                                <option <?php echo $colegiado->EstadoCivil=="conviviente"?'selected':''?> value="conviviente">Conviviente</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="genero">Género</label>
                            <select class="form-control" name="genero" id="genero">
                                <option <?php echo $colegiado->GeneroUsuario=="1"?'selected':''?> value="1">Hombre</option>
                                <option <?php echo $colegiado->GeneroUsuario=="2"?'selected':''?> value="2">Mujer</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Especialidad</label>
                            <select class="form-control" name="speciality" id="speciality">
                                <option <?php echo $colegiado->speciality==""?'selected':''?> value="">Seleccione Especialidad</option>
                                <option <?php echo $colegiado->speciality==="Biólogo"?'selected':''?> value="Biólogo">Biólogo</option>
                                <option <?php echo $colegiado->speciality==="Microbiólogo"?'selected':''?> value="Microbiólogo">Microbiólogo</option>
                                <option <?php echo $colegiado->speciality==="Pesquero"?'selected':''?> value="Pesquero">Pesquero</option>
                                <option <?php echo other_speciality($colegiado->speciality)==="Otros"?'selected':''?> value="Otros">Otros</option>
                            </select>
                            <div class="mt-2 insert_speciality <?php echo other_speciality($colegiado->speciality)=="Otros"?'':'d-none'?>">
                                <input value="<?php echo $colegiado->speciality?>" type="text" class="form-control" id="speciality_other" name="speciality_other" placeholder="Ingrese la especialidad">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="address">Dirección</label>
                            <input value="<?php echo $colegiado->DireccionUsuario; ?>" type="text" class="form-control" id="address" name="address" placeholder="Ingrese su dirección"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dpto">Departamento</label>
                            <!--<input value="<?php echo $colegiado->DepartamentoUsuario; ?>" type="text" class="form-control" id="dpto" name="dpto" placeholder="Ingrese Departamento">  -->
                            <select name="dpto" id="dpto"  class="form-control">
                                <option value="">Seleccione Departamento</option>
                                <?php foreach($obj_departamento as $obj_departamento){ ?>
                                    <option value="<?php echo $obj_departamento->id; ?>"
                                        <?php if($colegiado->DepartamentoUsuario==$obj_departamento->id){echo "selected";}?>>
                                        <?php echo $obj_departamento->name; ?>
                                    </option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="prov">Provincia</label>
                            <!--<input value="<?php echo $colegiado->ProvinciaUsuario; ?>" type="text" class="form-control" id="prov" name="prov" placeholder="Ingrese Provincia">  -->
                            <select name="prov" id="prov"  class="form-control">
                                <option value="">Seleccione Provincia</option>
                                <?php foreach($obj_provincia as $obj_provincia){ ?>
                                    <option value="<?php echo $obj_provincia->id; ?>"
                                        <?php if($colegiado->ProvinciaUsuario==$obj_provincia->id){echo "selected";}?>>
                                        <?php echo $obj_provincia->name; ?>
                                    </option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="district">Distrito</label>
                            <!--<input value="<?php echo $colegiado->DistritoUsuario; ?>" type="text" class="form-control" id="district" name="district" placeholder="Ingrese Distrito"> -->
                            <select name="district" id="district"  class="form-control">
                                <option value="">Seleccione Distrito</option>
                                <?php foreach($obj_distrito as $obj_distrito){ ?>
                                    <option value="<?php echo $obj_distrito->id; ?>"
                                        <?php if($colegiado->DistritoUsuario==$obj_distrito->id){echo "selected";}?>>
                                        <?php echo $obj_distrito->name; ?>
                                    </option>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="phone">Télefono</label>
                            <input value="<?php echo $colegiado->TelefonoUsuario; ?>" type="text" class="form-control" id="phone" name="phone" placeholder="Ingrese su télefono"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="celphone">Celular</label>
                            <input value="<?php echo $colegiado->CelularUsuario; ?>"  type="text" class="form-control" id="celphone" name="celphone" placeholder="Ingrese su celular">  
                        </div>
                    </div>
                </div>
                <input type="hidden" id="user_id" name="user_id" value="<?php echo $colegiado->idUsuarios;?>">
                <input type="hidden" id="colegiado_id" name="colegiado_id" value="<?php echo $colegiado->idColegiado;?>">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input value="<?php echo $colegiado->EmailUsuario; ?>"  type="text" class="form-control" id="email" name="email" placeholder="Ingrese su email"> 
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="email">Blog/Web</label>
                            <input value="<?php echo $colegiado->Linkweb; ?>"  type="text" class="form-control" id="linkweb" name="linkweb" placeholder="Ingrese link de Blog o Web"> 
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-check"></i> Verificar</button>
                </div>
            </form>
          </div>
          <?php }?>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo link_static('admin/js/ubigeo.js')?>"></script>
<script src="<?php echo link_static('admin/js/verify_colegiado.js')?>"></script>