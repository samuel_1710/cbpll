<link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/summernote-bs4.css">
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Registro/Edición de Usuarios</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Admin</a></li>
              <li class="breadcrumb-item active">Usuarios</li>
            </ol>
          </div>
        </div>
    </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Usuarios</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form id="formUser" role="form">
                  <div class="card-body">
                      <input type="hidden" id="administrativo_id" name="administrativo_id" value="<?php echo isset($obj_user)?$obj_user->idAdministrativos:''?>">
                      <input type="hidden" id="user_id" name="user_id" value="<?php echo isset($obj_user)?$obj_user->idUsuarios:''?>">
                      <div class="form-group">
                        <label for="ape_pat">Apellido Paterno</label>
                        <input value="<?php echo isset($obj_user)?$obj_user->ApellidosPaterno:''?>" type="text" class="form-control" id="ape_pat" name="ape_pat" placeholder="Ingrese apellido paterno">
                      </div>
                      <div class="form-group">
                        <label for="ape_pat">Apellido Materno</label>
                        <input value="<?php echo isset($obj_user)?$obj_user->ApellidoMaterno:''?>" type="text" class="form-control" id="ape_mat" name="ape_mat" placeholder="Ingrese apellido paterno">
                      </div>
                      <div class="form-group">
                        <label for="name">Nombres</label>
                        <input value="<?php echo isset($obj_user)?$obj_user->NombresUsuario:''?>" type="text" class="form-control" id="name" name="name" placeholder="Ingrese sus nombres">
                      </div>
                      <div class="form-group">
                        <label for="title_event">Usuario</label>
                        <input value="<?php echo isset($obj_user)?$obj_user->NumeroDocumento:''?>" type="text" class="form-control" id="username" name="username" placeholder="Ingrese un nombre de usuario">
                      </div>
                      <div class="form-group">
                        <label for="title_event">Contraseña</label>
                        <input value="" type="password" class="form-control" id="password_user" name="password_user" placeholder="Ingrese una contraseña">
                      </div>
                      <div class="form-group">
                        <label for="title_event">Repetir Contraseña</label>
                        <input value="" type="password" class="form-control" id="repeat_password_user" name="repeat_password_user" placeholder="Repita la contraseña">
                      </div>
                      <div class="form-group">
                        <label for="">Permisos</label>
                        <div class="custom-control custom-checkbox">
                          <input <?php echo (isset($obj_user)&&exist_role($obj_user->role,'home'))?'checked':''?> class="custom-control-input" type="checkbox" name="roles[]" id="permissionHome" value="home">
                          <label for="permissionHome" class="custom-control-label">Home</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input <?php echo (isset($obj_user)&&exist_role($obj_user->role,'nosotros'))?'checked':''?> class="custom-control-input" type="checkbox" name="roles[]" id="permissionNosotros" value="nosotros">
                          <label for="permissionNosotros" class="custom-control-label">Nosotros</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input <?php echo (isset($obj_user)&&exist_role($obj_user->role,'tramites'))?'checked':''?> class="custom-control-input" type="checkbox" name="roles[]" id="permissionTramites" value="tramites">
                          <label for="permissionTramites" class="custom-control-label">Trámites</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input <?php echo (isset($obj_user)&&exist_role($obj_user->role,'events'))?'checked':''?> class="custom-control-input" type="checkbox" name="roles[]" id="permissionEvents" value="events">
                          <label for="permissionEvents" class="custom-control-label">Eventos y Actividades</label>
                        </div>
                      </div>                 
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                      <button type="reset" class="btn btn-danger">Cancelar</button>
                      <button type="submit" class="btn btn-success">Guardar</button>
                  </div>
              </form>
        </div>
    </div>
  </div>
</section>

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/summernote-bs4.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/summernote/lang/summernote-es-ES.min.js"></script>

<script src="<?php echo link_static('admin/js/user/user_insert.js');?>"></script>