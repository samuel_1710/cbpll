<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reporte de Colegiados</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Reporte de Colegiados</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Colegiados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formReportColegiado" action="<?php echo base_url()?>admin/reports/export_colegiado" 
            class="formSearch" method="POST">
                <label>Selecciona los campos a exportar</label>
                <div class="row mb-2">
                  <div class="col-md-4">
                      <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" type="checkbox" name="extra" id="checkAll">
                        <label for="checkAll" class="custom-control-label">Seleccionar todo</label>
                      </div>
                  </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="extra"id="check_ap"  value="ApellidosPaterno" checked disabled>
                            <label for="check_ap" class="custom-control-label">Apellido Paterno</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="extra" id="check_am"  value="ApellidoMaterno" checked disabled>
                            <label for="check_am" class="custom-control-label">Apellido Materno</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="extra" id="check_name"  value="NombresUsuario" checked disabled>
                            <label for="check_name" class="custom-control-label">Nombres</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_cod" value="CodigoColegiado">
                            <label for="check_cod" class="custom-control-label">Cod. CBP</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]"  id="check_fecha_colegiado" value="FechaColegiado">
                            <label for="check_fecha_colegiado" class="custom-control-label">Fecha de Colegiatura</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]"  id="check_datepayment" value="FechaUltimoPago">
                            <label for="check_datepayment" class="custom-control-label">Última Fecha de Pago</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]"  id="check_state" value="EstadoColegiado">
                            <label for="check_state" class="custom-control-label">Estado de Colegiatura</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_tipodoc"  value="TipoDocumento">
                            <label for="check_tipodoc" class="custom-control-label">Tipo de Documento</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_nrodoc"  value="NumeroDocumento">
                            <label for="check_nrodoc" class="custom-control-label">Nro de Documento</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_datenac"  value="FechaNacimiento">
                            <label for="check_datenac" class="custom-control-label">Fecha de Nacimiento</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_genero"  value="GeneroUsuario">
                            <label for="check_genero" class="custom-control-label">Género</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="chech_ec"  value="EstadoCivil">
                            <label for="chech_ec" class="custom-control-label">Estado Civil</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_address"  value="DireccionUsuario">
                            <label for="check_address" class="custom-control-label">Dirección</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_district"  value="DistritoUsuario">
                            <label for="check_district" class="custom-control-label">Distrito</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_prov"  value="ProvinciaUsuario">
                            <label for="check_prov" class="custom-control-label">Provincia</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="fields[]" id="check_dpto"  value="DepartamentoUsuario">
                            <label for="check_dpto" class="custom-control-label">Departamento</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="fields[]" id="check_tel"  value="TelefonoUsuario">
                          <label for="check_tel" class="custom-control-label">Télefono</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="fields[]" id="check_cel"  value="CelularUsuario">
                          <label for="check_cel" class="custom-control-label">Celular</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="fields[]" id="check_email"  value="EmailUsuario">
                          <label for="check_email" class="custom-control-label">Email</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="fields[]" id="check_ts"  value="TipoSangre">
                          <label for="check_ts" class="custom-control-label">Tipo de Sangre</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" name="fields[]" id="check_blog"  value="Linkweb">
                          <label for="check_blog" class="custom-control-label">Blog/Web</label>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="text-center">
                    <button id="export_colegiado" type="button" class="btn btn-primary"> Exportar en Excel</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?php echo link_static('admin/js/report/report_colegiado.js')?>"></script>