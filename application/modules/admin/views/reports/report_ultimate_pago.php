<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Reporte de Colegiados</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Inicio</a></li>
              <li class="breadcrumb-item active">Reporte de Colegiados</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- general form elements disabled -->
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Colegiados</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <form id="formReportColegiado" action="<?php echo base_url()?>admin/reports/export_ultimate_pago" 
            class="formSearch" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                          <label for="date_pago">Fecha para Último Pago Cancelado:</label>
                          <input type="text" class="form-control" id="payment_to" name="payment_to" placeholder="Fecha de Pago" autocomplete="off">
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <button id="export_ultimate_pago" type="button" class="btn btn-primary"> Exportar en Excel</button>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<link rel="styleshhet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.css">

<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo link_static('admin/js/report/report_colegiado.js')?>"></script>