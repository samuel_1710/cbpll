<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>CBP La Libertad</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="<?php echo link_static('website/dist/images/favicon.png'); ?>" type="images/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/toastr/toastr.min.css">
  
  <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .login-page{
        background:#2C2E83;
    }
    .img-logo{
        width:320px;
    }
    .form-control,.input-group-text{
        border-radius:0 !important;
        font-size:14px;
    }
    .mb-1{
        font-size:14px;
    }
  </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url(); ?>/admin/login">
        <img class="img-logo" src="<?php echo URL_STATIC?>website/dist/images/logo-light-400x80.png"/>
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Inicia Sesión para comenzar</p>
      <div id="message-validate">

      </div>
      <form id="form_login" action="" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="usuario" placeholder="Usuario">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Contraseña">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" />
        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
            </div>
        </div>
      </form>

      <!--<p class="mb-1">
        <a href="#">Olvidé mi contraseña</a>
      </p>-->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->
<script>
    var site = '<?php echo base_url(); ?>';
</script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/jquery-validation/localization/messages_es.min.js"></script>
<script src="<?php echo URL_STATIC?>admin/template_admin/plugins/toastr/toastr.min.js"></script>

<script src="<?php echo URL_STATIC?>admin/template_admin/js/adminlte.min.js"></script>
<script src="<?php echo link_static('admin/js/login.js')?>"></script>

</body>
</html>
