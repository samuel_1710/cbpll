<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('static_version')) {

    function static_version()
    {
        $fileContentVersion = APPPATH . '../static/last_version';

        if (!file_exists($fileContentVersion)) {
            return 2138;
        }

        return trim(file_get_contents($fileContentVersion));
    }
}

if (!function_exists('link_static')) {

    function link_static($path)
    {
        return URL_STATIC . $path . '?v=' . static_version();
    }
}