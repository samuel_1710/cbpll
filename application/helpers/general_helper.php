<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function status_colegiado($is_vitalicio,$last_payment, $status_colegiado,$to = false){
    $status = '';
    if(isset($last_payment)){
        $fecha_actual = date ( 'Y-m-j');
        $fecha = date($last_payment);
        $nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        
        $fecha_actual_time = strtotime($fecha_actual);
        $nueva_fecha_time = strtotime($nuevafecha);
        if($is_vitalicio==="Vitalicio"){
          $status = '<span class="badge bg-success">Habilitado (Vitalicio)</span>';
        }
        else if($fecha_actual_time > $nueva_fecha_time){
          $status = '<span class="badge bg-danger">Inhabilitado</span>';
        } else {
            $status = '<span class="badge bg-success">Habilitado</span>';
            if($to == true){
                $status.= '<span> hasta el '.date ( 'Y-m-d' ,$nueva_fecha_time).'</span>';
            }
        }
    }else{
        if($is_vitalicio==="Vitalicio"){
          $status = '<span class="badge bg-success">Habilitado (Vitalicio)</span>';
        }
        else if($status_colegiado==1){ // En proceso
          $status = '<span class="badge bg-warning">En Proceso</span>';
        }else if($status_colegiado==2){ //Habilitado
          $status = '<span class="badge bg-success">Habilitado</span>';
        }else if($status_colegiado==3){ // Inhabilitado
          $status = '<span class="badge bg-danger">Inhabilitado</span>';
        }
    }
    return $status;
}
function status_colegiado_report($is_vitalicio,$last_payment,$status_colegiado){
  $status = '';
  if(isset($last_payment)){
      $fecha_actual = date ( 'Y-m-j');
      $fecha = date($last_payment);
      $nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
      $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
      
      $fecha_actual_time = strtotime($fecha_actual);
      $nueva_fecha_time = strtotime($nuevafecha);
      if($is_vitalicio==="Vitalicio"){
        $status = 'Habilitado (Vitalicio)';
      }
      else if($fecha_actual_time > $nueva_fecha_time){
        $status = 'Inhabilitado';
      } else {
          $status = 'Habilitado';
      }
  }else{
      if($is_vitalicio==="Vitalicio"){
        $status = 'Habilitado (Vitalicio)';
      }
      else if($status_colegiado==1){
        $status = 'En Proceso';
      }else if($status_colegiado==2){
        $status = 'Habilitado';
      }else if($status_colegiado==3){
        $status = 'Inhabilitado';
      }
  }
  return $status;
}
function status_colegiado_web($is_vitalicio,$last_payment, $status_colegiado){
    $status = '';
    if(isset($last_payment)){
        $fecha_actual = date ( 'Y-m-j');
        $fecha = date($last_payment);
        $nuevafecha = strtotime ( '+3 month' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-j' , $nuevafecha );
        
        $fecha_actual_time = strtotime($fecha_actual);
        $nueva_fecha_time = strtotime($nuevafecha);
        if($is_vitalicio==="Vitalicio"){
          $status = '<span class="status status--habil">Habilitado (Vitalicio)</span>';
        }
        else if($fecha_actual_time > $nueva_fecha_time){
          $status = '<span class="status status--inhabil">Inhabilitado</span>';
        } else {
          $status = '<span class="status status--habil">Habilitado</span>';
        }
      }else{
        if($is_vitalicio==="Vitalicio"){
          $status = '<span class="status status--habil">Habilitado (Vitalicio)</span>';
        }
        else if($status_colegiado==1){ // En proceso
          $status = '<span class="badge bg-warning">En Proceso</span>';
        }else if($status_colegiado==2){ //Habilitado
          $status = '<span class="status status--habil">Habilitado</span>';
        }else if($status_colegiado==3){ // Inhabilitado
          $status = '<span class="status status--inhabil">Inhabilitado</span>';
        }
      }
    return $status;
}

function slug($string) {

  $characters = array(
      "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
      "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
      "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
  );
  $string = strtr($string, $characters);
  $string = strtolower(trim($string));
  $string = preg_replace('/[^a-z0-9-]/', "-", $string);
  $string = preg_replace("/-+/", "-", $string);
  if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
      $string = substr($string, 0, strlen($string) - 1);
  }
  return $string;

}

function slug_name_img($string) {

  $characters = array(
      "Á" => "A", "Ç" => "c", "É" => "e", "Í" => "i", "Ñ" => "n", "Ó" => "o", "Ú" => "u",
      "á" => "a", "ç" => "c", "é" => "e", "í" => "i", "ñ" => "n", "ó" => "o", "ú" => "u",
      "à" => "a", "è" => "e", "ì" => "i", "ò" => "o", "ù" => "u"
  );
  $string = strtr($string, $characters);
  $string = strtolower(trim($string));
  $string = preg_replace('/[^a-z0-9-]/', "-", $string);
  $string = preg_replace("/-+/", "-", $string);
  if(substr($string, strlen($string) - 1, strlen($string)) === "-") {
      $string = substr($string, 0, strlen($string) - 1);
  }
  return $string;

}

function date_string($date){
  date_default_timezone_set("America/Lima");

  $fecha_separada=explode("-", $date);

  $week_day = weekday(date("N",mktime(0, 0, 0, $fecha_separada[1], $fecha_separada[2], $fecha_separada[0])));
  //$dia= strtolower(numtoletras($fecha_separada[0]));
  $dia = $fecha_separada[2];
  switch ($fecha_separada[1]) {

      case "01":
          $mes="Enero";
          break;
      case "02":
          $mes="Febrero";
          break;
      case "03":
          $mes="Marzo";
          break;
      case "04":
          $mes="Abril";
          break;
      case "05":
          $mes="Mayo";
          break;
      case "06":
          $mes="Junio";
          break;
      case "07":
          $mes="Julio";
          break;
      case "08":
          $mes="Agosto";
          break;
      case "09":
          $mes="Septiembre";
          break;
      case "10":
          $mes="Octubre";
          break;
      case "11":
          $mes="Noviembre";
          break;
      case "12":
          $mes="Diciembre";
          break;

      default:
          break;
  }

  //$anio= strtolower(numtoletras($fecha_separada[2]));
  $anio= $fecha_separada[0];
  return "$dia de $mes del $anio";
}
function weekday($dia){
  switch ($dia) {
      case 7:
          $dia_semana = "Domingo";
          break;
      case 1:
          $dia_semana = "Lunes";
          break;
      case 2:
          $dia_semana = "Martes";
          break;
      case 3:
          $dia_semana = "Miercoles";
          break;
      case 4:
          $dia_semana = "Jueves";
          break;
      case 5:
          $dia_semana = "Viernes";
          break;
      case 6:
          $dia_semana = "Sabado";
          break;
  }
  return $dia_semana;
}
function exist_role($roles,$rol){
  if(is_null($roles)){
    return true;
  }
  $pos = strpos($roles, $rol);
  if ($pos === false) {
    return false;
  } else {
    return true;
  } 
}
function is_null_custom($data){
  return (is_null($data) || $data==="")?'-':$data;
}
function other_speciality($especialidad){
  if(!is_null($especialidad) && $especialidad !== "Biólogo" &&
    $especialidad !== "Microbiólogo" &&
    $especialidad !== "Pesquero" 
  ){
    return "Otros";
  }else{
    return $especialidad;
  }
  
}