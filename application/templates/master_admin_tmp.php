<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin CBPLL</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo link_static('website/dist/images/favicon.png'); ?>" type="images/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/ekko-lightbox/ekko-lightbox.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
    <!-- Google Font: Source Sans Pro -->
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/jquery-validation/localization/messages_es.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/toastr/toastr.min.js"></script>

    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
    <script src="<?php echo URL_STATIC?>admin/template_admin/js/adminlte.min.js"></script>

</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
        </ul>
        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-user"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <?php if($this->session->session_cbpll["type"] == "Colegiado"){ ?>
                    <a href="<?php echo base_url(); ?>admin/mi-perfil" class="dropdown-item">
                        <i class="fas fa-address-card mr-2"></i> Mi Perfil
                    </a>
                    <?php }else{?>
                    <a href="<?php echo base_url(); ?>admin/cambiar-password" class="dropdown-item">
                        <i class="fas fa-address-card mr-2"></i> Mi Perfil
                    </a>
                    <?php } ?>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo base_url(); ?>admin/logout" class="dropdown-item">
                        <i class="fas fa-sign-out-alt mr-2"></i> Cerrar Sesión
                    </a>
                </div>
            </li>
        </ul>
    </nav>
  <!-- /.navbar -->
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
        <a href="<?php echo base_url(); ?>" class="brand-link">
            <img src="<?php echo URL_STATIC?>admin/template_admin/img/logo_web.png"
                alt="AdminLTE Logo"
                class="brand-image img-circle elevation-3"
                style="opacity: .8">
            <span class="brand-text font-weight-light">Colegio de Biólogos</span>
        </a>
    <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                <img src="<?php echo URL_STATIC?>admin/template_admin/img/user-default-160x160.png" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <a href="#" class="d-block"><?php echo $this->session->session_cbpll["nombres"]; ?> </a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <?php if($this->session->session_cbpll["type"] === "Secretaria"){ ?>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p> Colegiados <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/buscador" class="nav-link">
                                <i class="fas fa-search nav-icon"></i>
                                <p>Buscar Colegiados</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/listado-virtual" class="nav-link">
                                <i class="fas fa-address-book nav-icon"></i>
                                <p>Listado Registro Virtual</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon far fa-file-excel"></i>
                            <p> Reportes <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/reporte-colegiado" class="nav-link">
                                <i class="nav-icon"></i>
                                <p>Colegiados</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-header">Administración</li>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/cambiar-password" class="nav-link">
                            <i class="nav-icon fas fa-lock"></i>
                            <p>Cambiar Contraseña</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/logout" class="nav-link">
                            <i class="fas fa-sign-out-alt nav-icon"></i>
                            <p>Cerrar Sesión</p>
                        </a>
                    </li>
                    <?php } else if($this->session->session_cbpll["type"] === "Finanzas"){ ?>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p> Colegiados <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/buscador" class="nav-link">
                                <i class="fas fa-search nav-icon"></i>
                                <p>Buscar Colegiados</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/registro-cuota" class="nav-link">
                                <i class="fas fa-coins nav-icon"></i>
                                <p>Registrar Cuota</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon far fa-file-excel"></i>
                            <p> Reportes <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/reporte-ultimo-pago" class="nav-link">
                                <i class="nav-icon"></i>
                                <p>Último Pago Cancelado</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/reporte-colegiado" class="nav-link">
                                <i class="nav-icon"></i>
                                <p>Colegiados</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-header">Administración</li>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/cambiar-password" class="nav-link">
                            <i class="nav-icon fas fa-lock"></i>
                            <p>Cambiar Contraseña</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/logout" class="nav-link">
                            <i class="fas fa-sign-out-alt nav-icon"></i>
                            <p>Cerrar Sesión</p>
                        </a>
                    </li>
                    <?php } else if($this->session->session_cbpll["type"] == "Colegiado"){ ?>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-address-card"></i>
                            <p> Perfil <i class="right fas fa-angle-left"></i></p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/mi-perfil" class="nav-link">
                                    <i class="fas fa-user nav-icon"></i>
                                    <p>Datos Personales</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/reporte-cuotas" class="nav-link">
                                <i class="fas fa-coins nav-icon"></i>
                                <p>Reporte Cuotas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/cambiar-password" class="nav-link">
                                    <i class="fas fa-unlock-alt nav-icon"></i>
                                    <p>Cambiar Contraseña</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url(); ?>admin/logout" class="nav-link">
                                    <i class="fas fa-sign-out-alt nav-icon"></i>
                                    <p>Cerrar Sesión</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php } else if($this->session->session_cbpll["type"] === "Adminweb") {?>
                        <?php if(exist_role($this->session->session_cbpll["role"],'home') ) {?>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p> Home <i class="right fas fa-angle-left"></i></p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/home" class="nav-link">
                                    <i class="far fa-file-alt nav-icon"></i>
                                    <p>Comunicados</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/home/slider" class="nav-link">
                                    <i class="fas fa-images nav-icon"></i>
                                    <p>Slider Home</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/home/infraestructura" class="nav-link">
                                    <i class="fas fa-building nav-icon"></i>
                                    <p>Infraestructura</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/home/convenios" class="nav-link">
                                    <i class="fas fa-file-signature nav-icon"></i>
                                    <p>Convenios</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if(exist_role($this->session->session_cbpll["role"],'nosotros') ) {?>
                        <li class="nav-item has-treeview">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p> Nosotros <i class="right fas fa-angle-left"></i></p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/nosotros" class="nav-link">
                                    <i class="far fa-building nav-icon"></i>
                                    <p>Institucional</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/nosotros/directivos" class="nav-link">
                                    <i class="fas fa-users nav-icon"></i>
                                    <p>Consejo Directivo</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/nosotros/comisiones" class="nav-link">
                                    <i class="fas fa-users-cog nav-icon"></i>
                                    <p>Comisiones</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?php echo base_url(); ?>admin/cms/nosotros/boletines" class="nav-link">
                                    <i class="fas fa-file-upload nav-icon"></i>
                                    <p>Boletines y Resoluciones</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php if(exist_role($this->session->session_cbpll["role"],'tramites') ) {?>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/cms/procedure" class="nav-link">
                                <i class="nav-icon fas fa-folder-open"></i>
                                <p> Trámites </p>
                            </a>
                        </li>
                        <?php } ?>
                        <?php if(exist_role($this->session->session_cbpll["role"],'events') ) {?>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/cms/events" class="nav-link">
                                <i class="nav-icon fas fa-calendar-alt"></i>
                                <p> Eventos y Actividades </p>
                            </a>
                        </li>
                        <?php } ?>
                        <li class="nav-header">Administración</li>
                        <?php if( is_null($this->session->session_cbpll["role"]) ) {?>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/user" class="nav-link">
                                <i class="nav-icon fas fa-users-cog"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/cambiar-password" class="nav-link">
                                <i class="nav-icon fas fa-lock"></i>
                                <p>Cambiar Contraseña</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url(); ?>admin/logout" class="nav-link">
                                <i class="fas fa-sign-out-alt nav-icon"></i>
                                <p>Cerrar Sesión</p>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </nav>
        <!-- /.sidebar-menu -->
        </div>
    <!-- /.sidebar -->
    </aside>

  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php echo $body;?>
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; <?php echo date("Y")?> <a href="<?php echo base_url(); ?>">CBPLL</a>.</strong> Derechos Reservados.
    </footer>
</div>
<script>
        var site = '<?php echo base_url(); ?>';
        var url = window.location;
        $('ul.nav-sidebar a').filter(function() {
	        return this.href == url;
        })
        .addClass('active')
        .parent().parent().show()
        .parents(".nav-item").addClass("menu-open")
        //.children("a").addClass('active');
        
    </script>
<!-- ./wrapper -->
</body>
</html>
