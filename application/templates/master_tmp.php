<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php 
        $title_default = 'Colegio de Biólogos del Perú - Consejo Regional IV La Libertad.';
        $description_default = 'Comunidad de Profesionales Colegiados en Biología, Microbiología y Biología Pesquera de La Libertad.';
        $property_url_default = base_url();
        $property_image_default = link_static('website/dist/images/slider1920.jpg');
        $icon_default = link_static('website/dist/images/favicon.png');;
    ?>
    <title><?php echo isset($titleSeo)?$titleSeo:$title_default;?></title>
    <meta name="description" content="<?php echo isset($descriptionSeo)?htmlspecialchars($descriptionSeo, ENT_QUOTES):$description_default;?>" />
    <link rel="shortcut icon" href="<?php echo $icon_default;?>" type="images/x-icon">
    <!-- Tag OG/FB-->
    <meta property="og:title" content="<?php echo isset($titleSeo)?$titleSeo:$title_default;?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo isset($obj_seo[0]->url)?base_url().$obj_seo[0]->url:$property_url_default;?>" />
    <meta property="og:image" content="<?php echo isset($obj_seo[0]->image)?$obj_seo[0]->image:$property_image_default;?>" />
    <meta property="og:description" content="<?php echo isset($descriptionSeo)?htmlspecialchars($descriptionSeo, ENT_QUOTES):$description_default;?>" />

    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Work+Sans:300,700,800%7COswald:300,400,500">
    <link rel="stylesheet" href="<?php echo link_static('website/dist/css/cbpll.min.css'); ?>">
</head>
<body>
    <div class="page">
        <?php $this->load->view('partials/header');?>
        <div class="content-view-cbpll">
            <?php echo $body;?>
        </div>
        <?php $this->load->view('partials/footer');?>
    </div>
    <!--<div class="preloader"> 
		<div class="preloader-logo"><img src="<?php echo URL_STATIC?>website/dist/images/preloader_500_100.png" alt="" width="500" height="100" srcset="<?php echo URL_STATIC?>website/dist/images/preloader_500_100.png 2x"/></div>
		<div class="preloader-body">
			<div id="loadingProgressG">
				<div class="loadingProgressG" id="loadingProgressG_1"></div>
			</div>
		</div>
	</div>-->
    <script src="<?php echo link_static('website/dist/js/cbpll.min.js'); ?>"></script>
    <script>
      var site = '<?php echo base_url(); ?>';
    </script>
</body>
</html>