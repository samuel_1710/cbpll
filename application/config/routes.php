<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home/home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['busqueda'] = "home/search/index"; //buscador de colegiados
$route['ficha-virtual'] = "home/card/index"; //ficha virtual de colegiados

// rutas para nosotros
$route['institucional'] = "home/institucional/index"; //institucional de colegiados
$route['consejo-directivo'] = "home/institucional/consejodirectivo"; //consejo directivo de colegiados
$route['comisiones'] = "home/institucional/comisiones";
$route['organigrama'] = "home/institucional/organigrama";
$route['resoluciones'] = "home/institucional/resoluciones";
$route['boletines'] = "home/institucional/boletines";

// rutas para tramites
$route['tramites/colegiatura'] = "home/tramites/index";
$route['tramites/segunda-especialidad'] = "home/tramites/segunda_especialidad";
$route['tramites/traslados-sede'] = "home/tramites/traslados";
$route['tramites/renovacion-carnet'] = "home/tramites/renovacion_carnet";
$route['tramites/otros-tramites'] = "home/tramites/otros_tramites";

$route['tramites'] = "home/tramites/index"; // tramites virtual de colegiados
$route['tramite/(.+)'] = "home/tramites/tramite/$1"; // tramites virtual de colegiados

$route['eventos-actividades'] = "home/events/index"; // tramites virtual de colegiados
$route['evento-actividad/(.+)'] = "home/events/event/$1"; // tramites virtual de colegiados

//$route['admin'] = "admin/login/index"; //login admin
$route['admin/login'] = "admin/login/index"; //login admin
$route['admin/logout'] = "admin/login/logout"; //login admin
$route['admin/buscador'] = "admin/colegiado/index"; // buscador
$route['admin/editar-colegiado/(.+)'] = "admin/colegiado/editar_colegiado/$1"; // editar colegiado
$route['admin/reset-password/(.+)'] = "admin/colegiado/reset_password/$1"; // restablecer colegiado
$route['admin/status-financiero/(.+)'] = "admin/colegiado/status_financiero/$1"; // estado financiero


$route['admin/listado-virtual'] = "admin/colegiado/listado_virtual"; // listado registro virtual
$route['admin/registro-virtual'] = "admin/colegiado/registro_virtual"; // registro virtual
$route['admin/verificar-registro/(.+)'] = "admin/colegiado/verify_colegiado_virtual/$1"; // verificar registro virtual

$route['admin/registro-cuota'] = "admin/colegiado/registro_cuotas"; // registro cuota
$route['admin/editar-cuota/(.+)'] = "admin/colegiado/edit_cuotas/$1"; // edicion cuota

$route['admin/reporte-colegiado'] = "admin/reports/report_colegiado"; // reportes de colegiado
$route['admin/reporte-ultimo-pago'] = "admin/reports/report_ultimate_pago"; // reportes de colegiado
$route['admin/reporte-habilitados'] = "admin/reports/report_habilitados"; // reportes de colegiado

$route['admin/mi-perfil'] = "admin/perfil/index"; // registro virtual
$route['admin/cambiar-password'] = "admin/perfil/change_password"; // cambiar contraseña
$route['admin/reporte-cuotas'] = "admin/perfil/reporte_financiero"; // estado financiero




