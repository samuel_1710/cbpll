<header class="header">
	<nav class="navbar  navbar-expand-lg navbar-light"><!--fixed-top navbar-expand-lg!-->
		<div class="container">
			<a class="navbar-brand" href="<?php echo base_url() ?>">
				<img class="" src="<?php echo URL_STATIC?>website/dist/images/logoinicio_400_80_px.jpg">
			</a> 
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse float-right" id="navbarNavDropdown">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item <?php if($this->uri->uri_string() == '') { echo 'active'; } ?>">
						<a class="nav-link" href="<?php echo base_url() ?>">Inicio</a>
					</li>
					<?php $active = ''; if($this->uri->uri_string() == 'institucional' ||
							$this->uri->uri_string() == 'consejo-directivo' ||
							$this->uri->uri_string() == 'comisiones' ||
							$this->uri->uri_string() == 'boletines' ||
							$this->uri->uri_string() == 'resoluciones' ||
							$this->uri->uri_string() == 'organigrama'
							) { $active='active'; } ?>
					<li class="nav-item <?=$active?> dropdown dropdownHeader">
						<a class="nav-link dropdown-toggle" href="#" id="optionNosotros" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Nosotros
						</a>
						<div class="dropdown-menu" aria-labelledby="optionNosotros">
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'institucional') { echo 'active'; } ?>" href="<?php echo base_url() ?>institucional">Institucional</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'consejo-directivo') { echo 'active'; } ?>" href="<?php echo base_url() ?>consejo-directivo">Consejo Directivo</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'comisiones') { echo 'active'; } ?>" href="<?php echo base_url() ?>comisiones">Comisiones</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'boletines') { echo 'active'; } ?>" href="<?php echo base_url() ?>boletines">Boletines</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'resoluciones') { echo 'active'; } ?>" href="<?php echo base_url() ?>resoluciones">Resoluciones</a>
							<!--<a class="dropdown-item <?php if($this->uri->uri_string() == 'organigrama') { echo 'active'; } ?>" href="<?php echo base_url() ?>organigrama">Organigrama</a>-->
						</div>
					</li>
					<?php $active = ''; if($this->uri->uri_string() == 'tramites/colegiatura' ||
							$this->uri->uri_string() == 'tramites/segunda-especialidad' ||
							$this->uri->uri_string() == 'tramites/traslados-sede' ||
							$this->uri->uri_string() == 'tramites/renovacion-carnet' ||
							$this->uri->uri_string() == 'tramites/otros-tramites'
							) { $active='active'; } ?>
					<?php if(isset($procedures) && count($procedures)>0){?>
					<li class="nav-item <?=$active?> dropdown dropdownHeader">
						<a class="nav-link dropdown-toggle" href="#" id="optionProcedure" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Trámites
						</a>
						<div class="dropdown-menu" aria-labelledby="optionProcedure">
							<?php foreach ($procedures as $procedure) { ?>
								<a class="dropdown-item <?php if($this->uri->uri_string() == 'tramite/'.$procedure->slug) { echo 'active'; } ?>" 
									href="<?php echo base_url() ?>tramite/<?php echo $procedure->slug; ?>">
									<?php echo $procedure->title; ?>
								</a>
							<?php }?>
							<?php /*<a class="dropdown-item <?php if($this->uri->uri_string() == 'tramites/colegiatura') { echo 'active'; } ?>" href="<?php echo base_url() ?>tramites/colegiatura">Colegiatura static</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'tramites/segunda-especialidad') { echo 'active'; } ?>" href="<?php echo base_url() ?>tramites/segunda-especialidad">Segunda Especialización</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'tramites/traslados-sede') { echo 'active'; } ?>" href="<?php echo base_url() ?>tramites/traslados-sede">Traslados de Sede</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'tramites/renovacion-carnet') { echo 'active'; } ?>" href="<?php echo base_url() ?>tramites/renovacion-carnet">Renovación de Carnet</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'tramites/otros-tramites') { echo 'active'; } ?>" href="<?php echo base_url() ?>tramites/otros-tramites">Otros trámites</a> */
							?>
						</div>
					</li>
					<?php }?>
					<?php $active = ''; if($this->uri->uri_string() == 'ficha-virtual' ||
							$this->uri->uri_string() == 'busqueda'
							) { $active='active'; } ?>
					<li class="nav-item <?=$active?> dropdown dropdownHeader">
						<a class="nav-link dropdown-toggle" href="#" id="optionProcedure" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Servicios
						</a>
						<div class="dropdown-menu" aria-labelledby="optionProcedure">
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'ficha-virtual') { echo 'active'; } ?>" href="<?php echo base_url() ?>ficha-virtual">Registro Virtual</a>
							<a class="dropdown-item <?php if($this->uri->uri_string() == 'busqueda') { echo 'active'; } ?>" href="<?php echo base_url() ?>busqueda">Consultar Habilitación</a>
						</div>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url() ?>eventos-actividades">Eventos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" target="_blank" href="http://www.cbperu.org.pe/">Consejo Nacional</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url() ?>admin/login">
						Intranet
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>