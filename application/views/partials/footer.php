<!-- Page Footer-->
<footer class="section footer-minimal bg-gray-700">
	<div class="container">
		<hr>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<a class="brand mb-2" href="<?php echo base_url(); ?>">
					<img src="<?php echo URL_STATIC?>website/dist/images/logo-light-400x80.png" alt="" width="400" height="80" srcset="<?php echo URL_STATIC?>website/dist/images/logo-light-400x80.png 2x"/>
				</a>
				<div class="d-none d-sm-none d-md-block my-2 ml-3">
					<span>&copy;&nbsp;Copyright </span>
					<span class="copyright-year"><?php echo date("Y")?></span>
					<span>&nbsp;</span><span>Todos los derechos reservados CBPLL.</span>
				</div>
			</div>
			<div class="col-12 col-md-3">
				<h5>Contacto</h5>
				<ul class="list-unstyle">
					<li>
						<a href="#" class="text-decoration-none">
							<i class="fas fa-map-marked-alt"></i> Manuel Encinas Mz "S" Lote 27 Urb. Miraflores
						</a>
					</li>
					<li><a class="text-decoration-none" href="#"><i class="fas fa-envelope"></i> informes.cbpll@gmail.com</a></li>
					<li><a class="text-decoration-none" href="#"><i class="fas fa-phone"></i> (044)-244142</a></li>
					<li><a class="text-decoration-none" href="#"><i class="fab fa-whatsapp"></i> 960541137</a></li>
				</ul>
			</div>
			<div class="col-12 col-md-3">
				<h5>Enlaces de Interés</h5>
				<ul class="list-unstyle">
					<li>
						<a class="brand" target="_blank" href="<?php echo URL_STATIC?>website/dist/docs/MANUAL_DE_USUARIO_WEB.pdf">Manual de Usuario - Sistema CBPLL</a>
					</li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
			<div class="d-block d-sm-block d-md-none mt-2 col-12 text-center">
				<span>&copy;&nbsp;Copyright </span>
				<span class="copyright-year"><?php echo date("Y")?></span>
				<span>&nbsp;</span><span>Todos los derechos reservados CBPLL.</span>
			</div>
		</div>
	</div>
</footer>