<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class  MY_Controller  extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        /*$this->load->model("Customers_model", "obj_customers");
        $this->load->model('project_model', 'obj_project');
        $this->load->library('pdf');
        $this->load->library('fpdf17/fpdf');
        $this->pdf = new FPDF();
        $this->load->library('m_pdf');
        $this->load->helper('link_static_cms');
        */
        //$this->redirectNewPage();
        //$this->isRememberLogged();
    }
    public function get_tramites(){
        $this->load->model('Procedures_model', 'procedure_model');
        $data = $this->procedure_model->get_procedure_visibility();
        return $data;
        
    }
}