<?php

class MY_Model extends CI_Model
{
    public function insert($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insert_lote($data){
        $this->db->insert_batch($this->table, $data);
    }

    public function update($pk, $data){
        $this->db->where($this->table_id, $pk);
        $this->db->update($this->table, $data);
    }

    public function delete($pk){
        $this->db->where($this->table_id, $pk);
        $this->db->delete($this->table);
    }

    public function get_search_row($data){
        if (isset($data["select"])&& $data["select"]!=""){$this->db->select($data["select"]);}
        if (isset($data["where"]) && $data["where"]!=""){$this->db->where($data["where"]);}
        if (isset($data["order"]) && $data["order"]!=""){$this->db->order_by($data["order"]);}
        if (isset($data["group"]) && $data["group"]!=""){$this->db->group_by($data["group"]);}
        if (isset($data["having"]) && $data["having"]!=""){$this->db->having($data["having"]);}
        if (isset($data["join"])){if (count($data["join"])>0){foreach ($data["join"] as $rowJoin){$split = explode(",",$rowJoin);$this->db->join($split[0],$split[1],isset($split[2])?$split[2]:"");}}}
        $this->db->from($this->table);
        $query=  $this->db->get();
        return $query->row();
    }

    public function search($data){
        if (isset($data["select"])&& $data["select"]!=""){$this->db->select($data["select"]);}
        if (isset($data["where"]) && $data["where"]!=""){$this->db->where($data["where"]);}
        if (isset($data["order"]) && $data["order"]!=""){$this->db->order_by($data["order"]);}
        if (isset($data["group"]) && $data["group"]!=""){$this->db->group_by($data["group"]);}
        if (isset($data["having"]) && $data["having"]!=""){$this->db->having($data["having"]);}
        if (isset($data["limit"]) && $data["limit"]!=""){$this->db->limit($data["limit"], 0);}
        if (isset($data["join"])){if (count($data["join"])>0){foreach ($data["join"] as $rowJoin){$split = explode(",",$rowJoin);$this->db->join($split[0],$split[1],isset($split[2])?$split[2]:"");}}}
        $this->db->from($this->table);
        $query = $this->db->get();
        $dato = $query->result();
        return $dato;
    }

    public function total_records($data){
        if (isset($data["select"])&& $data["select"]!=""){$this->db->select($data["select"]);}
        if (isset($data["where"]) && $data["where"]!=""){$this->db->where($data["where"]);}
        if (isset($data["order"]) && $data["order"]!=""){$this->db->order_by($data["order"]);}
        if (isset($data["group"]) && $data["group"]!=""){$this->db->group_by($data["group"]);}
        if (isset($data["having"]) && $data["having"]!=""){$this->db->having($data["having"]);}
        if (isset($data["join"])){if (count($data["join"])>0){foreach ($data["join"] as $rowJoin){$split = explode(",",$rowJoin);$this->db->join($split[0],$split[1],isset($split[2])?$split[2]:"");}}}
        $this->db->from($this->table);
        $query = $this->db->get();
        $dato = $query->num_rows();
        return $dato;

    }

    public function search_data($data,$inicio,$num_reg){
        if (isset($data["select"])&& $data["select"]!=""){$this->db->select($data["select"]);}
        if (isset($data["where"]) && $data["where"]!=""){$this->db->where($data["where"]);}
        if (isset($data["order"]) && $data["order"]!=""){$this->db->order_by($data["order"]);}
        if (isset($data["group"]) && $data["group"]!=""){$this->db->group_by($data["group"]);}
        if (isset($data["having"]) && $data["having"]!=""){$this->db->having($data["having"]);}
        if (isset($data["join"])){if (count($data["join"])>0){foreach ($data["join"] as $rowJoin){$split = explode(",",$rowJoin);$this->db->join($split[0],$split[1],isset($split[2])?$split[2]:"");}}}
        $this->db->from($this->table);
        $query = $this->db->get("",$inicio,$num_reg);
        $dato = $query->result();
        return $dato;
    }

    public function search_data_rows($data,$inicio,$num_reg){
        if (isset($data["select"])&& $data["select"]!=""){$this->db->select($data["select"]);}
        if (isset($data["where"]) && $data["where"]!=""){$this->db->where($data["where"]);}
        if (isset($data["order"]) && $data["order"]!=""){$this->db->order_by($data["order"]);}
        if (isset($data["group"]) && $data["group"]!=""){$this->db->group_by($data["group"]);}
        if (isset($data["join"])){if (count($data["join"])>0){foreach ($data["join"] as $rowJoin){$split = explode(",",$rowJoin);$this->db->join($split[0],$split[1],isset($split[2])?$split[2]:"");}}}
        $this->db->from($this->table);
        $query = $this->db->get("",$inicio,$num_reg);
        $dato = $query->row();
        return $dato;
    }

    public function custom_query($data, $firstItem = false, $getSqlRaw = false)
    {
        try {

            if (isset($data['select'])) {
                $this->db->select($data['select']);
            }

            if (isset($data['where'])) {
                $this->db->where($data['where']);
            }

            if (isset($data['order'])) {
                if (is_array($data['order']) && isset($data['order']['column']) && isset($data['order']['direc'])) {
                    $this->db->order_by($data['order']['column'], $data['order']['direc']);
                } else {
                    $this->db->order_by($data['order']);
                }
            }

            if (isset($data['group'])) {
                $this->db->group_by($data['group']);
            }

            if (isset($data['having'])) {
                $this->db->having($data['having']);
            }

            if (isset($data['limit'])) {
                if (strpos($data['limit'], ',')) {
                    $limit = explode(',', $data['limit']);
                    $this->db->limit($limit[0], $limit[1]);
                } else {
                    $this->db->limit($data['limit'], 0);
                }
            }

            if (isset($data['join']) && count($data['join']) > 0) {
                foreach ($data['join'] as $rowJoin) {
                    $this->db->join($rowJoin[0], $rowJoin[1], isset($rowJoin[2]) ? $rowJoin[2] : '');
                }
            }

            $this->db->from($this->table);

            if ($getSqlRaw) {
                return $this->db->get_compiled_select();
            }

            $query = $this->db->get();

            return ($firstItem ?  $query->row(): $query->result());

        } catch (\Exception $ex) {
            throw new Exception(sprintf("Ocurrio un error en custom_query: %s", $ex->getMessage()));
        }
    }

    public function custom_query_replica($data, $db_b = NULL, $firstItem = false, $getSqlRaw = false) {
        try {
            if (isset($data['select'])) {
                $db_b->select($data['select']);
            }

            if (isset($data['where'])) {
                $db_b->where($data['where']);
            }

            if (isset($data['order'])) {
                if (is_array($data['order']) && isset($data['order']['column']) && isset($data['order']['direc'])) {
                    $db_b->order_by($data['order']['column'], $data['order']['direc']);
                } else {
                    $db_b->order_by($data['order']);
                }
            }

            if (isset($data['group'])) {
                $db_b->group_by($data['group']);
            }

            if (isset($data['having'])) {
                $db_b->having($data['having']);
            }

            if (isset($data['limit'])) {
                if (strpos($data['limit'], ',')) {
                    $limit = explode(',', $data['limit']);
                    $db_b->limit($limit[0], $limit[1]);
                } else {
                    $db_b->limit($data['limit'], 0);
                }
            }

            if (isset($data['join']) && count($data['join']) > 0) {
                foreach ($data['join'] as $rowJoin) {
                    $db_b->join($rowJoin[0], $rowJoin[1], isset($rowJoin[2]) ? $rowJoin[2] : '');
                }
            }

            $db_b->from($this->table);

            if ($getSqlRaw) {
                return $db_b->get_compiled_select();
            }

            $query = $db_b->get();

            return ($firstItem ?  $query->row(): $query->result());

        } catch (\Exception $ex) {
            throw new Exception(sprintf("Ocurrio un error en custom_query: %s", $ex->getMessage()));
        }
    }
    
    public function update_custom($set, $where)
    {
        return $this->db->update($this->table, $set, $where);
    }

}