<?php

defined('BASEPATH') or die('No direct script access allowed');

class Grades_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'grades';
        $this->table_id = 'grade_id';

        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }
        return parent::update($pk, $data);
    }
    public function delete_grades($colegiado_id){
        $params = array($colegiado_id);
        $sql = "delete from $this->table where colegiado_id = ?";
        $this->db->query($sql,$params);
    }
    public function get_grades_by_colegiado($colegiado_id){
        $params = array($colegiado_id);
        $sql = "select * from $this->table where colegiado_id=?";
        $query = $this->db->query($sql,$params);
        return $query->result();
    }
}
