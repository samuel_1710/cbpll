<?php

defined('BASEPATH') or die('No direct script access allowed');

class Boletin_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'boletins';
        $this->table_id = 'boletin_id';

        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }
        return parent::update($pk, $data);
    }
    public function get_all(){

        $sql = "select * from $this->table where status = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_boletines(){
        $year = date('Y');
        $sql = "select * from $this->table where status = 1 and type = 1 and year(created_at) = $year";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_resoluciones(){

        $sql = "select * from $this->table where status = 1 and type = 2";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_boletin($boletin_id){
        $params = array($boletin_id);
        $sql = "select * from $this->table where status = 1 and boletin_id=?";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
}
