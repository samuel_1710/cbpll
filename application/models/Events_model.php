<?php

defined('BASEPATH') or die('No direct script access allowed');

class Events_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'events';
        $this->table_id = 'event_id';

        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }
        return parent::update($pk, $data);
    }
    public function get_events(){

        $sql = "select * from $this->table where status = 1 order by created_at desc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_last_events(){

        $sql = "select * from $this->table where status = 1 order by created_at desc limit 4";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_events_visibility(){

        $sql = "select * from $this->table where status = 1 and visibility = 1 order by created_at desc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_event($id_event){
        $params = array($id_event);
        $sql = "select * from $this->table where status = 1 and event_id=?";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
    public function get_event_by_slug($slug){
        $params = array($slug);
        $sql = "select * from $this->table where slug= ? and status = 1";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
}
