<?php

defined('BASEPATH') or die('No direct script access allowed');

class Imageslider_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'image_slider';
        $this->table_id = 'image_id';

        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        /*if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }*/
        return parent::update($pk, $data);
    }

    public function get_images(){
        $sql = "select * from $this->table where status = 1 order by created_at desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_images_by_type($type){
        $sql = "select * from $this->table where status = 1 and type_image=$type order by created_at desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_images_home(){
        $sql = "select * from $this->table where status = 1 order by image_id asc";
        $query = $this->db->query($sql);
        return $query->result();
    }
}
