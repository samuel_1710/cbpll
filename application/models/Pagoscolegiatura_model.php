<?php

defined('BASEPATH') or die('No direct script access allowed');

class Pagoscolegiatura_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'pagoscolegiatura';
        $this->table_id = 'idPagosColegiatura';

        $this->FechaColegiado = '';
        $this->EstadoColegiado = '';
        $this->CodigoColegiado	 = '';
        $this->Usuarios_idUsuarios  = '';
        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }

        return parent::update($pk, $data);
    }
    public function get_pagos_by_id($idPayment){

        $params = array($idPayment);
        
        $sql = "select p.*,u.* from $this->table as p
            inner join colegiado as c ON p.Colegiado_idColegiado = c.idColegiado
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios
            where p.idPagosColegiatura = ? and c.EstadoColegiado <> 0 and p.status_value = 1";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
    public function get_pagos_by_colegiado($idColegiado,$limit = 1){

        $params = array($idColegiado);
        
        $sql = "select p.created_at as date_registro_pago,p.* from $this->table as p
            inner join colegiado as c ON p.Colegiado_idColegiado = c.idColegiado
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios
            where p.Colegiado_idColegiado = ? and c.EstadoColegiado <> 0 and p.status_value = 1 
            order by p.FechaPagada desc limit $limit ";
        $query = $this->db->query($sql,$params);
        return $query->result();

    }
}
