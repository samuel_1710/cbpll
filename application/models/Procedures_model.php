<?php

defined('BASEPATH') or die('No direct script access allowed');

class Procedures_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'procedures';
        $this->table_id = 'procedure_id';
        $this->created_at = '';
    }
    
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }
        return parent::update($pk, $data);
    }
    public function get_procedures(){

        $sql = "select * from $this->table where status = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_procedure_visibility(){

        $sql = "select * from $this->table where status = 1 and visibility = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_procedure($procedure_id){
        $params = array($procedure_id);
        $sql = "select * from $this->table where status = 1 and procedure_id=?";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }

    public function get_procedure_by_slug($slug){
        $params = array($slug);
        $sql = "select * from $this->table where slug= ? and status = 1";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
}
