<?php if ( ! defined("BASEPATH")) exit("No direct script access allowed");

class Ubigeo_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table_departments = 'ubigeo_peru_departments';
        $this->table_provinces = 'ubigeo_peru_provinces';
        $this->table_districts = 'ubigeo_peru_districts';

        $this->table = 'location';
        $this->table_id = 'ubigeo_id';

    }
    public function get_departments() {
        $sql = "SELECT * FROM $this->table_departments";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_provinces($id) {
        $params = array($id);
        $sql= "SELECT * FROM $this->table_provinces WHERE department_id = ?";
        $query = $this->db->query($sql,$params);
        return $query->result();
    }
    public function get_districts($id) {
        $params = array($id);
        $sql= "SELECT * FROM $this->table_districts WHERE province_id = ?";
        $query = $this->db->query($sql,$params);
        return $query->result();
    }

    public function search($data){
        if (isset($data["select"])&& $data["select"]!=""){$this->db->select($data["select"]);}
        if (isset($data["where"]) && $data["where"]!=""){$this->db->where($data["where"]);}
        if (isset($data["order"]) && $data["order"]!=""){$this->db->order_by($data["order"]);}
        if (isset($data["group"]) && $data["group"]!=""){$this->db->group_by($data["group"]);}
        /*if (isset($data["join"])){if (count($data["join"])>0){ foreach ($data["join"] as $rowJoin){$split = explode(",",$rowJoin);$this->db->join($split[0],$split[1]);}}}*/
        if (isset($data["join"])) {
            if (count($data["join"]) > 0) {
                foreach ($data["join"] as $rowJoin) {
                    $split = explode(",", $rowJoin);
                    $this->db->join($split[0], $split[1], isset($split[2]) ? $split[2] : "inner");
                }
            }
        }
        if (isset($data["limit"]) && $data["limit"]!=""){$this->db->limit($data["limit"]);}
        $this->db->from($this->table);
        $query = $this->db->get();
        $dato = $query->result();
        return $dato;
    }
    
    public function get_distritos_by_id($id) {
        $sql= "SELECT ll.ubigeo_id FROM location AS ll
            WHERE ubigeo_parent_id = $id";
        $query = $this->db->query($sql);
        return $query->result();
    }
    
    public function get_ubigeo_of_lima_by_name_distrito($name_distrito) {
        $sql = "select ldi.ubigeo_id as distrito_id, lpr.ubigeo_id as provincia_id, lde.ubigeo_id as departamento_id from location as ldi 
            inner join location as lpr on ldi.ubigeo_parent_id = lpr.ubigeo_id 
            inner join location as lde on lpr.ubigeo_parent_id = lde.ubigeo_id 
            where ldi.name = '$name_distrito' AND lde.ubigeo_id = 15";
        $query = $this->db->query($sql);
        return $query->row();
    }
    
    public function get_ubigeo_by_name_distrito($name_distrito) {
        $sql = "select ldi.ubigeo_id as distrito_id, lpr.ubigeo_id as provincia_id, lde.ubigeo_id as departamento_id from location as ldi 
            inner join location as lpr on ldi.ubigeo_parent_id = lpr.ubigeo_id 
            inner join location as lde on lpr.ubigeo_parent_id = lde.ubigeo_id 
            where ldi.name = '$name_distrito'";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function getDistritos() {
        $select = [
            'location.ubigeo_id as value',
            'location.ubigeo_id',
            'location.name as n1',
            '(select l2.name from location l2 where l2.ubigeo_id = location.ubigeo_parent_id) as n2',
            '(select l2.name from location l2 where l2.ubigeo_id in (select l3.ubigeo_parent_id from location l3 where l3.ubigeo_id = location.ubigeo_parent_id) ) as n3'
        ];
        if (!($ubigeos = $this->cache->memcached->get(__CLASS__.'_'.__FUNCTION__))) {

            $ubigeos = $this->obj_location->custom_query([
                'select' => implode(', ', $select),
                'having' => 'n2 IS NOT NULL AND n3 IS NOT NULL',
                'order' => 'value ASC'
                
            ]);
            foreach ($ubigeos as $key => $value) {
                $full_name = '';
                if($value->n2 && $value->n3 && $value->n1) {
                    $full_name = $value->n1 . ', ' . $value->n2 . ', ' . $value->n3;
                }
                if($full_name) {
                    $ubigeos[$key]->value = $full_name;
                }
                unset($ubigeos[$key]->n1);
                unset($ubigeos[$key]->n2);
                unset($ubigeos[$key]->n3);
            }
            $this->cache->memcached->save(__CLASS__.'_'.__FUNCTION__, $ubigeos, 900); //15 min
        }
        return $ubigeos;
    }
    
    public function get_location_by_id($id) {
        $params = array($id);
        $sql = "SELECT * FROM $this->table WHERE ubigeo_id=?";
        $query = $this->db->query($sql, $params);
        return $query->row_array();
    }
    
    public function get_ubigeo_by_distrito_id($district_id) {
        $params = array($district_id);
        $sql = "SELECT dist.ubigeo_id AS distrito_id, prov.ubigeo_id AS provincia_id,  
            depa.ubigeo_id AS departamento_id FROM location AS dist 
            INNER JOIN location AS prov ON dist.ubigeo_parent_id = prov.ubigeo_id 
            INNER JOIN location AS depa ON prov.ubigeo_parent_id = depa.ubigeo_id 
            WHERE dist.ubigeo_id = ?";
        $query = $this->db->query($sql, $params);
        return $query->row();
    }
}
