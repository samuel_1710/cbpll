<?php

defined('BASEPATH') or die('No direct script access allowed');

class Directives_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'directives';
        $this->table_id = 'directive_id';

        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }
        return parent::update($pk, $data);
    }
    public function get_directives(){

        $sql = "select * from $this->table where status = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_directive_visibility(){

        $sql = "select * from $this->table where status = 1 and visibility = 1 order by orden_position asc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_directive($comision_id){
        $params = array($comision_id);
        $sql = "select * from $this->table where status = 1 and directive_id=?";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
}
