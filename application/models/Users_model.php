<?php

defined('BASEPATH') or die('No direct script access allowed');

class Users_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'usuarios';
        $this->table_id = 'idUsuarios';

        $this->idUsuarios = '';
        $this->TipoDocumento = '';
        $this->NumeroDocumento = '';
        $this->ApellidosPaterno  = '';
        $this->ApellidoMaterno = '';
        $this->NombresUsuario = '';
        $this->FechaNacimiento = '';
        $this->GeneroUsuario = '';
        $this->EstadoCivil = '';
        $this->DireccionUsuario = '';
        $this->UrbanizacionUsuario   = '';
        $this->DepartamentoUsuario = '';
        $this->ProvinciaUsuario = '';
        $this->DistritoUsuario = '';
        $this->TelefonoUsuario = '';
        $this->CelularUsuario    = '';
        $this->NacionalidadUsuario = '';
        $this->TipoSangre = '';
        $this->EmailUsuario  = '';
        $this->created_at = '';
    }
    public function insert($data)
    {
    	if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }

        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        //if (!isset($data['update_at'])) {
            //$data['update_at'] = date('Y-m-d H:i:s');
        //}

        return parent::update($pk, $data);
    }
    public function verify_user($user,$password){
        $params = array(
            $user,
            md5($password)
        );
        $sql = "select * from $this->table as u
            inner join login as l ON u.idUsuarios = l.Usuarios_idUsuarios where 
            u.NumeroDocumento=? and l.PasswordLogin=? and u.status_value = 1";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
}
