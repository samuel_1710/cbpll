<?php

defined('BASEPATH') or die('No direct script access allowed');

class Comunication_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'comunication';
        $this->table_id = 'comunication_id';

        $this->created_at = '';
    }
    public function insert($data)
    {
        if (!isset($data['created_at'])) {
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        /*if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }*/
        return parent::update($pk, $data);
    }

    public function get_images(){
        $sql = "select * from $this->table where status = 1 order by created_at desc";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function get_images_home(){
        $sql = "select * from $this->table where status = 1 order by comunication_id desc";
        $query = $this->db->query($sql);
        return $query->result();
    }
}
