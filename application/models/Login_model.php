<?php

defined('BASEPATH') or die('No direct script access allowed');

class Login_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'login';
        $this->table_id = 'Usuarios_idUsuarios';
    }
    public function insert($data)
    {
    	if (!isset($data['data_create'])) {
            $data['data_create'] = date('Y-m-d H:i:s');
        }

        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        if (!isset($data['data_update'])) {
            $data['data_update'] = date('Y-m-d H:i:s');
        }

        return parent::update($pk, $data);
    }
}
