<?php

defined('BASEPATH') or die('No direct script access allowed');

class Institucional_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'institucional';
        $this->table_id = 'institucional_id';

        $this->created_at = '';
    }
        
    public function update($pk, $data)
    {
        /*if (!isset($data['update_at'])) {
            $data['update_at'] = date('Y-m-d H:i:s');
        }*/
        return parent::update($pk, $data);
    }

    public function get_data(){
        $sql = "select * from $this->table";
        $query = $this->db->query($sql);
        return $query->row();
    }
}
