<?php

defined('BASEPATH') or die('No direct script access allowed');

class Colegiado_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'colegiado';
        $this->table_id = 'idColegiado';

        $this->FechaColegiado = '';
        $this->EstadoColegiado = '';
        $this->CodigoColegiado	 = '';
        $this->Usuarios_idUsuarios  = '';
        $this->created_at = '';
    }
    public function insert($data)
    {
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        //if (!isset($data['update_at'])) {
            //$data['update_at'] = date('Y-m-d H:i:s');
        //}

        return parent::update($pk, $data);
    }
    public function get_colegiado_by_idUsuario($idUser){

        $params = array($idUser);
        $sql = "select * from $this->table as c
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where 
            u.idUsuarios=? and u.status_value = 1";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
    public function get_colegiado_by_id($idColegiado){

        $params = array($idColegiado);
        $sql = "select *,
            (select  FechaVencimiento from pagoscolegiatura as p where p.Colegiado_idColegiado=c.idColegiado and p.status_value = 1 order by FechaPagada desc limit 1 ) as FechaUltimoPago
            from $this->table as c
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where 
            c.idColegiado=? and u.status_value = 1";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }
    public function filter($type,$description)
    {
        $params = array($description);
        $field = "concat(TRIM(u.ApellidosPaterno),' ',TRIM(u.ApellidoMaterno),' ',TRIM(u.NombresUsuario)) as all_name";
        $having = "";$where = "";
        if($type == "name"){
            $where = "true";
            $having = "having all_name like '%$description%'";
        }
        else if($type == "dni"){
            $where = "u.TipoDocumento = 'dni' and u.NumeroDocumento = ?";
        }
        else if($type == "cbpll"){
            $where = "c.CodigoColegiado = ?";
        }
        $sql = "select $field,c.*,u.*,
        (select  FechaVencimiento from pagoscolegiatura as p where p.Colegiado_idColegiado=c.idColegiado order by FechaPagada desc limit 1 ) as FechaUltimoPago from $this->table as c
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where
            $where and c.EstadoColegiado <> 0 and u.status_value = 1 $having limit 10 ";
        $query = $this->db->query($sql,$params);
        return $query->result();
    }
    public function list_register_virtual()
    {

        $sql = "select * from $this->table as c
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where 
            c.EstadoColegiado = 0 and u.status_value = 1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_colegiados($text)
    {

        $field = "concat(TRIM(u.ApellidosPaterno),' ',TRIM(u.ApellidoMaterno),' ',TRIM(u.NombresUsuario)) as all_name";
        $sql = "select u.*,c.*,$field from $this->table as c
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where 
            c.EstadoColegiado <> 0 and c.CodigoColegiado !='' and u.status_value = 1 having all_name like '%$text%'";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_all_colegiados()
    {
        //$sql = "select u.*,c.*,dep.name as departamento,prov.name as provincia,district.name as distrito"; 
        $sql = "select temp.*,dep.name as departamento,prov.name as provincia,district.name as distrito,
            (select FechaVencimiento from pagoscolegiatura as p 
            where p.Colegiado_idColegiado=temp.idColegiado order by FechaPagada desc limit 1 ) as FechaUltimoPago
            from 
            (select u.*,c.* from $this->table as c 
               inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios 
               where c.EstadoColegiado <> 0 and c.CodigoColegiado !='' and u.status_value = 1) as temp 
            left join ubigeo_peru_departments as dep on dep.id=temp.DepartamentoUsuario 
            left join ubigeo_peru_provinces as prov on prov.id=temp.ProvinciaUsuario
            left join ubigeo_peru_districts as district on district.id=temp.DistritoUsuario order by temp.ApellidosPaterno asc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_all_colegiado_ultimo_payment($date){
        $params=array($date);
        $sql = "select c.*,u.*, 
        (select FechaVencimiento from pagoscolegiatura as p 
        where p.Colegiado_idColegiado=c.idColegiado order by FechaPagada desc limit 1 ) as FechaUltimoPago from colegiado as c 
        inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where c.EstadoColegiado <> 0 and u.status_value = 1
        HAVING FechaUltimoPago >= '$date' order by u.ApellidosPaterno asc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_habilitados($date){
        $date_temp = strtotime($date);
        $date_init = date("Y",$date_temp)."-".date("m",$date_temp).'-01';
        $sql = "select *, DATE_ADD(FechaUltimoPago,INTERVAL 3 MONTH) as date_habilitado,
        CASE 
            WHEN temp.type_member = 'Vitalicio'  THEN 'Habilitado(Vitalicio)'
            WHEN DATE_ADD(FechaUltimoPago,INTERVAL 3 MONTH) >= '$date'  THEN 'Habilitado'
            ELSE 'Inhabilitado'
        END AS status
        from 
        (select c.*,u.*, 
            (select FechaVencimiento from pagoscolegiatura as p 
            where p.Colegiado_idColegiado=c.idColegiado order by FechaPagada desc limit 1 ) as FechaUltimoPago
        from colegiado as c 
        inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where c.EstadoColegiado <> 0 and u.status_value = 1
        ) as temp  order by temp.ApellidosPaterno asc";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_colegiado_by_document($typeDoc,$nroDoc){
        $params = array($typeDoc,$nroDoc);
        $where = "u.TipoDocumento = ? and u.NumeroDocumento = ?";
        $sql = "select *,
            (select  FechaVencimiento from pagoscolegiatura as p where p.Colegiado_idColegiado=c.idColegiado and p.status_value = 1 order by FechaPagada desc limit 1 ) as FechaUltimoPago
            from $this->table as c
            inner join usuarios as u ON c.Usuarios_idUsuarios = u.idUsuarios where u.status_value = 1 and ".$where;
        $query = $this->db->query($sql,$params);
        return $query->result();
    }

}
