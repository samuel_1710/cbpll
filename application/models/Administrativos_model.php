<?php

defined('BASEPATH') or die('No direct script access allowed');

class Administrativos_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'administrativos';
        $this->table_id = 'idAdministrativos';

    }
    public function insert($data)
    {
        return parent::insert($data);
    }
    
    public function update($pk, $data)
    {
        //if (!isset($data['update_at'])) {
            //$data['update_at'] = date('Y-m-d H:i:s');
        //}

        return parent::update($pk, $data);
    }
    public function get_administrativos_by_user_id($user_id){
        $params = array($user_id);
        $sql = "select * from $this->table where Usuarios_idUsuarios = ?";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }

    public function get_administrativos_user(){

        $field = "concat(TRIM(u.ApellidosPaterno),' ',TRIM(u.ApellidoMaterno),' ',TRIM(u.NombresUsuario)) as all_name";
        $sql = "select u.*,admin.*, $field  from $this->table as admin
        inner join usuarios as u ON admin.Usuarios_idUsuarios = u.idUsuarios 
        where CargoAdministrativos = 'Adminweb' and role is not null and u.status_value=1";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_administrativo_by_id($idAdministrativos){

        $params = array($idAdministrativos);
        $sql = "select * 
            from $this->table as admin
            inner join usuarios as u ON admin.Usuarios_idUsuarios = u.idUsuarios
            where admin.idAdministrativos=?";
        $query = $this->db->query($sql,$params);
        return $query->row();
    }

}
