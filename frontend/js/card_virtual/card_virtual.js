import validate from 'jquery-validation'
import language from './../vendor/jsvalidate_es'
import datepicker from 'bootstrap-datepicker'
import toastr from "toastr";

(function($){
	$.fn.datepicker.dates['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy",
		monthsTitle: "Meses",
		clear: "Borrar",
		weekStart: 1,
		format: "yyyy-mm-dd"
	};
}(jQuery));
$('#date_nac').datepicker({
    language: "es",
    todayHighlight: true,
    toggleActive: true,
    autoclose:true,
});

$.validator.addMethod("emailCustom", function(value, element) {
    return this.optional(element) || /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i.test(value);
  }, "Ingrese un correo valido, porfavor");

const $formPreRegistro = $('#formPreRegistro');
$formPreRegistro.validate({
    ignore: ".ignore,:hidden",
    rules: {
        ape_pat: {
            required: true,
            minlength: 2
        },
        ape_mat: {
            required: true,
            minlength: 2
        },
        name: {
            required: true,
            minlength: 2
        },
        date_nac: {
            required: true,
            minlength: 2
        },
        type_doc:{
            required: true,
        },
        nro_doc:{
            required: true,
        },
        nationality:{
            required: true,
        },
        group_sangre:{
            required: true,
        },
        address:{
            required: true,
        },
        dpto:{
            required: true,
        },
        prov:{
            required: true,
        },
        district:{
            required: true,
        },
        phone: {
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        celphone: {
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        email: {
            required: true,
            email: true,
            emailCustom: true
        },
        linkweb:{
            url:true,
        },
        speciality:{
            required:true
        },
        speciality_other:{
            required: $("#speciality").val() === 'Otros'?true:false
        }
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "home/card/insert_colegiado",
            dataType: 'json',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                console.log(response);
                if(response.status=="SUCCESS"){
                    $('#formPreRegistro')[0].reset();
                    toastr.success(response.message)
                }else{
                    toastr.error(response.message)
                }
                
            }
        });
    }
})
$("#dpto").on("change",function(){
    let department_id = $(this).val();
    if (department_id != ""){
        $.ajax({
            type: "post",
            url: site+"home/card/get_provincias/",
            dataType: "json",
            data: {departamento :department_id},
            success:function(response){
                if(response.status=="SUCCESS"){
                    $('#prov').html('<option value="">Seleccione Provincia</option>');
                    let template = '';
                    let data = response.data;
                    for (let index = 0; index < data.length; index++) {
                        let element = data[index];
                        template +=`<option value="${element.id}">${element.name}</option>`;
                    }
                    $("#prov").append(template);
                }
            }
        });
    } else{
        $('#prov').html('<option value="">Seleccione Provincia</option>');
        $('#district').html('<option value="">Seleccione Distrito</option>');
    }
})
$("#prov").on("change",function(){
    let province_id = $(this).val();
    if (province_id != ""){
        $.ajax({
            type: "post",
            url: site+"home/card/get_distritos/",
            dataType: "json",
            data: {provincia :province_id},
            success:function(response){
                if(response.status=="SUCCESS"){
                    $('#district').html('<option value="">Seleccione Distrito</option>');
                    let template = '';
                    let data = response.data;
                    for (let index = 0; index < data.length; index++) {
                        let element = data[index];
                        template +=`<option value="${element.id}">${element.name}</option>`;
                    }
                    $("#district").append(template);
                }
            }
        });
    } else{
        $('#district').html('<option value="">Seleccione Distrito</option>');
    }
})
$("#speciality").on("change",function(){
    let speciality = $(this).val();
    if (speciality === "Otros"){
        $(".insert_speciality").removeClass("d-none");  
    }
    else{
        $(".insert_speciality").addClass("d-none");  
    }
})
