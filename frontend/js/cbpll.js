import collapse from 'bootstrap/js/dist/collapse';
import dropdown from 'bootstrap/js/dist/dropdown';
import modal from 'bootstrap/js/dist/modal';
//import bootstrap from 'bootstrap';
import style from './../scss/main/style.scss';

function toggleDropdown (e) {
    const _d = $(e.target).closest('.dropdown'),
      _m = $('.dropdown-menu', _d);
    setTimeout(function(){
      const shouldOpen = e.type !== 'click' && _d.is(':hover');
      _m.toggleClass('show', shouldOpen);
      _d.toggleClass('show', shouldOpen);
      $('[data-toggle="dropdown"]', _d).attr('aria-expanded', shouldOpen);
    }, e.type === 'mouseleave' ? 50 : 0);
}
  
$('body')
    .on('mouseenter mouseleave','.dropdown',toggleDropdown)
    .on('click', '.dropdown-menu a', toggleDropdown);