import modal from 'bootstrap/js/dist/modal';
//import bootstrap  from 'bootstrap'; 

$(".container-card-comision").on("click",function(){
    let name = $(this).find('.card-title').text()
    let president = $(this).find('.presidente-card').text()
    let members = $(this).find('.members').val()
    $(".modal-title").html(name)
    $(".presidente-modal").html(president)
    $(".list-integrantes-comision").html(members)
    $("#modal_comision").modal("show");
})
