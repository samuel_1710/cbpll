import owlCarousel from  'owl.carousel';
import modal from 'bootstrap/js/dist/modal';
//import bootstrap  from 'bootstrap'; 
let owlmodal = $('.owl-carousel-modal');
owlmodal.owlCarousel({
    autoplay: true,
    autoplayTimeout:7000,
    items: 1,
    loop: true,
    autoHeight:true,
    margin:3,
    lazyLoad: true,
    dots:true,
    responsive: {
        0: {
    	items: 1,
        },
        400: {
            items: 1
        },
        768: {
            items: 1
        }
    }
})
let owl = $('.owl-carousel-home');
owl.owlCarousel({
    autoplay: true,
    autoplayTimeout:5000,
    items: 1,
    margin:3,
    lazyLoad: true,
    dots:true,
    responsive: {
        0: {
    	items: 1,
        },
        400: {
            items: 1
        },
        768: {
            items: 1
        }
    }
})
const owlconvenios = $('.owl-carousel-convenios');
owlconvenios.owlCarousel({
    autoplay: true,
    autoplayTimeout:5000,
    items: 6,
    lazyLoad: true,
    dots:false,
    loop: true,
    lazyLoad: true,
    nav:true,
    margin:5,
    navClass:['owlNavCustom owlNavCustom__prev h-100','owlNavCustom owlNavCustom__next h-100'],
    navText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsive: {
        0: {
    	items: 2,
        },
        400: {
            items: 3,
            nav:false,
            navText:['','']
        },
        768: {
            items: 6
        }
    }
})
$("#modal_main_comunicado").modal("show");