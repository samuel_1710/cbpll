import validate from 'jquery-validation'
import language from './../vendor/jsvalidate_es'

$.validator.addMethod("emailCustom", function(value, element) {
    return this.optional(element) || /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i.test(value);
}, "Ingrese un correo valido, porfavor");

const $formSearchColegiado = $('#formSearchColegiado');
$formSearchColegiado.validate({
    ignore: ".ignore,:hidden",
    rules: {
        type_filter: {
            required: true,
        },
        description_filter: {
            required: true,
        }
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "home/search/filter",
            dataType: 'text',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                $(".title_resultados").removeClass("hide");
                $("#content_table").html(response);
            }
        });
    }
})
$("#type_filter").on("change",function(){
    let type_search = $("#type_filter").val();
    let input_search = $("#description_filter");
    if(type_search == "dni"){
        input_search.attr("placeholder","Ingrese DNI")
    }else if(type_search == "name"){
        input_search.attr("placeholder","Ingrese Apellidos y Nombres")
    }else if( type_search == "cbpll"){
        input_search.attr("placeholder","Ingrese código CBP")
    }
})
