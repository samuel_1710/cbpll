var path = require('path')
var webpack = require('webpack')
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const miniCSS = new MiniCssExtractPlugin({
	filename: "css/[name].min.css",
    chunkFilename: "/css/[id].min.css"
});

const devMode = process.env.NODE_ENV == 'production'

module.exports = {
   	entry: {
        cbpll:'./js/cbpll.js',
        cb_home:'./js/home/home.js',
        cb_comision:'./js/institucional/comisions.js',
        cb_search:'./js/search/search.js',
        cb_table_search:'./js/search/table_search.js',
        cb_card:'./js/card_virtual/card_virtual.js',
        
	},
   	output: {
        path:  path.resolve("../static/website/dist"),
        filename: 'js/[name].min.js',
   	},
   	module:{
   		rules: [
            { test: /\.css$/, 
            use: [
                MiniCssExtractPlugin.loader,
                {
                    loader: "css-loader", options: {
                        minimize:devMode?true:false,
                        url:false,
                        sourceMap: devMode?true:false,
                    }
                } 
            ]
            },
            { test: /\.scss$/, 
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader", options: {
                            url:false,
                            sourceMap: devMode?true:false,
                        }
                }, 
                {
                        loader: "sass-loader", options: {
                            url:false,
                            sourceMap: devMode?true:false,
                        }
                }
                ]
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                presets: ['env'],
                },
                exclude: /node_modules/
            },
	    ]
   	},
   	plugins: [
	    new webpack.ProvidePlugin({
	        $: 'jquery',
	        jQuery: 'jquery',
	        'window.$': 'jquery',
	        'window.jQuery': 'jquery',
	    }),
	    miniCSS
	],
}