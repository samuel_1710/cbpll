$("#dpto").on("change",function(){
    let department_id = $(this).val();
    if (department_id != ""){
        $.ajax({
            type: "post",
            url: site+"home/card/get_provincias/",
            dataType: "json",
            data: {departamento :department_id},
            success:function(response){
                if(response.status=="SUCCESS"){
                    $('#prov').html('<option value="">Seleccione Provincia</option>');
                    let template = '';
                    let data = response.data;
                    for (let index = 0; index < data.length; index++) {
                        let element = data[index];
                        template +=`<option value="${element.id}">${element.name}</option>`;
                    }
                    $("#prov").append(template);
                }
            }
        });
    } else{
        $('#prov').html('<option value="">Seleccione Provincia</option>');
        $('#district').html('<option value="">Seleccione Distrito</option>');
    }
})
$("#prov").on("change",function(){
    let province_id = $(this).val();
    if (province_id != ""){
        $.ajax({
            type: "post",
            url: site+"home/card/get_distritos/",
            dataType: "json",
            data: {provincia :province_id},
            success:function(response){
                if(response.status=="SUCCESS"){
                    $('#district').html('<option value="">Seleccione Distrito</option>');
                    let template = '';
                    let data = response.data;
                    for (let index = 0; index < data.length; index++) {
                        let element = data[index];
                        template +=`<option value="${element.id}">${element.name}</option>`;
                    }
                    $("#district").append(template);
                }
            }
        });
    } else{
        $('#district').html('<option value="">Seleccione Distrito</option>');
    }
})