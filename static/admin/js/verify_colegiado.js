$.validator.addMethod("emailCustom", function(value, element) {
    return this.optional(element) || /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i.test(value);
  }, "Ingrese un correo valido, porfavor");
$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
    daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
    daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    monthsTitle: "Meses",
    clear: "Borrar",
    weekStart: 1,
    format: "yyyy-mm-dd"
};
$(document).ready(function() {  
    $('#date_nac').datepicker({
        language: "es",
        todayHighlight: true,
        toggleActive: true,
        autoclose:true,
    });
    $("#speciality").on("change",function(){
        let speciality = $(this).val();
        if (speciality === "Otros"){
            $(".insert_speciality").removeClass("d-none");  
        }
        else{
            $(".insert_speciality").addClass("d-none");  
        }
    })
var $formPreRegistro = $('#formPreRegistro');
$formPreRegistro.validate({
    ignore: ".ignore,:hidden",
    rules: {
        ape_pat: {
            required: true,
            minlength: 2
        },
        ape_mat: {
            required: true,
            minlength: 2
        },
        name: {
            required: true,
            minlength: 2
        },
        date_nac: {
            required: true,
            minlength: 2
        },
        type_doc:{
            required: true,
        },
        nro_doc:{
            required: true,
        },
        nationality:{
            required: true,
        },
        group_sangre:{
            required: true,
        },
        address:{
            required: true,
        },
        dpto:{
            required: true,
        },
        prov:{
            required: true,
        },
        district:{
            required: true,
        },
        phone: {
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        celphone: {
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        email: {
            required: true,
            email: true,
            emailCustom: true
        },
        linkweb:{
            url:true,
        },
        speciality:{
            required:true
        },
        speciality_other:{
            required: $("#speciality").val() === 'Otros'?true:false
        }
    },
    highlight: function (element, errorClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "admin/colegiado/ajax_verificar_colegiado",
            dataType: 'json',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                console.log(response);
                if(response.status=="SUCCESS"){
                    $('#formPreRegistro')[0].reset();
                    toastr.success(response.message)
                    window.location.href = site+'admin/listado-virtual';
                }else{
                    toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                }
            }
        });
    }
})
})