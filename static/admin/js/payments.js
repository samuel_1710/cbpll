$(function () {
    $.fn.datepicker.dates['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy",
		monthsTitle: "Meses",
		clear: "Borrar",
		weekStart: 1,
		format: "yyyy-mm-dd"
    };
    $('#date_pago').datepicker({
        language: "es",
        todayHighlight: true,
        toggleActive: true,
        autoclose:true,
    });
    $('#date_vencimiento').datepicker({
        language: "es",
        todayHighlight: true,
        toggleActive: true,
        autoclose:true,
    });

    $('#select_colegiado').select2({
        theme: 'bootstrap4',
        placeholder: "Ingrese un colegiado",
        language: 'es',
        minimumInputLength: 2,
        ajax: {
            type:'POST',
            placeholder: "Buscar colegiado",
            url: site+"admin/colegiado/ajax_get_colegiados",
            dataType: 'json',
            data: function (term, page) {
                return {'term': term.term }
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data.data, function (item) {
                        return {
                            text: item.all_name,
                            id: item.idColegiado
                        }
                    })
                };
            }
        },
    })
    bsCustomFileInput.init();
    const $formCuotaColegiado = $('#formCuotaColegiado');
    $formCuotaColegiado.validate({
        ignore: ".ignore,:hidden",
        rules: {
            select_colegiado:{
                required: true,
            },
            date_pago: {
                required: true,
            },
            nro_operacion: {
                required: true,
            },
            monto_pago:{
                required: true,
            },
            date_vencimiento:{
                required: true,
            },
            /*detalle_pago:{
                required:true,
            },
            voucher_file:{
                required:true,
            }*/
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formCuotaColegiado')[0])
            $.ajax({
                url: site + "admin/colegiado/ajax_insert_cuota",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        $('#formCuotaColegiado')[0].reset();
                        $('#select_colegiado').val(null).trigger('change');
                        toastr.success(response.message)
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

    const $formEditCuota = $('#formEditCuota');
    $formEditCuota.validate({
        ignore: ".ignore,:hidden",
        rules: {
            date_pago: {
                required: true,
            },
            nro_operacion: {
                required: true,
            },
            monto_pago:{
                required: true,
            },
            date_vencimiento:{
                required: true,
            },
            /*detalle_pago:{
                required:true,
            },
            voucher_file:{
                required:true,
            }*/
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formEditCuota')[0])
            $.ajax({
                url: site + "admin/colegiado/ajax_edit_cuota",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message);
                        setTimeout(function(){
                            location.reload();
                        },1000)
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});