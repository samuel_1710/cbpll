const language = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
}
$(document).ready(function() { 

    $('#tableRegisterVirtual').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        language:language
    })
    $(".delete_register_virtual").on("click",function(){
        let user_id = $(this).data("user");
        let name = $(this).parent().parent().find(".nameUser").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar el registro virtual de "+name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/colegiado/delete_register_virtual",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        user_id:user_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar el registro. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar el registro. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})