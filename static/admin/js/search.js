$.validator.addMethod("emailCustom", function(value, element) {
    return this.optional(element) || /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i.test(value);
}, "Ingrese un correo valido, porfavor");

const $formSearchColegiado = $('#formSearchColegiado');
$formSearchColegiado.validate({
    ignore: ".ignore,:hidden",
    rules: {
        type_filter: {
            required: true,
        },
        description_filter: {
            required: true,
        }
    },
    highlight: function (element, errorClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "admin/colegiado/filter",
            dataType: 'text',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                $(".title_resultados").removeClass("d-none");
                $("#content_table").html(response);
            }
        });
    }
})
$("#type_filter").on("change",function(){
    let type_search = $("#type_filter").val();
    let input_search = $("#description_filter");
    if(type_search == "dni"){
        input_search.attr("placeholder","Ingrese DNI")
    }else if(type_search == "name"){
        input_search.attr("placeholder","Ingrese Apellidos y Nombres")
    }else if( type_search == "cbpll"){
        input_search.attr("placeholder","Ingrese código CBP")
    }
})
$("body").on("click",".delete_colegiado",function(){
    let user_id = $(this).data("user");
    let name = $(this).parent().parent().find(".nameUser").text()
    Swal.fire({
        title: 'Mensaje de Validación',
        text: "¿Estás seguro de eliminar al colegiado "+name+" ?",
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true,
        confirmButtonColor: '#28a745',
        cancelButtonColor: '#dc3545',
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: site + "admin/colegiado/delete_register_virtual",
                dataType: 'JSON',
                type: 'post',
                data: {
                    user_id:user_id
                },
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        Swal.fire(
                            'Eliminado!',
                            response.message,
                            'success'
                        )
                        setTimeout(function(){
                            window.location.reload()
                        },1000)
                    }else{
                        toastr.error('Ha ocurrido un inconveniente al eliminar el registro. Intentélo mas tarde.')
                    }
                },
                error:function(error){
                    toastr.error('Ha ocurrido un inconveniente al eliminar el registro. Intentélo mas tarde.')
                }
            });               
        }
    })
})