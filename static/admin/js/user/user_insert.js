

$(function () {
    const $formUser = $('#formUser');
    $formUser.validate({
        ignore: ".ignore,:hidden",
        rules: {
            ape_pat:{
                required: true,
            },
            ape_mat: {
                required: true,
            },
            name: {
                required: true,
            },
            username: {
                required: true,
            },
            password_user: {
                //required: true,
                minlength:6,
            },
            repeat_password_user:{
                equalTo: "#password_user"
            }
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formUser')[0])
            $.ajax({
                url: site + "admin/user/ajax_insert_user",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#user_id").val()===""){
                            setTimeout(function(){
                                window.location.href=site+"admin/user"
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});