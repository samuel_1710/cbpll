$(document).on("ready",function() {
    console.log("askjdkalshdkasjdlk") 
    
    $(".set_vitalicio").on("click",function(){
        let colegiado = $("#colegiado").val();
        let colegiado_id = $(this).attr("data-colegiado")
        console.log(colegiado_id);
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro que desea establecer como vitalicio al biologo "+colegiado+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/colegiado/ajax_set_vitalicio",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        colegiado_id:colegiado_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Éxito!',
                                response.message,
                                'success'
                            )
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al actualizar el registro. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al actualizar el registro. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})
$("#type_member").on("change",function(){
    console.log("aksdjaskldjaskljdksa")
    let colegiado_id = $("#colegiado_id").val();
    let type_member = $(this).val();
    console.log("click",colegiado_id,type_member)
    if (type_member !== ""){
        $.ajax({
            url: site + "admin/colegiado/ajax_set_type_member",
            dataType: 'JSON',
            type: 'post',
            data: {
                colegiado_id:colegiado_id,
                type_member:type_member
            },
            beforeSend:function(){
               $(".preload").html("Espere un momento porfavor.")
            },
            success: function(response) {
                $(".preload").html("")
                if(response.status=="SUCCESS"){
                    Swal.fire(
                        'Éxito!',
                        response.message,
                        'success'
                    )
                }else{
                    toastr.error('Ha ocurrido un inconveniente al actualizar el registro. Intentélo mas tarde.')
                }
            },
            error:function(error){
                toastr.error('Ha ocurrido un inconveniente al actualizar el registro. Intentélo mas tarde.')
            }
        });  
    }
    else{
        toastr.error('Debe seleccionar un opción.')  
    }
})