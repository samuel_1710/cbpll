$(document).ready(function() { 
    $(".delete_payment").on("click",function(){
        let payment_id = $(this).data("pago");
        let date_payment = $(this).parent().parent().find(".date_payment").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar la cuota del "+date_payment+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/colegiado/ajax_delete_cuota",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        payment_id:payment_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                //window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar la cuota. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar la cuota. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})