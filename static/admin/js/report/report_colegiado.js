
$(function () {
    $("#checkAll").change(function () {
        $("input[name='fields[]']").prop('checked', $(this).prop("checked"));
    });
    $.fn.datepicker.dates['es'] = {
		days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
		daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb"],
		daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
		months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		today: "Hoy",
		monthsTitle: "Meses",
		clear: "Borrar",
		weekStart: 1,
		format: "yyyy-mm-dd"
    };
    $('#payment_to').datepicker({
        language: "es",
        todayHighlight: true,
        toggleActive: true,
        autoclose:true,
    });
    $('#habilitados_to').datepicker({
        language: "es",
        todayHighlight: true,
        toggleActive: true,
        autoclose:true,
    });
});
$("#export_colegiado").on("click",function(e){
    e.preventDefault();
    if($("input[name='fields[]']").is(":checked")){
        $("#formReportColegiado").submit();
    }
    else{
        toastr.error("Debe seleccionar más campos para generar reporte")
    }
})
$("#export_ultimate_pago").on("click",function(e){
    e.preventDefault();
    if($("payment_to").val()!==""){
        $("#formReportColegiado").submit();
    }
    else{
        toastr.error("Debe seleccionar una fecha")
    }
})
$("#export_habilitados").on("click",function(e){
    e.preventDefault();
    if($("#habilitados_to").val()!==""){
        $("#formReportColegiado").submit();
    }
    else{
        toastr.error("Debe seleccionar una fecha")
    }
})