var $formChangePassword = $('#formChangePassword');
$formChangePassword.validate({
    ignore: ".ignore,:hidden",
    rules: {
        new_password:{
            required: true,
            minlength:6,
        },
        repeat_new_password:{
            equalTo: "#new_password"
        }
    },
    highlight: function (element, errorClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "admin/perfil/ajax_change_password",
            dataType: 'json',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                if(response.status=="SUCCESS"){
                    $(form)[0].reset();
                    toastr.success(response.message)
                }else{
                    toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                }
            }
        });
    }
})