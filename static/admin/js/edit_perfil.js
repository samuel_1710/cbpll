$.validator.addMethod("emailCustom", function(value, element) {
    return this.optional(element) || /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i.test(value);
  }, "Ingrese un correo valido, porfavor");

$(function () { 
    $(".btnAddStudy").on("click",function () {
        let count = $(this).attr("data-count")
        let html = '<div class="row mb-2">'+
            '<div class="col-md-4">'+
                '<input type="text" name="grade[]" class="form-control required nameGrade" placeholder="Ingrese grado o especialidad">'+
            '</div>'+
            '<div class="col-md-6">'+
                '<input type="text" name="namegrade[]" class="form-control required textGrade" placeholder="Ingrese nombre del grado">'+
            '</div>'+
            '<div class="col-md-2">'+
                '<button type="button" class="btn btn-danger btnDeleteGrade"><i class="fas fa-trash"></i></button>'+
            '</div>'
        '</div>';
        $(".contentStudys").append(html);
    })
    $("body").on("click",".btnDeleteGrade",function(){
        $(this).parent().parent().remove();
    })
    $("#speciality").on("change",function(){
        let speciality = $(this).val();
        if (speciality === "Otros"){
            $(".insert_speciality").removeClass("d-none");  
        }
        else{
            $(".insert_speciality").addClass("d-none");  
        }
    })
})
var $formMiPerfil = $('#formMiPerfil');
$formMiPerfil.validate({
    ignore: ".ignore,:hidden",
    rules: {
        ape_pat: {
            required: true,
            minlength: 2
        },
        ape_mat: {
            required: true,
            minlength: 2
        },
        name: {
            required: true,
            minlength: 2
        },
        date_nac: {
            required: true,
            minlength: 2
        },
        type_doc:{
            required: true,
        },
        nro_doc:{
            required: true,
        },
        nationality:{
            required: true,
        },
        group_sangre:{
            required: true,
        },
        address:{
            required: true,
        },
        dpto:{
            required: true,
        },
        prov:{
            required: true,
        },
        district:{
            required: true,
        },
        phone: {
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        celphone: {
            digits: true,
            minlength: 7,
            maxlength: 9
        },
        email: {
            required: true,
            email: true,
            emailCustom: true
        },
        linkweb:{
            url:true,
        },
        speciality:{
            required:true
        },
        speciality_other:{
            required: $("#speciality").val() === 'Otros'?true:false
        }
    },
    highlight: function (element, errorClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "admin/perfil/ajax_editar_perfil",
            dataType: 'json',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                console.log(response);
                if(response.status=="SUCCESS"){
                    toastr.success(response.message)
                    location.reload();
                }else{
                    toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                }
            }
        });
    }
})