const $form_login = $('#form_login');
$form_login.validate({
    ignore: ".ignore,:hidden",
    rules: {
        usuario: {
            required: true,
        },
        password: {
            required: true,
            minlength:6,
        }
    },
    highlight: function (element, errorClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass) {
        $(element).removeClass('is-invalid');
    },
    errorPlacement: function(error, element) {
        return false;
    },
    submitHandler:function(form) {
        $.ajax({
            url: site + "admin/login/login",
            dataType: 'JSON',
            type: 'post',
            data: $(form).serialize(),
            success: function(response) {
                $(form)[0].reset();
                if(response.status=="SUCCESS"){
                    if(response.type =="Colegiado"){
                        window.location.href = site+"admin/mi-perfil";
                    }
                    else if(response.type == "Adminweb"){
                        window.location.href = site+"admin/cms/events";
                    }
                    else {
                        window.location.href = site+"admin/buscador";
                    }
                }
                else{
                    toastr.error(response.message)
                }
            }
        });
    }
})