

$(function () {
    $("#image_directive").fileinput({
        language: "es",
        showUpload: false,
        showRemove: false,
        //required: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: [
            {caption: "image.jpg", size: 827000, width: "120px", url: "/file-upload-batch/2", key: 1},
        ]
    });

    const $formEvent = $('#formDirective');
    $formEvent.validate({
        ignore: ".ignore,:hidden",
        rules: {
            name_directive:{
                required: true,
            },
            position_directive: {
                required: true,
            },
            periodo: {
                required: true,
            }
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formDirective')[0])
            $.ajax({
                url: site + "admin/cms/nosotros/ajax_insert_directivo",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#directivo_id").val()===""){
                            setTimeout(function(){
                                window.location.href=site+"admin/cms/nosotros/directivos"
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});