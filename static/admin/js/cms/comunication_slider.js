$(function () {
    $("#comunicado_slide").fileinput({
        language: "es",
        uploadUrl: site + "admin/cms/home/ajax_insert_comunication",
        //required: true,
        //minImageWidth: 500,
        //minImageHeight: 1000,
        theme: 'fas',
        overwriteInitial: false,
        enableResumableUpload: true,
        initialPreviewAsData: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: image_initialPreviewConfig,
        deleteUrl: site + "admin/cms/home/delete_comunication",
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
    });

})