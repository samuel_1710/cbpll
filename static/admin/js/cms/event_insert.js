

$(function () {
    $("#image_event").fileinput({
        language: "es",
        showUpload: false,
        showRemove: false,
        //required: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: [
            {caption: "image.jpg", size: 827000, width: "120px", url: "/file-upload-batch/2", key: 1},
        ]
    });

    const $formEvent = $('#formEvent');
    $formEvent.validate({
        ignore: ".ignore,:hidden",
        rules: {
            title_event:{
                required: true,
            },
            description: {
                required: true,
            },
            objetivo: {
                required: true,
            },
            horario:{
                required: true,
            },
            temario:{
                required: true,
            }
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formEvent')[0])
            $.ajax({
                url: site + "admin/cms/events/ajax_insert_event",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#event_id").val()===""){
                            setTimeout(function(){
                                window.location.href=site+"admin/cms/events"
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

    const $formEditCuota = $('#formEditCuota');
    $formEditCuota.validate({
        ignore: ".ignore,:hidden",
        rules: {
            date_pago: {
                required: true,
            },
            nro_operacion: {
                required: true,
            },
            monto_pago:{
                required: true,
            },
            date_vencimiento:{
                required: true,
            },
            /*detalle_pago:{
                required:true,
            },
            voucher_file:{
                required:true,
            }*/
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formEditCuota')[0])
            $.ajax({
                url: site + "admin/colegiado/ajax_edit_cuota",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message);
                        setTimeout(function(){
                            location.reload();
                        },1000)
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});