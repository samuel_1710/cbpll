$(function () {
    bsCustomFileInput.init();
    const $formBoletin = $('#formBoletin');
    $formBoletin.validate({
        ignore: ".ignore,:hidden",
        rules: {
            text_boletin:{
                required: true,
            },
            type_boletin: {
                required: true,
            },
            /*detalle_pago:{
                required:true,
            },
            voucher_file:{
                required:true,
            }*/
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formBoletin')[0])
            $.ajax({
                url: site + "admin/cms/nosotros/ajax_insert_boletin",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#boletin_id").val()===""){
                            setTimeout(function(){
                                window.location.href=site+"admin/cms/nosotros/boletines"
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});