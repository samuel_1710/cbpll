$(document).ready(function() { 
    $('.customTextArea').summernote({
        lang: 'es-ES'
    });
    const $formProcedure = $('#formProcedure');
    $formProcedure.validate({
        ignore: ".ignore,:hidden,.note-editor *",
        rules: {
            name_tramite:{
                required: true,
            },
            /*description_tramite: {
                required: true,
            }*/
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formProcedure')[0])
            var textareaValue = $('.customTextArea').summernote('code');           
            var procedureData = { 
                procedure_id: $("#procedure_id").val(), 
                name_tramite: $("#name_tramite").val(), 
                note: $("#note").val(), 
                description_tramite: textareaValue 
            };
            $.ajax({
                url: site + "admin/cms/procedure/ajax_insert_procedure",
                type: 'post',
                dataType:'json',
                data: procedureData,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#procedure_id").val()===""){
                            setTimeout(function(){
                                window.location.href=site+"admin/cms/procedure"
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});