$(function () {

    // $('.customTextArea').summernote({
    //     lang: 'es-ES'
    // });
    $("#image_institucional").fileinput({
        language: "es",
        showUpload: false,
        showRemove: false,
        //required: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreviewAsData: true,
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: [
            {caption: "image.jpg", size: 827000, width: "120px", url: "", key: 1},
        ]
    });

    const $formInstitucional = $('#formInstitucional');
    $formInstitucional.validate({
        ignore: ".ignore,:hidden,.note-editor *",
        rules: {
            /*textHistoria:{
                required: true,
            },*/
            mision: {
                required: true,
            },
            vision: {
                required: true,
            }
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var textareaValue = $('.customTextArea').summernote('code');   
            var formData = new FormData($('#formInstitucional')[0])
            formData.append('s', textareaValue);
            $.ajax({
                url: site + "admin/cms/nosotros/ajax_update_institucional",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#event_id").val()===""){
                            setTimeout(function(){
                                window.location.reload();
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })

});