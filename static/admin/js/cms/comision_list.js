$(document).ready(function() { 
    $(".delete_comision").on("click",function(){
        let comision_id = $(this).data("id");
        let comsion_name = $(this).parent().parent().find(".comision_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar la comisión: "+comsion_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/nosotros/ajax_delete_comision",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        comision_id:comision_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar el evento. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar el evento. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
    $(".visibility_comision").on("click",function(){
        let comision_id = $(this).data("id");
        let visibility = $(this).data("visibility");
        let comsion_name = $(this).parent().parent().find(".comision_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de cambiar visibilidad de la comisión: "+comsion_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/nosotros/ajax_visibility_comision",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        comision_id:comision_id,
                        visibility:visibility
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Actualizado',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del evento. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del evento. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})