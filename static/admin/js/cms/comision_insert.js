

$(function () {
    const $formComision = $('#formComision');
    $formComision.validate({
        ignore: ".ignore,:hidden",
        rules: {
            tipoComision:{
                required: true,
            },
            name_comision: {
                required: true,
            },
            presidente_comision: {
                required: true,
            },
            members_comision:{
                required: true,
            }
        },
        highlight: function (element, errorClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass('is-invalid');
        },
        errorPlacement: function(error, element) {
            return false;
        },
        submitHandler:function(form) {
            var formData = new FormData($('#formComision')[0])
            $.ajax({
                url: site + "admin/cms/nosotros/ajax_insert_comision",
                type: 'post',
                dataType:'json',
                data: formData,
                cache:false,
                contentType:false,
                processData:false,
                success: function(response) {
                    if(response.status=="SUCCESS"){
                        toastr.success(response.message)
                        if($("#comision_id").val()===""){
                            setTimeout(function(){
                                window.location.href=site+"admin/cms/nosotros/comisiones"
                            },1000)
                        }
                    }else{
                        toastr.error('Ha ocurrido un inconveniente. Intentélo mas tarde.')
                    }
                }
            });
        }
    })
});