$(document).ready(function() { 
    $(".delete_procedure").on("click",function(){
        let procedure_id = $(this).data("id");
        let procedure_name = $(this).parent().parent().find(".procedure_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar el trámite: "+procedure_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/procedure/ajax_delete_procedure",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        procedure_id:procedure_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar el trámite. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar el trámite. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
    $(".visibility_procedure").on("click",function(){
        let procedure_id = $(this).data("id");
        let visibility = $(this).data("visibility");
        let procedure_name = $(this).parent().parent().find(".procedure_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de cambiar visibilidad del trámite: "+procedure_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/procedure/ajax_visibility_procedure",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        procedure_id:procedure_id,
                        visibility:visibility
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Actualizado',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del trámite. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del trámite. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})