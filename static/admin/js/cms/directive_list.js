$(document).ready(function() { 
    $(".delete_directive").on("click",function(){
        let directive_id = $(this).data("id");
        let directive_name = $(this).parent().parent().find(".directive_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar el directivo: "+directive_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/nosotros/ajax_delete_directive",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        directive_id:directive_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar el evento. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar el evento. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
    $(".visibility_directive").on("click",function(){
        let directive_id = $(this).data("id");
        let visibility = $(this).data("visibility");
        let directive_name = $(this).parent().parent().find(".directive_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de cambiar visibilidad de: "+directive_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/nosotros/ajax_visibility_directive",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        directive_id:directive_id,
                        visibility:visibility
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Actualizado',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del directivo. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del directivo. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})