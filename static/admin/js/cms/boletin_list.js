$(document).ready(function() { 
    $(".delete_boletin").on("click",function(){
        let boletin_id = $(this).data("id");
        let boletin_text = $(this).parent().parent().find(".boletin_text").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar la comisión: "+boletin_text+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/nosotros/ajax_delete_boletin",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        boletin_id:boletin_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar el documento. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar el documento. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})