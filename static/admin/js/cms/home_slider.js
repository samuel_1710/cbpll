$(function () {
    $("#image_slider").fileinput({
        language: "es",
        uploadUrl: site + "admin/cms/home/ajax_insert_slider",
        //required: true,
        //minImageWidth: 500,
        //minImageHeight: 1000,
        theme: 'fas',
        overwriteInitial: false,
        enableResumableUpload: true,
        initialPreviewAsData: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: image_initialPreviewConfig,
        deleteUrl: site + "admin/cms/home/delete_image_slider",
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
    });
    $("#infraestructura_slider").fileinput({
        language: "es",
        uploadUrl: site + "admin/cms/home/ajax_insert_infraestructura",
        //required: true,
        //minImageWidth: 500,
        //minImageHeight: 1000,
        theme: 'fas',
        overwriteInitial: false,
        enableResumableUpload: true,
        initialPreviewAsData: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: image_initialPreviewConfig,
        deleteUrl: site + "admin/cms/home/delete_image_slider",
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
    });
    $("#convenios_slider").fileinput({
        language: "es",
        uploadUrl: site + "admin/cms/home/ajax_insert_convenios",
        //required: true,
        //minImageWidth: 500,
        //minImageHeight: 1000,
        theme: 'fas',
        overwriteInitial: false,
        enableResumableUpload: true,
        initialPreviewAsData: true,
        allowedFileExtensions: ["jpg", "png", "gif",'jpeg'],
        initialPreview: image_initial,
        initialPreviewFileType: 'image',
        initialPreviewConfig: image_initialPreviewConfig,
        deleteUrl: site + "admin/cms/home/delete_image_slider",
        fileActionSettings: {
            showZoom: function(config) {
                if (config.type === 'pdf' || config.type === 'image') {
                    return true;
                }
                return false;
            }
        }
    });

})