$(document).ready(function() { 
    $(".delete_event").on("click",function(){
        let event_id = $(this).data("id");
        let event_name = $(this).parent().parent().find(".event_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar el evento: "+event_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/events/ajax_delete_event",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        event_id:event_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al eliminar el evento. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al eliminar el evento. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
    $(".visibility_event").on("click",function(){
        let event_id = $(this).data("id");
        let event_name = $(this).parent().parent().find(".event_name").text()
        Swal.fire({
            title: 'Mensaje de Validación',
            text: "¿Estás seguro de eliminar el evento: "+event_name+" ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: 'Aceptar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#dc3545',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: site + "admin/cms/events/ajax_visibility_event",
                    dataType: 'JSON',
                    type: 'post',
                    data: {
                        event_id:event_id
                    },
                    success: function(response) {
                        if(response.status=="SUCCESS"){
                            Swal.fire(
                                'Eliminado!',
                                response.message,
                                'success'
                            )
                            setTimeout(function(){
                                window.location.reload()
                            },1000)
                        }else{
                            toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del evento. Intentélo mas tarde.')
                        }
                    },
                    error:function(error){
                        toastr.error('Ha ocurrido un inconveniente al cambiar la visibilidad del evento. Intentélo mas tarde.')
                    }
                });               
            }
        })
    })
})